﻿using System;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator;
using BrivoApi.Core;
using BrivoApi.Pass;
using BrivoApi.Security;
using BrivoApi.Dealer;
using BrivoApi.Management;
using Microsoft.Rest.TransientFaultHandling;

namespace BrivoApi
{
  /// <summary>
  /// Represents an account for working with the Brivo API.
  /// </summary>
  public static class BrivoAccount
  {
    private static Uri ConstructApiEndpoint()
    {
      return new Uri("https://api.brivo.com/v1/api");
    }

    #region Create Administrator Client Methods

    /// <summary>
    /// Creates a new administrator client.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new administrator client.
    /// </returns>
    public static async Task<AdministratorClient> CreateAdministratorClientAsync(AccountCredentials credentials, Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default (CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      if (apiEndpoint == null)
      {
        apiEndpoint = ConstructApiEndpoint();
      }

      var context = await AuthorizationContext.CreateAsync(credentials, oAuthEndpoint, cancellationToken);
      return new AdministratorClient(context, apiEndpoint);



    }
    /// <summary>
    /// Creates an administrator client using <see cref="OAuthResponse"/>
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthResponse">The administrator OAuth token.</param>
    /// <param name="oAuthEndpoint">The OAuth token endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new <see cref="AdministratorClient" /> object.
    /// </returns>
    public static async Task<AdministratorClient> CreateAdministratorClientAsync(ClientCredentials credentials, OAuthResponse oAuthResponse, Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      if (apiEndpoint == null)
      {
        apiEndpoint = ConstructApiEndpoint();
      }
      var authorizationContext = await AuthorizationContext.CreateAsync(credentials, oAuthResponse, oAuthEndpoint, cancellationToken);
      return new AdministratorClient(authorizationContext, apiEndpoint);
    }

    /// <summary>
    /// Creates a new administrator client.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new administrator client.
    /// </returns>
    public static Task<AdministratorClient> CreateAdministratorClientAsync(string clientId, string clientSecret,string username, string password, Uri oAuthEndpoint = null, Uri apiEndpoint = null,CancellationToken cancellationToken = default(CancellationToken))
    {
      return CreateAdministratorClientAsync(new AccountCredentials(clientId, clientSecret, username, password),
      oAuthEndpoint, apiEndpoint, cancellationToken);
    }

    /// <summary>
    /// Creates a new administrator client.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <returns>
    /// A new administrator client.
    /// </returns>
    public static AdministratorClient CreateAdministratorClient(string clientId, string clientSecret, string username, string password, Uri oAuthEndpoint = null, Uri apiEndpoint = null)
    {
      return
        Task.Factory.StartNew(
          () => CreateAdministratorClientAsync(new AccountCredentials(clientId, clientSecret, username,
            password), oAuthEndpoint, apiEndpoint).ConfigureAwait(false).GetAwaiter().GetResult(),
          CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default).GetAwaiter().GetResult();
    }
    #endregion

    #region Create Dealer Client Methods
    /// <summary>
    /// Creates a new dealer client.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new dealer client.
    /// </returns>
    public static async Task<DealerClient> CreateDealerClientAsync(AccountCredentials credentials, Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
        Requires.ArgumentNotNull(credentials, "credentials");
        if (oAuthEndpoint == null)
        {
            oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
        }
        if (apiEndpoint == null)
        {
            apiEndpoint = ConstructApiEndpoint();
        }

        var context = await AuthorizationContext.CreateAsync(credentials, oAuthEndpoint, cancellationToken);
        return new DealerClient(context, apiEndpoint);
    }

    /// <summary>
    /// Creates a new dealer client.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new dealer client.
    /// </returns>
    public static Task<DealerClient> CreateDealerClientAsync(string clientId, string clientSecret,
        string username, string password, Uri oAuthEndpoint = null, Uri apiEndpoint = null,
        CancellationToken cancellationToken = default(CancellationToken))
    {
        return CreateDealerClientAsync(new AccountCredentials(clientId, clientSecret, username, password),
            oAuthEndpoint, apiEndpoint, cancellationToken);
    }

    /// <summary>
    /// Creates a new dealer client.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <returns>
    /// A new dealer client.
    /// </returns>
    public static DealerClient CreateDealerClient(string clientId, string clientSecret, string username, string password, Uri oAuthEndpoint = null, Uri apiEndpoint = null)
    {
        return
            Task.Factory.StartNew(
            () => CreateDealerClientAsync(new AccountCredentials(clientId, clientSecret, username,
                password), oAuthEndpoint, apiEndpoint).ConfigureAwait(false).GetAwaiter().GetResult(),
            CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default).GetAwaiter().GetResult();
    }
    /// <summary>
    /// creates a new client using <see cref="OAuthResponse"/>
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthResponse">The Dealer OAuth token.</param>
    /// <param name="oAuthEndpoint">The OAuth token endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new <see cref="DealerClient" /> object.
    /// </returns>
    public static async Task<DealerClient> CreateDealerClientAsync(ClientCredentials credentials, OAuthResponse oAuthResponse, Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      if (apiEndpoint == null)
      {
        apiEndpoint = ConstructApiEndpoint();
      }
      var authorizationContext = await AuthorizationContext.CreateAsync(credentials, oAuthResponse, oAuthEndpoint, cancellationToken);
      return new DealerClient(authorizationContext, apiEndpoint);
    }

    #endregion

    #region Create Management Client Methods
    /// <summary>
    /// Creates a new management client.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new management client.
    /// </returns>
    public static async Task<ManagementClient> CreateManagementClientAsync(AccountCredentials credentials,
      Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      if (apiEndpoint == null)
      {
        apiEndpoint = ConstructApiEndpoint();
      }

      var context = await AuthorizationContext.CreateAsync(credentials, oAuthEndpoint, cancellationToken);
      return new ManagementClient(context, apiEndpoint);
    }

    /// <summary>
    /// Creates a new management client from a previously retrieved OAuth response (refresh token).  
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthResponse">The OAuth response for a previously redeemed path.</param>
    /// <param name="oAuthEndpoint">The OAuth token endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new <see cref="ManagementClient" /> object.
    /// </returns>
    public static async Task<ManagementClient> CreateManagementClientAsync(ClientCredentials credentials, OAuthResponse oAuthResponse, Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      if (apiEndpoint == null)
      {
        apiEndpoint = ConstructApiEndpoint();
      }
      var authorizationContext = await AuthorizationContext.CreateAsync(credentials, oAuthResponse, oAuthEndpoint, cancellationToken);
      return new ManagementClient(authorizationContext, apiEndpoint);
    }

    /// <summary>
    /// Creates a new management client.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new management client.
    /// </returns>
    public static Task<ManagementClient> CreateManagementClientAsync(string clientId, string clientSecret, string username, string password, Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      return CreateManagementClientAsync(new AccountCredentials(clientId, clientSecret, username, password),
        oAuthEndpoint, apiEndpoint, cancellationToken);
    }

    /// <summary>
    /// Creates a new management client.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="oAuthEndpoint">The OAuth endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <returns>
    /// A new management client.
    /// </returns>
    public static ManagementClient CreateManagementClient(string clientId, string clientSecret, string username, string password, Uri oAuthEndpoint = null, Uri apiEndpoint = null)
    {
      return
        Task.Factory.StartNew(
          () => CreateManagementClientAsync(new AccountCredentials(clientId, clientSecret, username,
            password), oAuthEndpoint, apiEndpoint).ConfigureAwait(false).GetAwaiter().GetResult(),
          CancellationToken.None, TaskCreationOptions.None, TaskScheduler.Default).GetAwaiter().GetResult();
    }
    #endregion

    #region Create Pass Cilent Methods
    /// <summary>
    /// Creates the pass client that is intended to be used to redeem a pass.  No pass token information is provided
    /// so the caller must redeem a pass to be used with this client.
    /// </summary>
    /// <param name="credentials">The invitation credentials.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <returns>
    /// A new <see cref="PassClient" /> object.
    /// </returns>
    public static PassClient CreatePassClient(ClientCredentials credentials, Uri apiEndpoint = null)
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (apiEndpoint == null)
      {
        apiEndpoint = ConstructApiEndpoint();
      }
      return new PassClient(credentials, apiEndpoint);
    }

    /// <summary>
    /// Creates the pass client that is intended to be used to redeem a pass.  No pass token information is provided
    /// so the caller must redeem a pass to be used with this client.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <returns>
    /// A task that creates a new <see cref="PassClient" /> object.
    /// </returns>
    public static Task<PassClient> CreatePassClientAsync(ClientCredentials credentials, Uri apiEndpoint = null)
    {
      return Task.Factory.StartNew(() => CreatePassClient(credentials, apiEndpoint));
    }

    /// <summary>
    /// Creates the pass client from a previously redeemed pass.  The refresh token in the response is used (if necessary)
    /// to authenticate before using the pass.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthResponse">The OAuth response for a previously redeemed path.</param>
    /// <param name="oAuthEndpoint">The OAuth token endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that creates a new <see cref="PassClient" /> object.
    /// </returns>
    public static async Task<PassClient> CreatePassClientAsync(ClientCredentials credentials, OAuthResponse oAuthResponse, Uri oAuthEndpoint = null, Uri apiEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      if (apiEndpoint == null)
      {
        apiEndpoint = ConstructApiEndpoint();
      }
      var authorizationContext = await AuthorizationContext.CreateAsync(credentials, oAuthResponse, oAuthEndpoint, cancellationToken);
      return new PassClient(authorizationContext, apiEndpoint);
    }

    /// <summary>
    /// Creates the pass client from a previously redeemed pass.  The refresh token in the response is used (if necessary)
    /// to authenticate before using the pass.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthResponse">The o authentication response.</param>
    /// <param name="oAuthEndpoint">The o authentication endpoint.</param>
    /// <param name="apiEndpoint">The API endpoint.</param>
    /// <returns>A new pass client</returns>
    public static PassClient CreatePassClient(ClientCredentials credentials, OAuthResponse oAuthResponse,
      Uri oAuthEndpoint = null, Uri apiEndpoint = null)
    {
      return Task.Factory.StartNew(() => 
        CreatePassClientAsync(credentials, oAuthResponse, oAuthEndpoint, apiEndpoint, CancellationToken.None).GetAwaiter().GetResult()).GetAwaiter().GetResult();
    }
    #endregion

    #region OAuth Methods
    /// <summary>
    /// Acquires an OAuth token for the specified client and user credentials.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthEndpoint">The o authentication endpoint.</param>
    /// <returns>The OAuth response.</returns>
    public static OAuthResponse AcquireTokenAsUser(AccountCredentials credentials, Uri oAuthEndpoint = null)
    {
      return Task.Factory.StartNew(() => 
        AcquireTokenAsUserAsync(credentials, oAuthEndpoint, CancellationToken.None).GetAwaiter().GetResult()).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Acquires an OAuth token for the specified client and user credentials.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthEndpoint">The o authentication endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The OAuth response</returns>
    public static Task<OAuthResponse> AcquireTokenAsUserAsync(AccountCredentials credentials, Uri oAuthEndpoint = null,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      var svc = new OAuthService(credentials.CreateClientCredentials(), oAuthEndpoint);
      return ((IOAuthService) svc).AcquireTokenAsUserAsync(credentials.UserName, credentials.Password, cancellationToken);
    }

    /// <summary>
    /// Acquires a new token using a previously retrieved refresh token.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="refreshToken">The refresh token.</param>
    /// <param name="oAuthEndpoint">The o authentication endpoint.</param>
    /// <returns>The OAuth response.</returns>
    public static OAuthResponse AcquireTokenByRefreshToken(ClientCredentials credentials, string refreshToken, Uri oAuthEndpoint = null)
    {
      return Task.Factory.StartNew(() =>
        AcquireTokenByRefreshTokenAsync(credentials, refreshToken, oAuthEndpoint, CancellationToken.None).GetAwaiter().GetResult()).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Acquires a new token using a previously retrieved refresh token.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="refreshToken">The refresh token.</param>
    /// <param name="oAuthEndpoint">The o authentication endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    public static Task<OAuthResponse> AcquireTokenByRefreshTokenAsync(ClientCredentials credentials, string refreshToken, Uri oAuthEndpoint, CancellationToken cancellationToken = default(CancellationToken))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = AuthorizationContext.ConstructOAuthEndpoint();
      }
      var svc = new OAuthService(credentials.CreateClientCredentials(), oAuthEndpoint);
      return ((IOAuthService) svc).AcquireTokenByRefreshTokenAsync(refreshToken, cancellationToken);
    }
    #endregion
    /// <summary>
    /// This Sets the Global RetryPolicy policy for Transient Http Errors 
    /// <see cref="RetryPolicy"/>
    /// <see cref="ITransientErrorDetectionStrategy"/>
    /// <see cref="ExponentialBackoffRetryStrategy"/>
    /// </summary>
    public static RetryPolicy GlobalRetryPolicy { get; set; }

    /// <summary>
    /// Gets or sets the api key.
    /// </summary>
    /// <value>The api key.</value>
    public static string ApiKey { get; set; }

  }
}
