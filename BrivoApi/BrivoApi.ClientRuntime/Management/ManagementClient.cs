﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using BrivoApi.Dealer.Models;
using BrivoApi.Security;
using Microsoft.Rest;

namespace BrivoApi.Management
{
  /// <summary>
  /// A .NET client for the Brivo Management APIs.
  /// </summary>
  public class ManagementClient : AdministratorClientBase<ManagementClient>, IManagementClient
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ManagementClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    public ManagementClient(AuthorizationContext authorizationContext, Uri endpoint)
      : base(authorizationContext, endpoint)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ManagementClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="handlers">The handlers.</param>
    public ManagementClient(AuthorizationContext authorizationContext, Uri endpoint, params DelegatingHandler[] handlers)
      : base(authorizationContext, endpoint, handlers)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ManagementClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="rootHandler">The root handler.</param>
    /// <param name="handlers">The handlers.</param>
    public ManagementClient(AuthorizationContext authorizationContext, Uri endpoint, HttpClientHandler rootHandler,
      params DelegatingHandler[] handlers) : base(authorizationContext, endpoint, rootHandler, handlers)
    {
    }
  
    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    public async Task<HttpOperationResponse<Account>> CreateDealerAccountAsync(Account account,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return await
        this.PostAsync<Account, Account>(BrivoApiConstants.UriPaths.CreateAccount, account, nameof(this.CreateDealerAccountAsync),
          cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="password">the password for the account</param>
    /// <returns>the new <see cref="Account"/>.</returns>
    public async Task<HttpOperationResponse<Account>> CreateDealerAccountAsync(Account account, string password,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      var encodedPassword = WebUtility.UrlEncode(password);
      return await
        this.PostAsync<Account, Account>(string.Format(BrivoApiConstants.UriPaths.CreateAccountWithPassword, encodedPassword), account, nameof(this.CreateDealerAccountAsync),
          cancellationToken);
    }
    /// <summary>
    /// Gets an impersonation token for the specified administrator or dealer ID.
    /// </summary>
    /// <param name="adminId">The ID of the Administrator or Dealer to impersonate.</param>
    /// <param name="clientCredentials">The client credentials.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// a Task of type a <see cref="HttpOperationResponse{T}" /> containing a <see cref="OAuthResponse" /> that represents the asynchronous operation.
    /// </returns>
    public Task<HttpOperationResponse<OAuthResponse>> RequestImpersonationTokenByIdAsync(int adminId, ClientCredentials clientCredentials,
      CancellationToken cancellationToken = new CancellationToken())
    {
      var apiKey = new ApiKey {ClientId = clientCredentials.ClientId, ClientSecret = clientCredentials.ClientSecret};
      return this.PostAsync<ApiKey, OAuthResponse>(string.Format(BrivoApiConstants.UriPaths.RunAsTokenFormat, adminId),
        apiKey, nameof(this.RequestImpersonationTokenByIdAsync), cancellationToken);
    }
  }
}
