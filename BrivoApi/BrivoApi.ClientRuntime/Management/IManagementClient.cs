using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using BrivoApi.Security;
using Microsoft.Rest;

namespace BrivoApi.Management
{
  /// <summary>
  /// Defines the interface to a management client for Brivo API.
  /// </summary>
  public interface IManagementClient
  {
    /// <summary>
    /// Get the authorization context used for this client.
    /// </summary>
    AuthorizationContext AuthorizationContext { get; }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{TElement}" /> of <see cref="Account" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Account" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Account>>> ListAccountsPagedAsync(PageInfo page, Filter filter,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Account"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Account"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Account>> GetAccountByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    Task<HttpOperationResponse<Account>> CreateDealerAccountAsync(Account account,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="password">the password for the account</param>
    /// <returns>the new <see cref="Account"/>.</returns>
    Task<HttpOperationResponse<Account>> CreateDealerAccountAsync(Account account, string password,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Administrator"/>.
    /// </summary>
    /// <param name="accountId">the account id</param>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Administrator.Models.Administrator>>>
      ListAccountAdministratorsPagedAsync(int accountId, PageInfo page,
        CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Account"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Account"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Account>>> ListAccountsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    // Initiates an asynchronous operation to return a HTTP response containing a <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="BrivoApi.Administrator.Models.Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Administrator.Models.Administrator>> GetCurrentAdministratorAsync(
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Gets an impersonation token for the specified administrator or dealer ID.
    /// </summary>
    /// <param name="adminId">The ID of the Administrator or Dealer to impersonate.</param>
    /// <param name="clientCredentials">The client credentials.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// a Task of type a <see cref="HttpOperationResponse{T}"/> containing a <see cref="OAuthResponse"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<OAuthResponse>> RequestImpersonationTokenByIdAsync(int adminId,
      ClientCredentials clientCredentials, CancellationToken cancellationToken = default(CancellationToken));
  }
}