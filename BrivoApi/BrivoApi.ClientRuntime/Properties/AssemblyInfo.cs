﻿using System.Resources;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("BrivoApi.ClientRuntime")]
[assembly: AssemblyDescription("Client Runtime Library for Brivo API")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Brivo")]
[assembly: AssemblyProduct("Brivo OnAir Api")]
[assembly: AssemblyCopyright("Copyright © 2016 Brivo, Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en")]

[assembly: ComVisible(false)]
[assembly: AssemblyVersion("1.3.7.1")]
[assembly: AssemblyFileVersion("1.3.7.1")]
