﻿using Newtonsoft.Json;

namespace BrivoApi.Dealer.Models
{
  /// <summary>
  /// Encapsulate basic client credentials.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class ApiKey
  {
    /// <summary>
    /// Gets the client identifier.
    /// </summary>
    /// <value>
    /// The client identifier.
    /// </value>
    [JsonProperty("clientId")]
    public string ClientId { get; set; }
    /// <summary>
    /// Gets the client secret.
    /// </summary>
    /// <value>
    /// The client secret.
    /// </value>
    [JsonProperty("secret")]
    public string ClientSecret { get; set; }
  }
}