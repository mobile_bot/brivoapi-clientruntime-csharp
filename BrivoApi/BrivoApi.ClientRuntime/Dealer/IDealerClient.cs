﻿using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using Microsoft.Rest;
using BrivoApi.Security;

namespace BrivoApi.Dealer
{
  /// <summary>
  /// Defines client-side logical representation of the Brivo APIs for dealers.
  /// </summary>
  public interface IDealerClient
  {
    /// <summary>
    /// Get the authorization context used for this client.
    /// </summary>
    AuthorizationContext AuthorizationContext { get; }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Account"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Account"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Account>>> ListAccountsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Account"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Account"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Account>> GetAccountByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>the new <see cref="Account"/>. password is emailed</returns>
    Task<HttpOperationResponse<Account>> CreateAccountAsync(Account account,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="password">the the url encoded password for the account</param>
    /// <returns>the new <see cref="Account"/>.</returns>
    Task<HttpOperationResponse<Account>> CreateAccountAsync(Account account, string password,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="newAdministrator">The Administrator.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Administrator.Models.Administrator>> CreateAdministratorAsync(
      Administrator.Models.Administrator newAdministrator,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="newAdministrator">The Administrator.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="password">the password for the account</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Administrator.Models.Administrator>> CreateAdministratorAsync(
      Administrator.Models.Administrator newAdministrator, string password,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Administrator"/>.
    /// </summary>
    /// <param name="accountId">the account id</param>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Administrator.Models.Administrator>>>
      ListAccountAdministratorsPagedAsync(int accountId, PageInfo page,
        CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="page">Page.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="BrivoApi.Administrator.Models.Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Administrator.Models.Administrator>>>
      ListAdministratorsPagedAsync(PageInfo page, CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">filter</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>  
    Task<HttpOperationResponse<GenericPagedCollection<Administrator.Models.Administrator>>> ListAdministratorsPagedAsync(PageInfo page, Filter filter, CancellationToken cancellationToken = new CancellationToken());
    /// <summary>
    // Initiates an asynchronous operation to return a HTTP response containing a <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="BrivoApi.Administrator.Models.Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Administrator.Models.Administrator>> GetCurrentAdministratorAsync(
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Gets an Impersonation Token token for the given <see cref="Administrator"/>
    /// </summary>
    /// <param name="id">administrator id</param>
    /// <param name="clientCredentials">dealer account api clientId\clientSecret </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// a Task of type a <see cref="HttpOperationResponse{T}"/> containing a <see cref="OAuthResponse"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<OAuthResponse>> RequestAdministratorImpersonationTokenByIdAsync(int id,
      ClientCredentials clientCredentials, CancellationToken cancellationToken = default(CancellationToken));
  }
}