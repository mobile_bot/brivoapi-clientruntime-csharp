﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using BrivoApi.Security;
using Microsoft.Rest;
using System.Collections.Generic;
using System.Net;
using BrivoApi.Administrator;
using BrivoApi.Dealer.Models;

namespace BrivoApi.Dealer
{
  /// <summary>
  /// A .NET client for the Brivo Administrator APIs.
  /// </summary>
  public class DealerClient : AdministratorClientBase<DealerClient>, IDealerClient
  {


    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    public DealerClient(AuthorizationContext authorizationContext, Uri endpoint)
      : base(authorizationContext, endpoint)
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="handlers">The handlers.</param>
    public DealerClient(AuthorizationContext authorizationContext, Uri endpoint,
      params DelegatingHandler[] handlers)
      : base(authorizationContext, endpoint, handlers)
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="rootHandler">The root handler.</param>
    /// <param name="handlers">The handlers.</param>
    public DealerClient(AuthorizationContext authorizationContext, Uri endpoint, HttpClientHandler rootHandler,
      params DelegatingHandler[] handlers)
      : base(authorizationContext, endpoint, rootHandler, handlers)
    {

    }
    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    public async Task<HttpOperationResponse<Account>> CreateAccountAsync(Account account,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return await
        this.PostAsync<Account, Account>(BrivoApiConstants.UriPaths.CreateAccount, account, "CreateAccountAsync",
          cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to create an <see cref="Account"/>.
    /// </summary>
    /// <param name="account">the account <see cref="Account"/> </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="password">the password for the account</param>
    /// <returns>the new <see cref="Account"/>.</returns>
    public async Task<HttpOperationResponse<Account>> CreateAccountAsync(Account account, string password,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      var encodedPassword = WebUtility.UrlEncode(password);
      return await
        this.PostAsync<Account, Account>(string.Format(BrivoApiConstants.UriPaths.CreateAccountWithPassword, encodedPassword), account, "CreateAccountAsync",
          cancellationToken);
    }
    /// <summary>
    /// Gets an Impersonation Token token for the given <see cref="Administrator"/>
    /// </summary>
    /// <param name="id">administrator id</param>
    /// <param name="clientCredentials">dealer account api clientId\clientSecret </param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// a Task of type a <see cref="HttpOperationResponse{T}"/> containing a <see cref="OAuthResponse"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<OAuthResponse>> RequestAdministratorImpersonationTokenByIdAsync(int id,
      ClientCredentials clientCredentials, CancellationToken cancellationToken = default(CancellationToken))
    {
      var client =
        new ApiKey
        {
          ClientId = clientCredentials.ClientId,
          ClientSecret = clientCredentials.ClientSecret
        };
      return await
        this.PostAsync<ApiKey, OAuthResponse>(string.Format(BrivoApiConstants.UriPaths.RunAsTokenFormat, id), client,
          "RequestAdministratorImpersonationTokenById",
          cancellationToken);
    }

  }
}