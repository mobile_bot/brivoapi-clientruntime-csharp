using System;
using System.IO;
using System.Threading;
using BrivoApi.Pass.Models;

namespace BrivoApi.Pass
{
  /// <summary>
  /// Provides a set of extension methods for objects of type <see cref="IPassClient"/>.
  /// </summary>
  public static class PassClientExtensions
  {
    /// <summary>
    /// Redeems a pass token using the specified request data.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="tokenRequestData">The request data.</param>
    /// <param name="tokenEndpoint">The token endpoint.</param>
    /// <returns>
    /// A <see cref="RedeemPassTokenResponse" /> containing pass token information.
    /// </returns>
    public static RedeemPassTokenResponse RedeemPassToken(this IPassClient client, RedeemPassTokenRequest tokenRequestData, Uri tokenEndpoint)
    {
      return client.RedeemPassTokenAsync(tokenRequestData, tokenEndpoint, CancellationToken.None).GetAwaiter().GetResult();
    }
    /// <summary>
    /// Use a pass to act on resource in the specified request data.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="requestData">The request data.</param>
    /// <returns>True if the use pass request was accepted, false otherwise.</returns>
    public static bool Use(this IPassClient client, UsePassRequest requestData)
    {
      return client.UseAsync(requestData, CancellationToken.None).GetAwaiter().GetResult().Body;
    }
    /// <summary>
    /// Refresh a pass using the specified request data.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <returns>A <see cref="RefreshPassResponse"/> containing pass information.</returns>
    public static RefreshPassResponse Refresh(this IPassClient client)
    {
      return client.RefreshAsync(CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the account logo as a stream of image bytes.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="requestData">The request data.</param>
    /// <returns>The stream to read the logo image from.</returns>
    public static Stream GetAccountLogo(this IPassClient client, GetAccountLogoRequest requestData)
    {
      return client.GetAccountLogoAsync(requestData, CancellationToken.None).GetAwaiter().GetResult();
    }
  }
}