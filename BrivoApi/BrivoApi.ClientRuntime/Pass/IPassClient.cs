﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Pass.Models;
using Microsoft.Rest;
using BrivoApi.Core;
using BrivoApi.Administrator.Models;

namespace BrivoApi.Pass
{
  /// <summary>
  /// Defines client-side logical representation of the Brivo APIs for working with bearer passes.
  /// </summary>
  public interface IPassClient
  {
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to redeem a pass token.
    /// </summary>
    /// <param name="tokenRequestData">The request data.</param>
    /// <param name="tokenEndpoint">The token endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{RedeemPassResponse}" /> that represents the asynchronous operation.
    /// </returns>
    Task<RedeemPassTokenResponse> RedeemPassTokenAsync(RedeemPassTokenRequest tokenRequestData, Uri tokenEndpoint,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to use a bearer pass.
    /// </summary>
    /// <param name="requestData">The request data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task of type <see cref="HttpOperationResponse{UsePassResponse}"/> that represents the asynchronous operation.</returns>
    Task<HttpOperationResponse<bool>> UseAsync(UsePassRequest requestData,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to refresh a bearer pass.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task of type <see cref="HttpOperationResponse{RefreshPassResponse}"/> that represents the asynchronous operation.</returns>
    Task<HttpOperationResponse<RefreshPassResponse>> RefreshAsync(
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to download an account logo.
    /// </summary>
    /// <param name="requestData">The request data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task of type <see cref="HttpOperationResponse{RefreshPassResponse}"/> that represents the asynchronous operation.</returns>
    Task<Stream> GetAccountLogoAsync(GetAccountLogoRequest requestData,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="AccessPoint" />.
    /// </summary>
    /// <param name="siteId">The site identifier.</param>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="AccessPoint" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<AccessPoint>>> ListAccessPointsBySiteIdPagedAsync(
      int siteId,
      PageInfo page, CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Site" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Site" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Site>>> ListSitesPagedAsync(
      PageInfo page, CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="DigitalCredential" />.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="DigitalCredential" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<DigitalCredential>> GetDigitalCredentialAsync(
      CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to get the digital credential
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="credential">The credential value for the card holder as retrieved from <see cref="GetDigitalCredentialAsync"></see></param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> UnlockAccessPointAsync(int id, string credential,
      CancellationToken cancellationToken = new CancellationToken());
  }
}