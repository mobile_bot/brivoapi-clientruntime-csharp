﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Core;
using BrivoApi.Pass.Models;
using BrivoApi.Security;
using Microsoft.Rest;
using BrivoApi.Administrator.Models;
using BrivoApi.Core.Filters;

namespace BrivoApi.Pass
{
  /// <summary>
  /// Provides a client-side logical representation of the Brivo APIs for working with bearer passes.
  /// </summary>
  public class PassClient : BrivoApiServiceClientBase<PassClient>, IPassClient
  {
    private AuthorizationContext authorizationContext;

    /// <summary>
    /// Initializes a new instance of the <see cref="PassClient"/> class.
    /// </summary>
    /// <param name="clientCredentials">The client credentials.</param>
    /// <param name="endpoint">The endpoint.</param>
    public PassClient(ClientCredentials clientCredentials, Uri endpoint)
      : base(clientCredentials.CreateClientCredentials(), endpoint)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PassClient" /> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    public PassClient(AuthorizationContext authorizationContext, Uri endpoint)
      : base(authorizationContext.GetToken().Credentials, endpoint)
    {
      this.authorizationContext = authorizationContext;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PassClient" /> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="handlers">The handlers.</param>
    public PassClient(AuthorizationContext authorizationContext, Uri endpoint, params DelegatingHandler[] handlers)
      : base(authorizationContext.GetToken().Credentials, endpoint, handlers)
    {
      this.authorizationContext = authorizationContext;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PassClient" /> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="rootHandler">The root handler.</param>
    /// <param name="handlers">The handlers.</param>
    public PassClient(AuthorizationContext authorizationContext, Uri endpoint, HttpClientHandler rootHandler,
      params DelegatingHandler[] handlers)
      : base(authorizationContext.GetToken().Credentials, endpoint, rootHandler, handlers)
    {
      this.authorizationContext = authorizationContext;
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to redeem a bearer pass.
    /// </summary>
    /// <param name="tokenRequestData">The request data.</param>
    /// <param name="tokenEndpoint">The token endpoint.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{RedeemPassResponse}" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<RedeemPassTokenResponse> RedeemPassTokenAsync(RedeemPassTokenRequest tokenRequestData,
      Uri tokenEndpoint, CancellationToken cancellationToken = new CancellationToken())
    {
      Requires.ArgumentNotNull(tokenRequestData, "requestData");

      this.authorizationContext =
        await AuthorizationContext.CreateAsync(this.Credentials,
          new InvitationCredentials(tokenRequestData.Email, tokenRequestData.ActivationCode), tokenEndpoint,
          cancellationToken);
      var rawResponse = this.authorizationContext.GetToken().OAuthResponse;
      return new RedeemPassTokenResponse
      {
        AccessToken = rawResponse.AccessToken,
        TokenType = rawResponse.TokenType,
        RefreshToken = rawResponse.RefreshToken,
        ExpiresInSeconds = rawResponse.ExpiresInSeconds,
        Scope = rawResponse.Scope,
      };
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to use a bearer pass.
    /// </summary>
    /// <param name="requestData">The request data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task of type <see cref="HttpOperationResponse{UsePassResponse}"/> that represents the asynchronous operation.</returns>
    [Obsolete("brivo internal only")]
    public async Task<HttpOperationResponse<bool>> UseAsync(UsePassRequest requestData,
      CancellationToken cancellationToken = new CancellationToken())
    {
      Requires.ArgumentNotNull(requestData, "requestData");

      if (this.authorizationContext == null)
      {
        throw new InvalidOperationException(
          "The UseAsync() method cannot be called without first obtaining an authorization context.");
      }
      var postResult =
        await
          this.PostAsync<UsePassRequest, string>(
            string.Format(BrivoApiConstants.UriPaths.UsePassFormat, requestData.DoorId), requestData, "UseAsync",
            cancellationToken);
      var result = new HttpOperationResponse<bool>
      {
        Body = postResult.Response.StatusCode == HttpStatusCode.NoContent,
        Response = postResult.Response,
        Request = postResult.Request
      };
      return result;
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to refresh a bearer pass.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task of type <see cref="HttpOperationResponse{RefreshPassResponse}"/> that represents the asynchronous operation.</returns>
    [Obsolete("brivo internal only")]
    public async Task<HttpOperationResponse<RefreshPassResponse>> RefreshAsync(
      CancellationToken cancellationToken = new CancellationToken())
    {
      if (this.authorizationContext == null)
      {
        throw new InvalidOperationException(
          "The RefreshAsync() method cannot be called without first obtaining an authorization context.");
      }
      return
        await
          this.GetAsync<RefreshPassResponse>(BrivoApiConstants.UriPaths.RefreshPass, "RefreshAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to download an account logo.
    /// </summary>
    /// <param name="requestData">The request data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{RefreshPassResponse}" /> that represents the asynchronous operation.
    /// </returns>
    /// <exception cref="System.InvalidOperationException">The GetAccountLogo() method cannot be called without first obtaining an authorization context.</exception>
    [Obsolete("brivo internal only")]
    public async Task<Stream> GetAccountLogoAsync(GetAccountLogoRequest requestData,
      CancellationToken cancellationToken = new CancellationToken())
    {
      Requires.ArgumentNotNull(requestData, "requestData");

      if (this.authorizationContext == null)
      {
        throw new InvalidOperationException(
          "The GetAccountLogo() method cannot be called without first obtaining an authorization context.");
      }

      Action<HttpRequestHeaders> acceptedImageFormats = h =>
      {
        h.Accept.ParseAdd("image/png");
        h.Accept.ParseAdd("image/jpeg");
        h.Accept.ParseAdd("image/gif");
        h.Accept.ParseAdd("application/json");
      };
      var httpContent =
        await
          this.GetRawContentAsync(
            string.Format(BrivoApiConstants.UriPaths.GetAccountLogoFormat, requestData.AccountId), null,
            "GetAccountLogoAsync", cancellationToken, acceptedImageFormats);
      return await httpContent.ReadAsStreamAsync();
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="AccessPoint" />.
    /// </summary>
    /// <param name="siteId">The site identifier.</param>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="AccessPoint" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<AccessPoint>>> ListAccessPointsBySiteIdPagedAsync(
      int siteId,
      PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<AccessPoint>>(
            string.Format(BrivoApiConstants.UriPaths.GetAccessPointsBySiteFormat, siteId), page,
            $"{nameof(this.ListAccessPointsBySiteIdPagedAsync)}", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Site" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Site" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Site>>> ListSitesPagedAsync(
      PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<Site>>(BrivoApiConstants.UriPaths.GetSites, page,
            "ListSitesPagedAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="DigitalCredential" />.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="DigitalCredential" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<DigitalCredential>> GetDigitalCredentialAsync(
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<DigitalCredential>(BrivoApiConstants.UriPaths.GetDigitalCredential,
            "GetDigitalCredentialAsync", cancellationToken);
    }




    /// <summary>
    /// Initiates an asynchronous operation to get the digital credential
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="credential">The credential value for the card holder as retrieved from <see cref="GetDigitalCredentialAsync"></see></param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> UnlockAccessPointAsync(int id, string credential,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PostAsync<object, object>(
            string.Format(BrivoApiConstants.UriPaths.UnlockAccessPointFormat, id), new {credentialValue = credential},
            $"{nameof(this.UnlockAccessPointAsync)}", cancellationToken);
    }
  }
}