using System.Collections.Generic;
using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Represents a site that a pass has access to.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class SiteAccess
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="SiteAccess"/> class.
    /// </summary>
    public SiteAccess()
    {
      this.AccessPoints = new List<AccessPointInfo>();
    }

    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the name of the site.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("siteName")]
    public string Name { get; set; }
    /// <summary>
    /// Gets or sets the address.
    /// </summary>
    /// <value>
    /// The address.
    /// </value>
    [JsonProperty("address")]
    public SiteAddress Address { get; set; }
    /// <summary>
    /// Gets or sets the time zone information.
    /// </summary>
    /// <value>
    /// The time zone information.
    /// </value>
    [JsonProperty("timeZone")]
    public string TimeZoneInfo { get; set; }
    /// <summary>
    /// Gets or sets the doors that a pass has access to.
    /// </summary>
    /// <value>
    /// The doors.
    /// </value>
    [JsonProperty("accessPoints")]
    public IList<AccessPointInfo> AccessPoints { get; set; }
  }
}