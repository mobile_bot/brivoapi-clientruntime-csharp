﻿using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Represents an access-point that a pass can access.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class AccessPointInfo
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    [JsonProperty("name")]
    public string Name { get; set; }
  }
}