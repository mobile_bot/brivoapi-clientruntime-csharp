﻿using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Encapsulates data for a refresh pass response.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class RefreshPassResponse : PassDataResponseBase
  {
  }
}
