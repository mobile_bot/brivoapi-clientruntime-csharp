﻿using BrivoApi.Security;
using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Encapsulates data for redeeming a pass token.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class RedeemPassTokenResponse : OAuthResponse
  {
  }
}
