﻿using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Encapsulates data for a redeem a pass token request.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class RedeemPassTokenRequest
  {
    /// <summary>
    /// Gets or sets the email associated with the person redeeming the pass.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    [JsonProperty("email")]
    public string Email { get; set; }
    /// <summary>
    /// Gets or sets the activation code.
    /// </summary>
    /// <value>
    /// The activation code.
    /// </value>
    [JsonProperty("activationCode")]
    public string ActivationCode { get; set; }
  }
}
