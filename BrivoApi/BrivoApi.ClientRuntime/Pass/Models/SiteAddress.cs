using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Encapsulates the address information for a site.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class SiteAddress
  {
    /// <summary>
    /// Gets or sets the street line1.
    /// </summary>
    /// <value>
    /// The street line1.
    /// </value>
    [JsonProperty("streetAddress1")]
    public string StreetLine1 { get; set; }
    /// <summary>
    /// Gets or sets the street line2.
    /// </summary>
    /// <value>
    /// The street line2.
    /// </value>
    [JsonProperty("streetAddress2", NullValueHandling = NullValueHandling.Ignore)]
    public string StreetLine2 { get; set; }
    /// <summary>
    /// Gets or sets the city.
    /// </summary>
    /// <value>
    /// The city.
    /// </value>
    [JsonProperty("city")]
    public string City { get; set; }
    /// <summary>
    /// Gets or sets the state.
    /// </summary>
    /// <value>
    /// The state.
    /// </value>
    [JsonProperty("state")]
    public string State { get; set; }
    /// <summary>
    /// Gets or sets the postal code.
    /// </summary>
    /// <value>
    /// The postal code.
    /// </value>
    [JsonProperty("postalCode")]
    public string PostalCode { get; set; }
  }
}