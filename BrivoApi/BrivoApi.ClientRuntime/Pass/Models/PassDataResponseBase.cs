﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Represents a redeem pass response.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public abstract class PassDataResponseBase
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="PassDataResponseBase"/> class.
    /// </summary>
    protected PassDataResponseBase()
    {
      this.Sites = new List<SiteAccess>();
    }

    /// <summary>
    /// Gets or sets the pass.
    /// </summary>
    /// <value>
    /// The pass.
    /// </value>
    [JsonProperty("pass")]
    public string Pass { get; set; }

    /// <summary>
    /// Gets or sets the name of the account.
    /// </summary>
    /// <value>
    /// The name of the account.
    /// </value>
    [JsonProperty("accountName")]
    public string AccountName { get; set; }

    /// <summary>
    /// Gets or sets the account identifier.
    /// </summary>
    /// <value>
    /// The account identifier.
    /// </value>
    [JsonProperty("accountId")]
    public int AccountId { get; set; }

    /// <summary>
    /// Gets or sets the issue date.
    /// </summary>
    /// <value>
    /// The issue date.
    /// </value>
    [JsonProperty("generated")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime GeneratedDate { get; set; }
	
    /// <summary>
    /// Gets or sets the enabled date.
    /// </summary>
    /// <value>
    /// The enabled date.
    /// </value>
    [JsonProperty("enabled", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? EnabledDate { get; set; }

    /// <summary>
    /// Gets or sets the expiry date.
    /// </summary>
    /// <value>
    /// The expiry date.
    /// </value>
    [JsonProperty("expires", NullValueHandling = NullValueHandling.Ignore)]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? ExpiryDate { get; set; }

	  /// <summary>
    /// Gets or sets the sites that can be accessed with this pass.
    /// </summary>
    /// <value>
    /// The sites that can be accessed with this pass.
    /// </value>
    [JsonProperty("sites")]
    public IList<SiteAccess> Sites { get; set; }
  }
}
