﻿using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Encapsulates data needed to retrieve an account logo.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class GetAccountLogoRequest
  {
    /// <summary>
    /// Gets or sets the account identifier.
    /// </summary>
    /// <value>
    /// The account identifier.
    /// </value>
    [JsonProperty("accountId")]
    public int AccountId { get; set; }
  }
}