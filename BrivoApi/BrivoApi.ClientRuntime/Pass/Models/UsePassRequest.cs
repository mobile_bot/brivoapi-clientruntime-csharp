﻿using Newtonsoft.Json;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Encapsulates data for a use pass request.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class UsePassRequest
  {
    /// <summary>
    /// Gets or sets the pass payload.
    /// </summary>
    /// <value>
    /// The pass.
    /// </value>
    [JsonProperty("pass")]
    public string Pass { get; set; }
    /// <summary>
    /// Gets or sets the door identifier.
    /// </summary>
    /// <value>
    /// The door identifier.
    /// </value>
    [JsonProperty("accessPointId")]
    public int DoorId { get; set; }
    /// <summary>
    /// Gets or sets the proximity payload.
    /// </summary>
    /// <value>The proximity data.</value>
    [JsonProperty ("proximity")]
    public dynamic Proximity { get; set; }
  }
}
