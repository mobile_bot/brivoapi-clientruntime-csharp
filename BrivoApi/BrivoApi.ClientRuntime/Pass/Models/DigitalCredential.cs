﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace BrivoApi.Pass.Models
{
  /// <summary>
  /// Encapsulates data for a digital credential response.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class DigitalCredential
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the credential value.
    /// </summary>
    /// <value>
    /// The credential value.
    /// </value>
    [JsonProperty("credentialValue")]
    public string CredentialValue { get; set; }

    /// <summary>
    /// Gets or sets the occurred.
    /// </summary>
    /// <value>
    /// The occurred.
    /// </value>
    [JsonProperty("effectiveFrom")]
    [JsonConverter(typeof (IsoDateTimeConverter))]
    public DateTime EffectiveFrom { get; set; }

    /// <summary>
    /// Gets or sets the occurred.
    /// </summary>
    /// <value>
    /// The occurred.
    /// </value>
    [JsonProperty("effectiveTo")]
    [JsonConverter(typeof (IsoDateTimeConverter))]
    public DateTime EffectiveTo { get; set; }
  }
}