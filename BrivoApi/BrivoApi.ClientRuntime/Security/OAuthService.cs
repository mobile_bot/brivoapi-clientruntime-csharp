﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Core;
using Microsoft.Rest;

namespace BrivoApi.Security
{
  /// <summary>
  /// Support class for working with the Brivo Auth Server.
  /// </summary>
  /// <seealso>
  ///   <cref>BrivoApi.Core.BrivoApiServiceClientBase{BrivoApi.Security.OAuthService}</cref>
  /// </seealso>
  /// <seealso cref="BrivoApi.Security.IOAuthService" />
  internal class OAuthService : BrivoApiServiceClientBase<OAuthService>, IOAuthService
  {
    // support class to create query parameters for getting OAuth token.
    private class PasswordGrantQueryParameters : IQueryParametersAccessor
    {
      private readonly string userName;
      private readonly string password;
      internal PasswordGrantQueryParameters(string userName, string password)
      {
        this.userName = userName;
        this.password = password;
      }

      public IEnumerable<Tuple<string, string>> GetQueryParameters()
      {
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.GrantType, BrivoApiConstants.UriQueries.PasswordGrant);
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.UserName, this.userName);
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.Password, this.password);
      }
    }

    private class RefreshGrantQueryParameters : IQueryParametersAccessor
    {
      private readonly string refreshToken;
      internal RefreshGrantQueryParameters(string refreshToken)
      {
        this.refreshToken = refreshToken;
      }

      public IEnumerable<Tuple<string, string>> GetQueryParameters()
      {
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.GrantType, BrivoApiConstants.UriQueries.RefreshGrant);
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.RefreshToken, this.refreshToken);
      }

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="OAuthService"/> class.
    /// </summary>
    /// <param name="clientCredentials">The client credentials.</param>
    /// <param name="oAuthEndpoint">The o authentication endpoint.</param>
    public OAuthService(ServiceClientCredentials clientCredentials, Uri oAuthEndpoint) : base(clientCredentials, oAuthEndpoint)
    {
    }

    Task<AuthorizationToken> IOAuthService.GetNewAuthenticationTokenAsync(AccountCredentials accountCredentials, CancellationToken cancellationToken)
    {
      return Task.Factory.StartNew(() => {
        var response = this.GetNewTokenFromServerAsync(accountCredentials.CreateOAuthQueryParameters(), cancellationToken).GetAwaiter().GetResult();
        return new AuthorizationToken(this, accountCredentials.ClientId, DateTime.UtcNow, response.Body);
      }, cancellationToken);
    }

    Task<AuthorizationToken> IOAuthService.GetNewInvitationTokenAsync(InvitationCredentials invitationCredentials, CancellationToken cancellationToken)
    {
      return Task.Factory.StartNew(() =>
      {
        var response = this.GetNewTokenFromServerAsync(invitationCredentials.CreateOAuthQueryParameters(), cancellationToken).GetAwaiter().GetResult();
        // extract the client ID from the client credentials.
        var basicAuthCredentials = (BasicAuthenticationCredentials) this.Credentials;
        return new AuthorizationToken(this, basicAuthCredentials.UserName, DateTime.UtcNow, response.Body);
      }, cancellationToken);
    }

    Task<AuthorizationToken> IOAuthService.GetTokenAsync(AuthorizationToken authorizationToken, CancellationToken cancellationToken)
    {      
      if (authorizationToken.IsExpired())
      {
        return Task.Factory.StartNew(() =>
        {
          var response = this.RefreshTokenFromServerAsync(authorizationToken, cancellationToken).GetAwaiter().GetResult();
          authorizationToken.SetRefreshed(response.Body);
          return authorizationToken;
        }, cancellationToken);        
      }
      return Task.FromResult(authorizationToken);
    }

    Task<OAuthResponse> IOAuthService.AcquireTokenAsUserAsync(string userName, string password, CancellationToken cancellationToken)
    {
      return Task.Factory.StartNew(() => 
      {
          var response =
            this.GetNewTokenFromServerAsync(new PasswordGrantQueryParameters(userName, password), cancellationToken).GetAwaiter().GetResult();
          return response.Body;
      }, cancellationToken);
    }

    Task<OAuthResponse> IOAuthService.AcquireTokenByRefreshTokenAsync(string refreshToken, CancellationToken cancellationToken)
    {
      return Task.Factory.StartNew(() =>
      {
        var response = this.SendAsync<string, OAuthResponse>(HttpMethod.Post, BrivoApiConstants.UriPaths.OAuthToken,
          new RefreshGrantQueryParameters(refreshToken), null, nameof(IOAuthService.AcquireTokenByRefreshTokenAsync), cancellationToken).GetAwaiter().GetResult();
        return response.Body;
      }, cancellationToken);
    }

    private Task<HttpOperationResponse<OAuthResponse>> RefreshTokenFromServerAsync(AuthorizationToken authorizationToken,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendAsync<string, OAuthResponse>(HttpMethod.Post, BrivoApiConstants.UriPaths.OAuthToken,
        authorizationToken.CreateRefreshTokenQueryParameters(), null, nameof(this.RefreshTokenFromServerAsync), cancellationToken);
    }
    private Task<HttpOperationResponse<OAuthResponse>> GetNewTokenFromServerAsync(IQueryParametersAccessor queryParameters, CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendAsync<string, OAuthResponse>(HttpMethod.Post, BrivoApiConstants.UriPaths.OAuthToken,
        queryParameters, null, nameof(this.GetNewTokenFromServerAsync), cancellationToken);
    }
  }
}