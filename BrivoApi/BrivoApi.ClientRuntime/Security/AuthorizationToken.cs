﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Core;
using Microsoft.Rest;

namespace BrivoApi.Security
{
  /// <summary>
  /// Encapsulates the access and refresh tokens, making the token available to callers and refreshing it automatically if it expires.
  /// </summary>
  public class AuthorizationToken : ITokenProvider
  {
    private class RefreshQueryParameters : IQueryParametersAccessor
    {
      private readonly AuthorizationToken authorizationToken;

      internal RefreshQueryParameters(AuthorizationToken authorizationToken)
      {
        this.authorizationToken = authorizationToken;
      }

      public IEnumerable<Tuple<string, string>> GetQueryParameters()
      {
        yield return
          new Tuple<string, string>(BrivoApiConstants.UriQueries.GrantType,
            BrivoApiConstants.UriQueries.RefreshGrant);
        yield return
          new Tuple<string, string>(BrivoApiConstants.UriQueries.RefreshToken,
            this.authorizationToken.response.RefreshToken);
      }
    }

    private readonly string clientId;
    private readonly object authTokenLock = new object();

    private DateTimeOffset issued;
    private DateTimeOffset expires;
    private OAuthResponse response;
    private readonly TokenCredentials credentials;
    private readonly IOAuthService authService;

    /// <summary>
    /// Initializes a new instance of the <see cref="AuthorizationToken" /> class.
    /// </summary>
    /// <param name="authService">An interface to the auth service, to get new access tokens.</param>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="issuedUtc">The UTC datetime that the token was issued.</param>
    /// <param name="response">The response.</param>
    internal AuthorizationToken(IOAuthService authService, string clientId, DateTime issuedUtc, OAuthResponse response)
    {
      Requires.ArgumentNotNull(authService, nameof(authService));
      Requires.ArgumentNotNull(clientId, nameof(clientId));
      Requires.ArgumentNotNull(response, nameof(response));
      Requires.ArgumentNotNull(response.AccessToken, nameof(response.AccessToken));
      Requires.ArgumentNotNull(response.RefreshToken, nameof(response.RefreshToken));

      this.authService = authService;
      this.clientId = clientId;
      this.issued = issuedUtc;
      this.expires = this.issued.AddSeconds(response.ExpiresInSeconds);
      this.response = response;
      this.credentials = new TokenCredentials(this);
    }

    /// <summary>
    /// Determines whether this instance is expired.
    /// </summary>
    /// <returns>True if expired, false otherwise</returns>
    public bool IsExpired()
    {
      lock (this.authTokenLock)
      {
        var currentTime = DateTimeOffset.UtcNow;
        var expiryTime = this.expires;
        return (expiryTime - currentTime).TotalSeconds < 1;
      }
    }

    /// <summary>
    /// Gets the <see cref="DateTimeOffset"/> when the most recent access token expires.
    /// </summary>
    /// <value>
    /// The <see cref="DateTimeOffset"/> when the most recent access token expires.
    /// </value>
    public DateTimeOffset ExpiresUtc
    {
      get
      {
        lock (this.authTokenLock)
        {
          return this.expires;
        }
      }
    }

    /// <summary>
    /// Creates the refresh token query parameters.
    /// </summary>
    /// <returns>The refresh token query parameters.</returns>
    internal IQueryParametersAccessor CreateRefreshTokenQueryParameters()
    {
      lock (this.authTokenLock)
      {
        return new RefreshQueryParameters(this);
      }
    }

    /// <summary>
    /// Refreshes this token.
    /// </summary>
    /// <param name="latestResponse">The latest response.</param>
    internal void SetRefreshed(OAuthResponse latestResponse)
    {
      lock (this.authTokenLock)
      {
        this.issued = DateTimeOffset.UtcNow;
        this.expires = this.issued.AddSeconds(latestResponse.ExpiresInSeconds);
        this.response = latestResponse;
      }
    }

    /// <summary>
    /// Gets the refresh token.
    /// </summary>
    /// <value>
    /// The refresh token.
    /// </value>
    public string RefreshToken
    {
      get
      {
        lock (this.authTokenLock)
        {
          return this.response.RefreshToken;
        }
      }
    }

    /// <summary>
    /// Gets the OAuth response.
    /// </summary>
    /// <value>
    /// The OAuth response.
    /// </value>
    internal OAuthResponse OAuthResponse
    {
      get
      {
        lock (this.authTokenLock)
        {
          return this.response;
        }
      }
    }

    /// <summary>
    /// Gets the token credentials associated with this auth token.
    /// </summary>
    /// <value>
    /// The token credentials associated with this auth token.
    /// </value>
    public TokenCredentials Credentials
    {
      get
      {
        lock (this.authTokenLock)
        {
          return this.credentials;
        }
      }
    }

    /// <summary>
    /// Get the access token from the most recent OAuth response.
    /// </summary>
    public string AccessToken
    {
      get
      {
        lock (this.authTokenLock)
        {
          return this.response.AccessToken;
        }
      }
    }

    /// <summary>
    /// Invalidates this token.
    /// </summary>
    internal void Invalidate()
    {
      lock (this.authTokenLock)
      {
        this.expires = DateTimeOffset.MinValue;
      }
    }

    async Task<AuthenticationHeaderValue> ITokenProvider.GetAuthenticationHeaderAsync(CancellationToken cancellationToken)
    {
      if (this.IsExpired())
      {
        var newToken = await this.authService.AcquireTokenByRefreshTokenAsync(this.RefreshToken, cancellationToken);
        this.SetRefreshed(newToken);
      }
      return new AuthenticationHeaderValue("Bearer", this.AccessToken);
    }
  }
}