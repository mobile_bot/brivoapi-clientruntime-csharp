﻿using BrivoApi.Core;
using Microsoft.Rest;

namespace BrivoApi.Security
{
  /// <summary>
  /// Encapsulate basic client credentials.
  /// </summary>
  public sealed class ClientCredentials
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ClientCredentials" /> class.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    public ClientCredentials(string clientId, string clientSecret)
    {
      Requires.ArgumentNotNullOrWhitespace(clientId, "clientId");
      Requires.ArgumentNotNullOrWhitespace(clientSecret, "clientSecret");
      this.ClientId = clientId;
      this.ClientSecret = clientSecret;
    }

    /// <summary>
    /// Gets the client identifier.
    /// </summary>
    /// <value>
    /// The client identifier.
    /// </value>
    public string ClientId { get; private set; }
    /// <summary>
    /// Gets the client secret.
    /// </summary>
    /// <value>
    /// The client secret.
    /// </value>
    public string ClientSecret { get; private set; }

    /// <summary>
    /// Creates the client credentials.
    /// </summary>
    /// <returns>The client credentials</returns>
    public ServiceClientCredentials CreateClientCredentials()
    {
      return new BasicAuthenticationCredentials {UserName = this.ClientId, Password = this.ClientSecret};
    }
  }
}