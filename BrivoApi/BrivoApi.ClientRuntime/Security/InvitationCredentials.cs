﻿using System;
using System.Collections.Generic;
using BrivoApi.Core;

namespace BrivoApi.Security
{
  /// <summary>
  /// Encapsulate credentials for authenticating Brivo Phone Pass invitiation credentials.
  /// </summary>
  public sealed class InvitationCredentials
  {
    // support class to create query parameters for getting OAuth token.
    private class OAuthQueryParameters : IQueryParametersAccessor
    {
      private readonly InvitationCredentials credentials;
      internal OAuthQueryParameters(InvitationCredentials credentials)
      {
        this.credentials = credentials;
      }
      public IEnumerable<Tuple<string, string>> GetQueryParameters()
      {
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.GrantType, BrivoApiConstants.UriQueries.InvitationGrant);
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.Email, this.credentials.Email);
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.AccessCode, this.credentials.AccessCode);
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AccountCredentials" /> class.
    /// </summary>
    /// <param name="email">The email.</param>
    /// <param name="accessCode">The access code.</param>
    public InvitationCredentials(string email, string accessCode)
    {
      Requires.ArgumentNotNullOrWhitespace(email, "email");
      Requires.ArgumentNotNullOrWhitespace(accessCode, "accessCode");
      this.Email = email;
      this.AccessCode = accessCode;
    }

    /// <summary>
    /// Gets the email.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    public string Email { get; private set; }
    /// <summary>
    /// Gets the access code.
    /// </summary>
    /// <value>
    /// The access code.
    /// </value>
    public string AccessCode { get; private set; }

    /// <summary>
    /// Creates the query parameters needed to obtain an OAuth token.
    /// </summary>
    /// <returns>The query parameters accessor</returns>
    public IQueryParametersAccessor CreateOAuthQueryParameters()
    {
      return new OAuthQueryParameters(this);
    }
  }
}