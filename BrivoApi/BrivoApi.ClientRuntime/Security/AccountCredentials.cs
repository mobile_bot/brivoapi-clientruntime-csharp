using System;
using System.Collections.Generic;
using System.Net;
using BrivoApi.Core;
using Microsoft.Rest;

namespace BrivoApi.Security
{
  /// <summary>
  /// Encapsulate credentials for authenticating the account.
  /// </summary>
  public sealed class AccountCredentials
  {
    // support class to create query parameters for getting OAuth token.
    private class OAuthQueryParameters : IQueryParametersAccessor
    {
      private readonly AccountCredentials credentials;
      internal OAuthQueryParameters(AccountCredentials credentials)
      {
        this.credentials = credentials;
      }
      public IEnumerable<Tuple<string, string>> GetQueryParameters()
      {
        //make sure the request does not fail due to reserved chars
        var encodedUserName = WebUtility.UrlEncode(this.credentials.UserName);
        var encodedPassword = WebUtility.UrlEncode(this.credentials.Password);       
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.GrantType, BrivoApiConstants.UriQueries.PasswordGrant);
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.UserName, encodedUserName);
        yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.Password, encodedPassword);
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AccountCredentials" /> class.
    /// </summary>
    /// <param name="clientId">The client identifier.</param>
    /// <param name="clientSecret">The client secret.</param>
    /// <param name="userName">Name of the user.</param>
    /// <param name="password">The password.</param>
    public AccountCredentials(string clientId, string clientSecret, string userName, string password)
    {
      Requires.ArgumentNotNullOrWhitespace(clientId, "clientId");
      Requires.ArgumentNotNullOrWhitespace(clientSecret, "clientSecret");
      Requires.ArgumentNotNullOrWhitespace(userName, "userName");
      Requires.ArgumentNotNullOrWhitespace(password, "password");
      this.UserName = userName;
      this.Password = password;
      this.ClientId = clientId;
      this.ClientSecret = clientSecret;
    }

    /// <summary>
    /// Gets the client identifier.
    /// </summary>
    /// <value>
    /// The client identifier.
    /// </value>
    public string ClientId { get; private set; }
    /// <summary>
    /// Gets the client secret.
    /// </summary>
    /// <value>
    /// The client secret.
    /// </value>
    public string ClientSecret { get; private set; }
    /// <summary>
    /// Gets or sets the name of the user.
    /// </summary>
    /// <value>
    /// The name of the user.
    /// </value>
    public string UserName { get; private set; }
    /// <summary>
    /// Gets or sets the password.
    /// </summary>
    /// <value>
    /// The password.
    /// </value>
    public string Password { get; private set; }

    /// <summary>
    /// Creates the client credentials.
    /// </summary>
    /// <returns>The client credentials</returns>
    public ServiceClientCredentials CreateClientCredentials()
    {
      return new BasicAuthenticationCredentials {UserName = this.ClientId, Password = this.ClientSecret};
    }

    /// <summary>
    /// Creates the query parameters needed to obtain an OAuth token.
    /// </summary>
    /// <returns>The query parameters accessor</returns>
    public IQueryParametersAccessor CreateOAuthQueryParameters()
    {
      return new OAuthQueryParameters(this);
    }
  }
}