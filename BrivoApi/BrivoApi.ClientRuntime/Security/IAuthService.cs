﻿using System.Threading;
using System.Threading.Tasks;

namespace BrivoApi.Security
{
  /// <summary>
  /// Defines the interface to a service that provides OAuth tokens.
  /// </summary>
  internal interface IOAuthService
  {
    /// <summary>
    /// Gets the new authentication token asynchronous.
    /// </summary>
    /// <param name="accountCredentials">The account credentials.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that gets the authorization token.
    /// </returns>
    Task<AuthorizationToken> GetNewAuthenticationTokenAsync(AccountCredentials accountCredentials, CancellationToken cancellationToken);

    /// <summary>
    /// Gets the new invitation token asynchronous.
    /// </summary>
    /// <param name="invitationCredentials">The invitation credentials.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task that gets the authorization token.
    /// </returns>
    Task<AuthorizationToken> GetNewInvitationTokenAsync(InvitationCredentials invitationCredentials, CancellationToken cancellationToken);

    /// <summary>
    /// Gets the token asynchronously, refreshing the cached token if necessary.
    /// </summary>
    /// <param name="authorizationToken">The authentication token.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    Task<AuthorizationToken> GetTokenAsync(AuthorizationToken authorizationToken, CancellationToken cancellationToken);

    /// <summary>
    /// Acquires an OAuth token as a user.
    /// </summary>
    /// <param name="userName">Name of the user.</param>
    /// <param name="password">The password.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The OAuth response</returns>
    Task<OAuthResponse> AcquireTokenAsUserAsync(string userName, string password, CancellationToken cancellationToken);
    /// <summary>
    /// Acquires an OAuth token using a refresh token from a previously stored token.
    /// </summary>
    /// <param name="refreshToken">The refresh token.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The OAuth response</returns>
    Task<OAuthResponse> AcquireTokenByRefreshTokenAsync(string refreshToken, CancellationToken cancellationToken);
  }
}