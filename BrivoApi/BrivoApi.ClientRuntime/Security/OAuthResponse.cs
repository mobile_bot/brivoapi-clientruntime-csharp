﻿using Newtonsoft.Json;

namespace BrivoApi.Security
{
  /// <summary>
  /// Represents the OAuth token response
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class OAuthResponse
  {
    /// <summary>
    /// Gets or sets the access token.
    /// </summary>
    /// <value>
    /// The access token.
    /// </value>
    [JsonProperty("access_token")]
    public string AccessToken { get; set; }

    /// <summary>
    /// Gets or sets the type of the token.
    /// </summary>
    /// <value>
    /// The type of the token.
    /// </value>
    [JsonProperty("token_type")]
    public string TokenType { get; set; }

    /// <summary>
    /// Gets or sets the refresh token.
    /// </summary>
    /// <value>
    /// The refresh token.
    /// </value>
    [JsonProperty("refresh_token")]
    public string RefreshToken { get; set; }

    /// <summary>
    /// Gets or sets the expires in seconds.
    /// </summary>
    /// <value>
    /// The expires in seconds.
    /// </value>
    [JsonProperty("expires_in")]
    public int ExpiresInSeconds { get; set; }

    /// <summary>
    /// Gets or sets the scope.
    /// </summary>
    /// <value>
    /// The scope.
    /// </value>
    [JsonProperty("scope")]
    public string Scope { get; set; }

    /// <summary>
    /// Gets or sets the java web token identifier.
    /// </summary>
    /// <value>
    /// The java web token identifier.
    /// </value>
    [JsonProperty("jti")]
    public string JavaWebTokenId { get; set; }
  }
}
