﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Rest;

namespace BrivoApi.Security
{
  /// <summary>
  /// Represents the authorization context for connecting to the Brivo API.
  /// </summary>
  /// <remarks>
  /// This class handles obtaining an an access token and a refresh token and automatically refreshing
  /// the token when it is needed.
  /// </remarks>
  public class AuthorizationContext
  {
    /// <summary>
    /// Constructs the default production OAuth endpoint.
    /// </summary>
    /// <returns>The base URI for API authorization.</returns>
    internal static Uri ConstructOAuthEndpoint()
    {
      return new Uri("https://auth.brivo.com");
    }

    private readonly OAuthService service;
    private readonly AuthorizationToken token;

    /// <summary>
    /// Asynchronously creates the <see cref="AuthorizationContext"/> for working with an administrator account using the Brivo API.
    /// </summary>
    /// <param name="credentials">The account credentials.</param>
    /// <param name="oAuthEndpoint">The endpoint used to generate OAuth tokens.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>An operation that executes the request and returns the authorization context.</returns>
    public static async Task<AuthorizationContext> CreateAsync(AccountCredentials credentials, Uri oAuthEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = ConstructOAuthEndpoint();
      }

      var svc = new OAuthService(credentials.CreateClientCredentials(), oAuthEndpoint);
      var initialToken = await ((IOAuthService)svc).GetNewAuthenticationTokenAsync(credentials, cancellationToken);
      return new AuthorizationContext(svc, initialToken);
    }

    /// <summary>
    /// Asynchronously creates the <see cref="AuthorizationContext"/> for a new pass invitation using the Brivo API.
    /// </summary>
    /// <param name="serviceCredentials">Credentials to access the service.</param>
    /// <param name="invitationCredentials">The credentials to redeem a pass token.</param>
    /// <param name="oAuthEndpoint">The endpoint used to generate OAuth tokens.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>An operation that executes the request and returns the authorization context.</returns>
    internal static async Task<AuthorizationContext> CreateAsync(ServiceClientCredentials serviceCredentials, InvitationCredentials invitationCredentials, Uri oAuthEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = ConstructOAuthEndpoint();
      }

      var svc = new OAuthService(serviceCredentials, oAuthEndpoint);
      var initialToken = await ((IOAuthService)svc).GetNewInvitationTokenAsync(invitationCredentials, cancellationToken);
      return new AuthorizationContext(svc, initialToken);
    }

    /// <summary>
    /// Asynchronously creates the <see cref="AuthorizationContext"/> for a previously stored pass using the Brivo API.
    /// </summary>
    /// <param name="serviceCredentials">Credentials to access the service.</param>
    /// <param name="oAuthResponse">The OAuth response saved when the pass was redeemed.</param>
    /// <param name="oAuthEndpoint">The endpoint used to generate OAuth tokens.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>An operation that executes the request and returns the authorization context.</returns>
    internal static async Task<AuthorizationContext> CreateAsync(ClientCredentials serviceCredentials, OAuthResponse oAuthResponse, Uri oAuthEndpoint = null, CancellationToken cancellationToken = default(CancellationToken))
    {
      if (oAuthEndpoint == null)
      {
        oAuthEndpoint = ConstructOAuthEndpoint();
      }
      var svc = new OAuthService(serviceCredentials.CreateClientCredentials(), oAuthEndpoint);
      // since we are instantiating from a refresh token, create this initial token as already expired.
      var initialToken = new AuthorizationToken(svc, serviceCredentials.ClientId, DateTime.MinValue.ToUniversalTime(), oAuthResponse);
      var refreshedToken = await((IOAuthService)svc).GetTokenAsync(initialToken, cancellationToken);
      return new AuthorizationContext(svc, refreshedToken);
    }


    /// <summary>
    /// Creates an <see cref="AuthorizationContext"/> for working with the Brivo API.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="oAuthEndpoint">The o authentication endpoint.</param>
    /// <returns></returns>
    public static AuthorizationContext Create(AccountCredentials credentials, Uri oAuthEndpoint = null)
    {
      return Task.Factory.StartNew(
          () => CreateAsync(credentials, oAuthEndpoint).ConfigureAwait(false).GetAwaiter().GetResult())
          .GetAwaiter()
          .GetResult();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AuthorizationContext"/> class.
    /// </summary>
    /// <param name="service">The service.</param>
    /// <param name="initialToken">The initial token.</param>
    private AuthorizationContext(OAuthService service, AuthorizationToken initialToken)
    {
      this.service = service;
      this.token = initialToken;
    }

    /// <summary>
    /// Gets the latest <see cref="AuthorizationToken"/>.
    /// </summary>
    /// <returns>The latest <see cref="AuthorizationToken"/>.</returns>
    public AuthorizationToken GetToken()
    {
      return
        Task.Factory.StartNew(
          () => this.GetTokenAsync(CancellationToken.None).ConfigureAwait(false).GetAwaiter().GetResult())
          .GetAwaiter()
          .GetResult();
    }

    /// <summary>
    /// Gets the latest <see cref="AuthorizationToken"/> asynchronously.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>An operation to get the latest <see cref="AuthorizationToken"/>.</returns>
    public Task<AuthorizationToken> GetTokenAsync(CancellationToken cancellationToken = default(CancellationToken))
    {
      return ((IOAuthService)this.service).GetTokenAsync(this.token, cancellationToken);
    }

    /// <summary>
    /// Asynchronously ensures that the curernt token is valid.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>The operation to peform this request.</returns>
    public Task EnsureValidTokenAsync(CancellationToken cancellationToken = default(CancellationToken))
    {
      return ((IOAuthService)this.service).GetTokenAsync(this.token, cancellationToken);
    }

    /// <summary>
    /// Ensures that the token is valid.
    /// </summary>
    public void EnsureValidToken()
    {
      this.GetToken();
    }

    /// <summary>
    /// Immediately invalidates the current access token, forcing a refresh next time a token is requested.
    /// </summary>
    public void InvalidateToken()
    {
      this.token.Invalidate();
    }
  }
}