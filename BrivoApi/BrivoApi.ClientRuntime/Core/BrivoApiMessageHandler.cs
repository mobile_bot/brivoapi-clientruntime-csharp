using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Rest;
using Microsoft.Rest.TransientFaultHandling;
using ModernHttpClient;

namespace BrivoApi.Core
{
  /// <summary>
  /// The default message handler used
  /// the reason the handelr needs to be used is due to the fact that credentials might have to be checked on on retry condtions
  /// this is just a small shim to add line 37 to line 41
  /// </summary>
  public class BrivoApiMessageHandler : ModernHttpClient.NativeMessageHandler
  {
    public RetryPolicy RetryPolicy { get; set; }
    private readonly ServiceClientCredentials credentials;
    public BrivoApiMessageHandler(ServiceClientCredentials credentials,bool throwOnCaptiveNetwork, bool customSSLVerification, NativeCookieHandler cookieHandler = null):
      base(throwOnCaptiveNetwork, customSSLVerification, cookieHandler)
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      this.credentials = credentials;
    }

    /// <summary>
    /// Sends an HTTP request to the inner handler to send to the server as an asynchronous
    ///             operation. Retries request if needed based on Retry Policy.
    /// </summary>
    /// <param name="request">The HTTP request message to send to the server.</param><param name="cancellationToken">A cancellation token to cancel operation.</param>
    /// <returns>
    /// Returns System.Threading.Tasks.Task&lt;TResult&gt;. The 
    ///             task object representing the asynchronous operation.
    /// </returns>
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
      cancellationToken.ThrowIfCancellationRequested();
      await this.credentials.ProcessHttpRequestAsync(request, cancellationToken).ConfigureAwait(false);
      return await base.SendAsync(request, cancellationToken);
    }
  }
}