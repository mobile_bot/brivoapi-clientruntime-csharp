using System;
using System.Collections.Generic;

namespace BrivoApi.Core
{
  /// <summary>
  /// Defines the interface to an object that returns query parameters.
  /// </summary>
  public interface IQueryParametersAccessor
  {
    /// <summary>
    /// Gets the query parameters.
    /// </summary>
    /// <returns>The collection of name/value pairs for the query parameters.</returns>
    IEnumerable<Tuple<string, string>> GetQueryParameters();
  }
}