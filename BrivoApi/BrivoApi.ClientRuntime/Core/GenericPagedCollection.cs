﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BrivoApi.Core
{
  /// <summary>
  /// Represents a paged collection of resources.
  /// </summary>
  /// <typeparam name="TElement">The type of the element.</typeparam>
  [JsonObject(MemberSerialization.OptIn)]
  public class GenericPagedCollection<TElement>
  {
    /// <summary>
    /// Gets or sets the page.
    /// </summary>
    /// <value>
    /// The page.
    /// </value>
    [JsonProperty("data")]
    public List<TElement> Page { get; set; }

    /// <summary>
    /// Gets or sets the offset.
    /// </summary>
    /// <value>
    /// The offset.
    /// </value>
    [JsonProperty("offset")]
    public int Offset { get; set; }

    /// <summary>
    /// Gets or sets the size of the page.
    /// </summary>
    /// <value>
    /// The size of the page.
    /// </value>
    [JsonProperty("pageSize")]
    public int PageSize { get; set; }

    /// <summary>
    /// Gets or sets the total.
    /// </summary>
    /// <value>
    /// The total.
    /// </value>
    [JsonProperty("count")]
    public int Total { get; set; }
  }
}
