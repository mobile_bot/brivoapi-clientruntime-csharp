﻿namespace BrivoApi.Core
{
  /// <summary>
  /// Constants for working with the administrator client APIs.
  /// </summary>
  internal static class BrivoApiConstants
  {
    /// <summary>
    /// The URI endpoint paths
    /// </summary>
    internal static class UriPaths
    {
      internal const string OAuthToken = "/oauth/token";
      internal const string OAuthTokenFormat = "/oauth/token?=password&username={0}&password={1}";

      internal const string RunAsTokenFormat = "/administrators/{0}/run-as-token";

      internal const string GetAdministrators = "/administrators";
      internal const string GetAdministratorsFormat = "/administrators/{0}";
      internal const string GetAccountAdministrators = "/accounts/{0}/administrators";
      internal const string GetCurrentAdministrator = "/administrators/whoami";
      internal const string CreateAdministrator = "/administrators";
      internal const string CreateAdministratorWithPassword = "/administrators?password={0}";

      internal const string GetAccounts = "/accounts";
      internal const string GetAccountFormat = "/accounts/{0}";
      internal const string CreateAccount = "/accounts";
      internal const string CreateAccountWithPassword = "/accounts?password={0}";

      internal const string GetSites = "/sites";
      internal const string GetSiteFormat = "/sites/{0}";
      internal const string GetSiteCamerasFormat = "/sites/{0}/cameras";
      internal const string GetAccessPointsBySiteFormat = "/sites/{0}/access-points";

      internal const string GetCameras = "/cameras";
      internal const string GetCameraFormat = "/cameras/{0}?showStatus={1}";
      internal const string GetAccessPointsByCamerasFormat = "/cameras/{0}/access-points";
      internal const string GetCamerasByAccessPointsFormat = "/access-points/{0}/cameras";
      internal const string GetVideoClipsFormat = "/cameras/{0}/video?startTime={1}&endTime={2}";

      internal const string GetAccessPoints = "/access-points";
      internal const string GetAccessPointFormat = "/access-points/{0}";
      internal const string ActivateAccessPointFormat = "/access-points/{0}/activate";

      internal const string GetActivities = "/activities";
      internal const string GetActivityFormat = "/activities/{0}";

      internal const string GetControlPanels = "/control-panels";
      internal const string GetControlPanelFormat = "/control-panels/{0}";
     
      internal const string GetCredentialFormats = "/credentials/formats";
      internal const string GetCredentials = "/credentials";
      internal const string CreateCredential = "/credentials";
      internal const string GetCredentialFormat = "/credentials/{0}";
      internal const string GetUserByCredentialFormat = "/credentials/{0}/user";
      internal const string DeleteCredentialFormat = "/credentials/{0}";

      internal const string GetCustomFields = "/custom-fields";


      internal const string GetGroups = "/groups";
      internal const string CreateGroup = "/groups";
      internal const string DeleteGroupFormat = "/groups/{0}";
      internal const string GetGroupByIdFormat = "/groups/{0}";
      internal const string GetGroupUsersFormat = "/groups/{0}/users";
      internal const string AddUserToGroupFormat = "/groups/{0}/users/{1}";
      internal const string RemoveUserFromGroupFormat = "/groups/{0}/users/{1}";

      internal const string CreateApplication = "/applications";
      internal const string DeleteApplicationFormat = "/applications/{0}";
      internal const string GetApplicationFormat = "/applications/{0}";
      internal const string GetAuthorizedApplications = "/applications/authorized";
      internal const string GetApplications = "/applications";

      internal const string CreateEventSubscription = "/event-subscriptions";
      internal const string DeleteEventSubscriptionFormat = "/event-subscriptions/{0}";
      internal const string UpdateEventSubscriptionFormat = "/event-subscriptions/{0}";
      internal const string GetEventSubscriptionFormat = "/event-subscriptions/{0}";
      internal const string GetEventSubscriptions = "/event-subscriptions";

      internal const string CreateUser = "/users";
      internal const string UpdateUserFormat = "/users/{0}";
      internal const string DeleteUserFormat = "/users/{0}";
      internal const string GetUsers = "/users";
      internal const string GetUserFormat = "/users/{0}";
      internal const string IsUserSuspendedFormat = "/users/{0}/suspended";
      internal const string SetUserSuspendedFormat = "/users/{0}/suspended";
      internal const string GetUserGroupsFormat = "/users/{0}/groups";
      internal const string GetUserCredentialsFormat = "/users/{0}/credentials";
      internal const string AddUserCredentialFormat = "/users/{0}/credentials/{1}";
      internal const string RemoveUserCredentialFormat = "/users/{0}/credentials/{1}";
      internal const string GetUserByExternalIdFormat = "/users/{0}/external";
      internal const string GetUserPhotoFormat = "/users/{0}/photo";
      internal const string UpdateUserPhotoFormat = "/users/{0}/photo";
      internal const string DeleteUserPhotoFormat = "/users/{0}/photo";
      internal const string GetUserCustomFieldsFormat = "/users/{0}/custom-fields";
      internal const string DeleteUserCustomFieldFormat = "/users/{0}/custom-fields/{1}";
      internal const string UpdateUserCustomFieldFormat = "/users/{0}/custom-fields/{1}";

      internal const string GetAllProximityData = "/sites/proximity";
      internal const string SetSiteProximityWifiData = "/sites/{0}/proximity/wifi";

      internal const string GetAccountLogoFormat = "/accounts/{0}/logo";
      internal const string UsePassFormat = "/pass/{0}";
      internal const string RefreshPass = "/pass";

      internal const string UnlockAccessPointFormat = "/access-points/{0}/unlock";
      internal const string GetDigitalCredential = "/users/credentials/digital";
    }

    internal const string DateTimeFormat = "yyyy-MM-ddTHH:mm:sszzzz";

    internal static class UriQueries
    {
      internal const string PageOffset = "offset";
      internal const string PageSize = "pageSize";
      internal const string Filter = "filter";

      internal const string GrantType = "grant_type";
      internal const string RefreshGrant = "refresh_token";
      internal const string PasswordGrant = "password";
      internal const string InvitationGrant = "invitation";
      internal const string UserName = "username";
      internal const string Password = "password";
      internal const string Email = "email";
      internal const string AccessCode = "access_code";
      internal const string ClientId = "client_id";
      internal const string RefreshToken = "refresh_token";
    }
  }
}