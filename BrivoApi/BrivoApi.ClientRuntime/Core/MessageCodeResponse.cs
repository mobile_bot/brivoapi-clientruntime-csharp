﻿using Newtonsoft.Json;

namespace BrivoApi.Core
{
  /// <summary>
  /// Represents a message code response from the server.
  /// </summary>
  public class MessageCodeResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="MessageCodeResponse"/> class.
    /// </summary>
    public MessageCodeResponse()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="MessageCodeResponse"/> class.
    /// </summary>
    /// <param name="message">The message.</param>
    /// <param name="code">The code.</param>
    public MessageCodeResponse(string message, int code)
    {
      this.Message = message;
      this.Code = code;
    }

    /// <summary>
    /// Gets or sets the error.
    /// </summary>
    /// <value>
    /// The error.
    /// </value>
    [JsonProperty("message")]
    public string Message { get; set; }

    /// <summary>
    /// Gets or sets the error description.
    /// </summary>
    /// <value>
    /// The error description.
    /// </value>
    [JsonProperty("code")]
    public int Code { get; set; }
  }
}