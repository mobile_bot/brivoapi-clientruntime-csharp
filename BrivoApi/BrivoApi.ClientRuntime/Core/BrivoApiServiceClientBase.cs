﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Rest;
using Newtonsoft.Json;

namespace BrivoApi.Core
{
  /// <summary>
  /// A base class for all Brivo API service clients to extend and reuse.
  /// </summary>
  /// <typeparam name="TServiceClient">The type of the service client.</typeparam>
  public abstract class BrivoApiServiceClientBase<TServiceClient> : ServiceClient<TServiceClient> 
    where TServiceClient : ServiceClient<TServiceClient>
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="BrivoApiServiceClientBase{TServiceClient}"/> class.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="endpoint">The endpoint.</param>
    protected BrivoApiServiceClientBase(ServiceClientCredentials credentials, Uri endpoint) : base(new BrivoApiMessageHandler(credentials,false, true))
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      Requires.ArgumentNotNull(endpoint, "endpoint");
      this.Credentials = credentials;
      this.Endpoint = endpoint;    
      if (BrivoAccount.GlobalRetryPolicy != null)
        base.SetRetryPolicy(BrivoAccount.GlobalRetryPolicy);
      this.Credentials.InitializeServiceClient(this);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="BrivoApiServiceClientBase{TServiceClient}"/> class.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="handlers">The handlers.</param>
    protected BrivoApiServiceClientBase(ServiceClientCredentials credentials, Uri endpoint, params DelegatingHandler[] handlers)
      : base(new BrivoApiMessageHandler(credentials,false, true), handlers)
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      Requires.ArgumentNotNull(endpoint, "endpoint");
      this.Credentials = credentials;
      this.Endpoint = endpoint;     
      if (BrivoAccount.GlobalRetryPolicy != null)
        base.SetRetryPolicy(BrivoAccount.GlobalRetryPolicy);
      this.Credentials.InitializeServiceClient(this);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="BrivoApiServiceClientBase{TServiceClient}"/> class.
    /// </summary>
    /// <param name="credentials">The credentials.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="rootHandler">The root handler.</param>
    /// <param name="handlers">The handlers.</param>
    protected BrivoApiServiceClientBase(ServiceClientCredentials credentials, Uri endpoint, HttpClientHandler rootHandler, params DelegatingHandler[] handlers)
      : base(rootHandler, handlers)
    {
      Requires.ArgumentNotNull(credentials, "credentials");
      Requires.ArgumentNotNull(endpoint, "endpoint");
      this.Credentials = credentials;
      this.Endpoint = endpoint;
      if (BrivoAccount.GlobalRetryPolicy != null)
        base.SetRetryPolicy(BrivoAccount.GlobalRetryPolicy);
      this.Credentials.InitializeServiceClient(this);
    }

    /// <summary>
    /// Gets the credentials.
    /// </summary>
    /// <value>
    /// The credentials.
    /// </value>
    protected ServiceClientCredentials Credentials { get; private set; }
    /// <summary>
    /// Gets the endpoint.
    /// </summary>
    /// <value>
    /// The endpoint.
    /// </value>
    protected Uri Endpoint { get; private set; }

    /// <summary>
    /// Requests the specified body.
    /// </summary>
    /// <typeparam name="TRequest">The type of the request.</typeparam>
    /// <typeparam name="TResponse">The type of the response.</typeparam>
    /// <param name="body">The body.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    protected TResponse Request<TRequest, TResponse>(TRequest body,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return default(TResponse);
    }

    #region GET helpers
    /// <summary>
    /// Sends a HTTP GET request with no query parameters asynchronously.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> GetAsync<TResult>(string uriPath, string serviceName, CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<string, TResult>(HttpMethod.Get, uriPath, null, null, serviceName, cancellationToken);
    }
    /// <summary>
    /// Sends a HTTP GET request with query parameters asynchronously.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="queryParameterProvider">The query parameter provider.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> GetAsync<TResult>(string uriPath, IQueryParametersAccessor queryParameterProvider, string serviceName, CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<string, TResult>(HttpMethod.Get, uriPath, new[] { queryParameterProvider }, null, serviceName, cancellationToken);
    }
    /// <summary>
    /// Sends a HTTP GET request with a set of query parameters asynchronously.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="queryParameterProviders">The query parameter providers.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> GetAsync<TResult>(string uriPath, IEnumerable<IQueryParametersAccessor> queryParameterProviders, string serviceName, CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<string, TResult>(HttpMethod.Get, uriPath, queryParameterProviders, null, serviceName, cancellationToken);
    }
    #endregion

    #region POST helpers
    /// <summary>
    /// Sends a HTTP POST request asynchronously.
    /// </summary>
    /// <typeparam name="TBody">The type of the body.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="body">The body.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> PostAsync<TBody, TResult>(string uriPath, TBody body, string serviceName, CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<TBody, TResult>(HttpMethod.Post, uriPath, null, body, serviceName, cancellationToken);
    }
    #endregion

    #region PUT helpers
    /// <summary>
    /// Sends a HTTP PUT request asynchronously.
    /// </summary>
    /// <typeparam name="TBody">The type of the body.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="body">The body.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> PutAsync<TBody, TResult>(string uriPath, TBody body, string serviceName, CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<TBody, TResult>(HttpMethod.Put, uriPath, null, body, serviceName, cancellationToken);
    }
    #endregion

    #region DELETE helpers
    /// <summary>
    /// Sends a HTTP DELETE request asynchronously.
    /// </summary>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> DeleteAsync<TResult>(string uriPath, string serviceName, CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<string, TResult>(HttpMethod.Delete, uriPath, null, null, serviceName, cancellationToken);
    }
    #endregion

    #region Send helpers
    /// <summary>
    /// Sends a HTTP request asynchronously.
    /// </summary>
    /// <typeparam name="TBody">The type of the body.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="method">The method.</param>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="queryParameterProviders">The query parameter providers.</param>
    /// <param name="body">The body.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> SendAsync<TBody, TResult>(HttpMethod method, string uriPath,
      IQueryParametersAccessor queryParameterProviders, TBody body, string serviceName,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<TBody, TResult>(method, uriPath, new[] {queryParameterProviders}, body, serviceName, cancellationToken);
    }

    /// <summary>
    /// Sends a HTTP request asynchronously.
    /// </summary>
    /// <typeparam name="TBody">The type of the body.</typeparam>
    /// <typeparam name="TResult">The type of the result.</typeparam>
    /// <param name="method">The method.</param>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="queryParameterProviders">The query parameter providers.</param>
    /// <param name="body">The body.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>A task that executes the request and returns a response.</returns>
    protected Task<HttpOperationResponse<TResult>> SendAsync<TBody, TResult>(HttpMethod method, string uriPath,
      IEnumerable<IQueryParametersAccessor> queryParameterProviders, TBody body, string serviceName,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return this.SendCoreAsync<TBody, TResult>(method, uriPath, queryParameterProviders, body, serviceName, cancellationToken);
    }
    #endregion

    /// <summary>
    /// Gets the raw content asynchronously.
    /// </summary>
    /// <param name="uriPath">The URI path.</param>
    /// <param name="queryParameterProviders">The query parameter providers.</param>
    /// <param name="serviceName">Name of the service.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="headerAction">The header action.</param>
    /// <returns>A task to retrieve the HTTP content.</returns>
    protected async Task<HttpContent> GetRawContentAsync(string uriPath, IEnumerable<IQueryParametersAccessor> queryParameterProviders, string serviceName, CancellationToken cancellationToken, Action<HttpRequestHeaders> headerAction = null)
    {
      var method = HttpMethod.Get;
      string invocationId = null;
      var traceEnabled = ServiceClientTracing.IsEnabled;
      if (traceEnabled)
      {
        invocationId = ServiceClientTracing.NextInvocationId.ToString();
        var tracingParameters = new Dictionary<string, object> { { "body", null } };
        ServiceClientTracing.Enter(invocationId, this, serviceName, tracingParameters);
      }

      var uri = this.BuildEndpointUri(uriPath, queryParameterProviders);

      // Create HTTP transport objects
      var httpRequest = new HttpRequestMessage
      {
        Method = method,
        RequestUri = new Uri(uri),
      };

	  // Add api key if needed
	  if (!string.IsNullOrEmpty (BrivoAccount.ApiKey))
		httpRequest.Headers.Add ("api-key", BrivoAccount.ApiKey);

      // Custom headers
      if (headerAction != null)
      {
        headerAction(httpRequest.Headers);
      }

      // Set Credentials
      if (this.Credentials != null)
      {
        cancellationToken.ThrowIfCancellationRequested();
        await this.Credentials.ProcessHttpRequestAsync(httpRequest, cancellationToken).ConfigureAwait(false);
      }

      // Send Request
      if (traceEnabled)
      {
        ServiceClientTracing.SendRequest(invocationId, httpRequest);
      }
      cancellationToken.ThrowIfCancellationRequested();
      var httpResponse = await this.HttpClient.SendAsync(httpRequest, cancellationToken).ConfigureAwait(false);
      if (traceEnabled)
      {
        ServiceClientTracing.ReceiveResponse(invocationId, httpResponse);
      }

      var statusCode = httpResponse.StatusCode;
      cancellationToken.ThrowIfCancellationRequested();
      if (this.IsThrowingStatusCode(statusCode, method))
      {
        var responseErrorContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
        var ex = this.CreateException(httpRequest, httpResponse, responseErrorContent);
        if (traceEnabled)
        {
          ServiceClientTracing.Error(invocationId, ex);
        }
        throw ex;
      }

      var responseContent = httpResponse.Content;
      if (traceEnabled)
      {
        ServiceClientTracing.Exit(invocationId, responseContent);
      }
      return responseContent;
    }
    public async  Task<HttpOperationResponse<TResult>> PostMultipartFormDataContentAsync<TResult>(string uriPath, Func<MultipartFormDataContent> byteArrayContentFunction,string serviceName, CancellationToken cancellationToken)
    {
      var method = HttpMethod.Post;
      // Custom headers
      var requestContent = byteArrayContentFunction.Invoke();
      var uri = this.BuildEndpointUri(uriPath, null);
      var traceEnabled = ServiceClientTracing.IsEnabled;
      string invocationId = null;
      if (traceEnabled)
      {
        invocationId = ServiceClientTracing.NextInvocationId.ToString();
        var tracingParameters = new Dictionary<string, object> { { "body", requestContent } };
        ServiceClientTracing.Enter(invocationId, this, serviceName, tracingParameters);
      }
      // Create HTTP transport objects
      var httpRequest = new HttpRequestMessage
      {
        Method = method,
        RequestUri = new Uri(uri),
        Content = requestContent
      };

	  // Add api key if needed
	  if (!string.IsNullOrEmpty (BrivoAccount.ApiKey))
		httpRequest.Headers.Add ("api-key", BrivoAccount.ApiKey);

      // Set Credentials
      if (this.Credentials != null)
      {
        cancellationToken.ThrowIfCancellationRequested();
        await this.Credentials.ProcessHttpRequestAsync(httpRequest, cancellationToken).ConfigureAwait(false);
      }
      cancellationToken.ThrowIfCancellationRequested();
      var httpResponse = await this.HttpClient.SendAsync(httpRequest, cancellationToken).ConfigureAwait(false);
      if (traceEnabled)
      {
        ServiceClientTracing.ReceiveResponse(invocationId, httpResponse);
      }
      var statusCode = httpResponse.StatusCode;
      cancellationToken.ThrowIfCancellationRequested();
      var responseContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
      if (this.IsThrowingStatusCode(statusCode, method))
      {
        var ex = this.CreateException(httpRequest, httpResponse, responseContent);
        if (traceEnabled)
        {
          ServiceClientTracing.Error(invocationId, ex);
          ServiceClientTracing.Information($"ERROR BODY [{invocationId}]--->\n{responseContent}\n<---");
        }
        throw ex;
      }
      // Create Result
      var result = new HttpOperationResponse<TResult>
      {
        Request = httpRequest,
        Response = httpResponse
      };
      return result;
    }  
    private async Task<HttpOperationResponse<TResult>> SendCoreAsync<TBody, TResult>(HttpMethod method, string uriPath, IEnumerable<IQueryParametersAccessor> queryParameterProviders, TBody body, string serviceName, CancellationToken cancellationToken = default(CancellationToken))
    {     
      // trace
      var traceEnabled = ServiceClientTracing.IsEnabled;
      string invocationId = null;
      if (traceEnabled)
      {
        invocationId = ServiceClientTracing.NextInvocationId.ToString();
        var tracingParameters = new Dictionary<string, object> {{"body", body}};
        ServiceClientTracing.Enter(invocationId, this, serviceName, tracingParameters);
      }

      var uri = this.BuildEndpointUri(uriPath, queryParameterProviders);

      // Create HTTP transport objects
      var httpRequest = new HttpRequestMessage
      {
        Method = method,
        RequestUri = new Uri(uri)
      };

      // Serialize Request
      if (body != null)
      {
        var requestContent = JsonConvert.SerializeObject(body);
        httpRequest.Content = new StringContent(requestContent, Encoding.UTF8);
        httpRequest.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
      }

	  // Add api key if needed
	  if (!string.IsNullOrEmpty (BrivoAccount.ApiKey))
		httpRequest.Headers.Add ("api-key", BrivoAccount.ApiKey);

      // Send Request
      if (traceEnabled)
      {
        ServiceClientTracing.SendRequest(invocationId, httpRequest);
      }
      cancellationToken.ThrowIfCancellationRequested();

      var httpResponse   = await this.HttpClient.SendAsync(httpRequest, cancellationToken).ConfigureAwait(false);
      if (traceEnabled)
      {
        ServiceClientTracing.ReceiveResponse(invocationId, httpResponse);
      }
      var statusCode = httpResponse.StatusCode;
      cancellationToken.ThrowIfCancellationRequested();
      var responseContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
      if (this.IsThrowingStatusCode(statusCode, method))
      {
        var response = await this.AttemptUnauthorizedTokenRecoveryAsync(httpRequest,httpResponse, body, responseContent, invocationId, cancellationToken);
        //if the retry was ok then reset the items
        httpResponse = response.Item2;
        httpRequest = response.Item1;
        statusCode = httpResponse.StatusCode;
        responseContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
      }
    
      // Create Result
      var result = new HttpOperationResponse<TResult>
      {
        Request = httpRequest,
        Response = httpResponse
      };

      // Deserialize Response
      if (statusCode == HttpStatusCode.OK || statusCode == HttpStatusCode.Created)
      {
        if (!string.IsNullOrEmpty(responseContent))
        {
          try
          {
            result.Body = JsonConvert.DeserializeObject<TResult>(responseContent);
          }
          catch (JsonException jsonException)
          {
            if (traceEnabled)
            {
              ServiceClientTracing.Error(invocationId, jsonException);
            }

            throw this.CreateException(httpRequest, httpResponse, responseContent);
          }
          catch (Exception unhandledException)
          {
            if (traceEnabled)
            {
              ServiceClientTracing.Error(invocationId, unhandledException);
            }

            throw this.CreateException(httpRequest, httpResponse, responseContent);
          }
        }
      }

      if (traceEnabled)
      {
        ServiceClientTracing.Exit(invocationId, result);
      }
      return result;     
    }


    private async Task<Tuple<HttpRequestMessage, HttpResponseMessage>> AttemptUnauthorizedTokenRecoveryAsync<TBody>(HttpRequestMessage originalRequestMessage, HttpResponseMessage originalResponseMessage, TBody originalBody, string originalResponseContent, string invocationId , CancellationToken cancellationToken)
    {     
   
      var traceEnabled = ServiceClientTracing.IsEnabled;
      //Token expired mid flight, after it was added to the header but before the request was made 
      //TODO: ES 09/21/2016. It would have been nice to do this using Rest retrypolicy but no way of knowing why it was Unauthorized
      //the github code eats the message and only returns the code 
      //See: https://github.com/Azure/autorest/blob/a87323539370c54410a43d30ce1a1cacd2252886/src/client/Microsoft.Rest.ClientRuntime/RetryDelegatingHandler.cs
      if (originalResponseMessage.StatusCode == HttpStatusCode.Unauthorized && originalResponseContent.Contains("expired"))     
      {
        // Create HTTP transport objects
        var httpRequest = new HttpRequestMessage
        {
          Method = originalRequestMessage.Method,
          RequestUri = originalRequestMessage.RequestUri
        };
        // Serialize Request
        if (originalBody!=null)
        {
          var requestContent = JsonConvert.SerializeObject(originalBody);
          httpRequest.Content = new StringContent(requestContent, Encoding.UTF8);
          httpRequest.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
        }

        var httpResponse = await this.HttpClient.SendAsync(httpRequest, cancellationToken).ConfigureAwait(false);
        if (traceEnabled)
        {
          ServiceClientTracing.ReceiveResponse(invocationId, httpResponse);
        }
        var statusCode = httpResponse.StatusCode;
        cancellationToken.ThrowIfCancellationRequested();

        var reponseContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait(false);
        if (this.IsThrowingStatusCode(statusCode, httpRequest.Method))
        {
          throw this.GenerateError(invocationId, httpRequest, httpResponse, reponseContent, traceEnabled);
        }
        return new Tuple<HttpRequestMessage, HttpResponseMessage>(originalRequestMessage, httpResponse);
      }
      throw this.GenerateError(invocationId, originalRequestMessage, originalResponseMessage, originalResponseContent, traceEnabled);
    }

    private Exception GenerateError(string invocationId, HttpRequestMessage httpRequest, HttpResponseMessage httpResponse,
      string responseContent, bool traceEnabled)
    {
      var ex = this.CreateException(httpRequest, httpResponse, responseContent);
      if (traceEnabled)
      {
        ServiceClientTracing.Error(invocationId, ex);
        ServiceClientTracing.Information($"ERROR BODY [{invocationId}]--->\n{responseContent}\n<---");
      }
      return ex;
    }
    private bool IsThrowingStatusCode(HttpStatusCode statusCode, HttpMethod method)
    {
      if (HttpMethod.Get == method)
      {
        return statusCode != HttpStatusCode.OK;
      }

      if (HttpMethod.Post == method || HttpMethod.Put == method)
      {
        switch (statusCode)
        {
          case HttpStatusCode.OK:
          case HttpStatusCode.Created:
          case HttpStatusCode.Accepted:
          case HttpStatusCode.NoContent:
            return false;
        }
        return true;
      }

      if (HttpMethod.Delete == method)
      {
        switch (statusCode)
        {
          case HttpStatusCode.NotFound:
          case HttpStatusCode.OK:
          case HttpStatusCode.Accepted:
          case HttpStatusCode.NoContent:
            return false;
        }
        return true;
      }

      return statusCode >= HttpStatusCode.BadRequest;
    }

  
    private Exception CreateException(HttpRequestMessage httpRequest, HttpResponseMessage httpResponse, string responseContent)
    {
      var defaultErrorResponse = new ErrorResponse(httpResponse.StatusCode.ToString(), httpResponse.ReasonPhrase);

      // handle no content
      if (string.IsNullOrEmpty(responseContent))
      {
        return new HttpOperationException
        {
          Request = httpRequest,
          Response = httpResponse,
          Body = defaultErrorResponse
        };
      }

      if (httpResponse.StatusCode == HttpStatusCode.Unauthorized)
      {
        if (responseContent.Contains("error_description"))
        {
          return new HttpOperationException
          {
            Request = httpRequest,
            Response = httpResponse,
            Body = JsonConvert.DeserializeObject<ErrorResponse>(responseContent)
          };
        }

        return new HttpOperationException
        {
          Request = httpRequest,
          Response = httpResponse,
          Body = JsonConvert.DeserializeObject<UnauthorizedResponse>(responseContent)
        };                
      }

      // message-code content
      if (responseContent.Contains("message") && responseContent.Contains("code"))
      {
        return new HttpOperationException
        {
          Request = httpRequest,
          Response = httpResponse,
          Body = JsonConvert.DeserializeObject<MessageCodeResponse>(responseContent)
        };        
      }

      // error content
      if (responseContent.Contains("error") && responseContent.Contains("error_description"))
      {
        return new HttpOperationException
        {
          Request = httpRequest,
          Response = httpResponse,
          Body = JsonConvert.DeserializeObject<ErrorResponse>(responseContent)
        };
      }

      return new HttpOperationException
      {
        Request = httpRequest,
        Response = httpResponse,
        Body = responseContent
      };
    }


    private string BuildEndpointUri(string url, IEnumerable<IQueryParametersAccessor> queryParameterProviders)
    {
      if (queryParameterProviders == null)
      {
        queryParameterProviders = Enumerable.Empty<IQueryParametersAccessor>();
      }

      // Construct URL
      var queryParameters =
        queryParameterProviders.SelectMany(p => p.GetQueryParameters())
          .Select(t => string.Format("{0}={1}", t.Item1, t.Item2))
          .ToList();
      if (queryParameters.Count > 0)
      {
        url = url + "?" + string.Join("&", queryParameters);        
      }
      var baseUrl = this.Endpoint.AbsoluteUri;
      // Trim '/' character from the end of baseUrl and beginning of url.
      if (baseUrl[baseUrl.Length - 1] == '/')
      {
        baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
      }
      if (url[0] == '/')
      {
        url = url.Substring(1);
      }
      url = baseUrl + "/" + url;
      url = url.Replace(" ", "%20");
      return url;
    }
  }


}