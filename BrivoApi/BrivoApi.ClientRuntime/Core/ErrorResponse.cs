﻿using Newtonsoft.Json;

namespace BrivoApi.Core
{
  /// <summary>
  /// Represents an error response from the server.
  /// </summary>
  public class ErrorResponse
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ErrorResponse"/> class.
    /// </summary>
    public ErrorResponse()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ErrorResponse"/> class.
    /// </summary>
    /// <param name="error">The error.</param>
    /// <param name="description">The description.</param>
    public ErrorResponse(string error, string description)
    {
      this.Error = error;
      this.ErrorDescription = description;
    }

    /// <summary>
    /// Gets or sets the error.
    /// </summary>
    /// <value>
    /// The error.
    /// </value>
    [JsonProperty("error")]
    public string Error { get; set; }
    /// <summary>
    /// Gets or sets the error description.
    /// </summary>
    /// <value>
    /// The error description.
    /// </value>
    [JsonProperty("error_description")]
    public string ErrorDescription { get; set; }
  }}