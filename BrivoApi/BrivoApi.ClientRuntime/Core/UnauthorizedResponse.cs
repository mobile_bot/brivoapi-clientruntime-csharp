using Newtonsoft.Json;

namespace BrivoApi.Core
{
  /// <summary>
  /// Represents the response data when a request is unauthorized.
  /// </summary>
  public class UnauthorizedResponse
  {
    /// <summary>
    /// Gets or sets the timestamp.
    /// </summary>
    /// <value>
    /// The timestamp.
    /// </value>
    [JsonProperty("timestamp")]
    public string Timestamp { get; set; }
    /// <summary>
    /// Gets or sets the status.
    /// </summary>
    /// <value>
    /// The status.
    /// </value>
    [JsonProperty("status")]
    public int Status { get; set; }
    /// <summary>
    /// Gets or sets the error.
    /// </summary>
    /// <value>
    /// The error.
    /// </value>
    [JsonProperty("error")]
    public string Error { get; set; }
    /// <summary>
    /// Gets or sets the message.
    /// </summary>
    /// <value>
    /// The message.
    /// </value>
    [JsonProperty("message")]
    public string Message { get; set; }
    /// <summary>
    /// Gets or sets the path.
    /// </summary>
    /// <value>
    /// The path.
    /// </value>
    [JsonProperty("path")]
    public string Path { get; set; }
  }
}