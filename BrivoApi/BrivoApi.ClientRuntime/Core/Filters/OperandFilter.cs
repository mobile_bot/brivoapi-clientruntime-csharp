using System;
using System.Linq;
using System.Net;

namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// A filter that represents an operand expression for one or more values.
  /// </summary>
  /// <typeparam name="TValue">The type of the value.</typeparam>
  internal class OperandFilter<TValue> : Filter
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="OperandFilter{TValue}"/> class.
    /// </summary>
    /// <param name="filterName">Name of the filter.</param>
    /// <param name="operand">The operand.</param>
    /// <param name="val">The value.</param>
    internal OperandFilter(string filterName, Operands operand, TValue val)
    {
      this.FilterName = filterName;
      this.Operand = operand;
      this.Values = new [] { val };
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="OperandFilter{TValue}"/> class.
    /// </summary>
    /// <param name="filterName">Name of the filter.</param>
    /// <param name="operand">The operand.</param>
    /// <param name="val">The value.</param>
    internal OperandFilter(string filterName, Operands operand, TValue[] val)
    {
      this.FilterName = filterName;
      this.Operand = operand;
      this.Values = val;
    }
    /// <summary>
    /// Gets the name of the filter.
    /// </summary>
    /// <value>
    /// The name of the filter.
    /// </value>
    internal string FilterName { get; private set; }
    /// <summary>
    /// Gets the operand.
    /// </summary>
    /// <value>
    /// The operand.
    /// </value>
    internal Operands Operand { get; private set; }
    /// <summary>
    /// Gets the values.
    /// </summary>
    /// <value>
    /// The values.
    /// </value>
    internal TValue[] Values { get; private set; }

    /// <summary>
    /// Renders the filter string.
    /// </summary>
    /// <returns>The filter string.</returns>
    protected internal override string Render()
    {
      string valueString;
      if (typeof (TValue) == typeof (bool))
      {
        valueString = string.Join(",", this.Values).ToLowerInvariant();
      }
      else if (typeof (TValue) == typeof (DateTime))
      {
        valueString = string.Join(",", this.Values.Select(v => ((DateTime)(object)v).ToString("O")));
      }
      else if (typeof(TValue) == typeof(Enum))
      {
        valueString = string.Join(",", this.Values.Select(v => ((Enum)(object)v).ToString("G")));
      }
      else
      {
        valueString = string.Join(",", this.Values);
      }
      valueString = WebUtility.UrlEncode(valueString);
      var expression = $"{this.FilterName.ToLowerInvariant()}__{this.Operand.Render()}:{valueString}";
      return expression;
    }
  }
}