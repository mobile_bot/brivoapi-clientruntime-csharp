﻿namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Extension methods for working with filtes.
  /// </summary>
  public static class FilterExtensions
  {
    /// <summary>
    /// Combines two filters as a logical And.
    /// </summary>
    /// <param name="source">The source.</param>
    /// <param name="item">The item.</param>
    /// <returns>A new filter, the logical And of the source and item.</returns>
    public static Filter And(this Filter source, Filter item)
    {
      return new And(source, item);
    }
  }
}