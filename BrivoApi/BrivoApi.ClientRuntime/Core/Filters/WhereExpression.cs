using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Newtonsoft.Json;

namespace BrivoApi.Core.Filters
{
  internal class WhereExpression<TEntity, TValue> : IWhereExpression<TValue>
  {
    private readonly Expression<Func<TEntity, TValue>> fieldExpression;
    public WhereExpression(Expression<Func<TEntity, TValue>> fieldExpression)
    {
      this.fieldExpression = fieldExpression;
    }
    public Filter Equals(TValue val)
    {
      return this.WhereCore(this.fieldExpression, val, Operands.EqualTo);
    }

    public Filter NotEquals(TValue val)
    {
      return this.WhereCore(this.fieldExpression, val, Operands.NotEqualTo);
    }

    public Filter GreaterThan(TValue val)
    {
      return this.WhereCore(this.fieldExpression, val, Operands.GreaterThan);
    }

    public Filter LessThan(TValue val)
    {
      return this.WhereCore(this.fieldExpression, val, Operands.LessThan);
    }

    public Filter In(IEnumerable<TValue> val)
    {
      return this.WhereCore(this.fieldExpression, val.ToArray(), Operands.In);
    }

    public Filter NotIn(IEnumerable<TValue> val)
    {
      return this.WhereCore(this.fieldExpression, val.ToArray(), Operands.NotIn);
    }

    public Filter Before(DateTime val)
    {
      if (typeof (TValue) != typeof (DateTime))
      {
        throw new InvalidOperationException("The Before() predicate only works with DateTime values.");
      }
      return this.WhereCore(this.fieldExpression, (TValue)((object)val), Operands.LessThan);
    }

    public Filter After(DateTime val)
    {
      if (typeof(TValue) != typeof(DateTime))
      {
        throw new InvalidOperationException("The After() predicate only works with DateTime values.");
      }
      return this.WhereCore(this.fieldExpression, (TValue)((object)val), Operands.GreaterThan);
    }

    public Filter Between(DateTime val1, DateTime val2)
    {
      if (val1 > val2)
      {
        throw new ArgumentException("Expected val1 to be less than val2.", "val1");
      }
      if (typeof(TValue) != typeof(DateTime))
      {
        throw new InvalidOperationException("The After() predicate only works with DateTime values.");
      }
      if (val1 == val2)
      {
        return this.Equals((TValue) ((object) val1));
      }
      return new And(this.After(val1), this.Before(val2));
    }

    public Filter In(params TValue[] values)
    {
      return this.WhereCore(this.fieldExpression, values, Operands.In);
    }

    private Filter WhereCore(Expression<Func<TEntity, TValue>> fieldExpr, TValue val, Operands operand)
    {
      var name = ExtractFieldName(fieldExpr);
      return new OperandFilter<TValue>(name, operand, val);
    }
    private Filter WhereCore(Expression<Func<TEntity, TValue>> fieldExpr, TValue[] val, Operands operand)
    {
      var name = ExtractFieldName(fieldExpr);
      return new OperandFilter<TValue>(name, operand, val);
    }

    private static string ExtractFieldName(Expression<Func<TEntity, TValue>> fieldExpr)
    {
      if (fieldExpr.Body.NodeType != ExpressionType.MemberAccess &&
        fieldExpr.Body.NodeType != ExpressionType.Convert)
      {
        throw new ArgumentException("Invalid field expression.  Expected a simple member expression: u => u.FirstName",
          nameof(fieldExpr));
      }

      MemberInfo memberInfo=null;
      
      if (fieldExpr.Body.NodeType == ExpressionType.Convert)
      {
        var unaryExpression = (UnaryExpression) fieldExpr.Body;
        memberInfo = ((MemberExpression)unaryExpression.Operand).Member;
      }
      else
      {
        memberInfo = ((MemberExpression)fieldExpr.Body).Member;
      }
    
      var name = memberInfo.Name.ToLowerInvariant();

      var filterAttr = memberInfo.GetCustomAttribute<RenderFilterAttribute>();
      if (filterAttr != null)
      {
        name = filterAttr.FilterName;
      }
      else
      {
        var attr = memberInfo.GetCustomAttribute<JsonPropertyAttribute>();
        if (attr != null)
        {
          name = attr.PropertyName;
        }
      }
      return name;
    }
  }
}