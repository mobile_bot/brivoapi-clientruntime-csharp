﻿namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Defines the base interface to a custom filter.
  /// </summary>
  public abstract class CustomFilter
  {
    /// <summary>
    /// Renders this instance.
    /// </summary>
    /// <returns>The filter expression</returns>
    protected internal abstract string Render();
  }
}