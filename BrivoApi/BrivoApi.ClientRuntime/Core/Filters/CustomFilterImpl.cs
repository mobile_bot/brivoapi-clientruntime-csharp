namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Support class to delegate to a custom filter.
  /// </summary>
  internal class CustomFilterImpl : Filter
  {
    private readonly CustomFilter customFilter;

    /// <summary>
    /// Initializes a new instance of the <see cref="CustomFilterImpl"/> class.
    /// </summary>
    /// <param name="customFilter">The custom filter.</param>
    internal CustomFilterImpl(CustomFilter customFilter)
    {
      this.customFilter = customFilter;
    }

    /// <summary>
    /// Renders the filter expression.
    /// </summary>
    /// <returns></returns>
    protected internal override string Render()
    {
      return this.customFilter.Render();
    }
  }
}