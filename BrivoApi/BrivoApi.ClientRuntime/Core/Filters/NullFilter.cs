using System;
using System.Collections.Generic;

namespace BrivoApi.Core.Filters
{
  internal class NullFilter : Filter
  {
    protected internal override string Render()
    {
      return string.Empty;
    }

    public override IEnumerable<Tuple<string, string>> GetQueryParameters()
    {
      yield break;
    }
  }
}