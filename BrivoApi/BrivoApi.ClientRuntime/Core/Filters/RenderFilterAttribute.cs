﻿using System;

namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Override the default behavior when applying a query filter to use options from this class.
  /// </summary>
  [AttributeUsage(AttributeTargets.Property)]
  public class RenderFilterAttribute : Attribute
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="RenderFilterAttribute"/> class.
    /// </summary>
    /// <param name="filterName">Name of the filter.</param>
    public RenderFilterAttribute(string filterName)
    {
      this.FilterName = filterName;
    }

    /// <summary>
    /// Gets or sets the name of the filter.
    /// </summary>
    /// <value>
    /// The name of the filter.
    /// </value>
    public string FilterName { get; set; }
  }
}
