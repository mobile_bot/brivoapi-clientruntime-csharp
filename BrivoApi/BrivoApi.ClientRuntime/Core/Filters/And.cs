﻿namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Represents a logical AND of two filters.
  /// </summary>
  public class And : Filter
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="And"/> class.
    /// </summary>
    /// <param name="one">The one.</param>
    /// <param name="two">The two.</param>
    public And(Filter one, Filter two)
    {
      this.Filter1 = one;
      this.Filter2 = two;
    }

    /// <summary>
    /// Gets the filter1.
    /// </summary>
    /// <value>
    /// The filter1.
    /// </value>
    public Filter Filter1 { get; private set; }
    /// <summary>
    /// Gets the filter2.
    /// </summary>
    /// <value>
    /// The filter2.
    /// </value>
    public Filter Filter2 { get; private set; }
    /// <summary>
    /// Renders the filter expression.
    /// </summary>
    /// <returns></returns>
    internal protected override string Render()
    {
      return string.Format("{0};{1}", this.Filter1.Render(), this.Filter2.Render());
    }
  }
}