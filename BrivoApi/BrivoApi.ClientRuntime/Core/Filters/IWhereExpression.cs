﻿using System;
using System.Collections.Generic;

namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Defines the interface to a where expression used for search filters.
  /// </summary>
  /// <typeparam name="TValue">The type of the value.</typeparam>
  public interface IWhereExpression<in TValue>
  {
    /// <summary>
    /// Creates an equals filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values equal to the specified value.</returns>
    Filter Equals(TValue val);
    /// <summary>
    /// Creates a not equals filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values not equal to the specified value.</returns>
    Filter NotEquals(TValue val);
    /// <summary>
    /// Creates a greater than filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values greater than the specified value.</returns>
    Filter GreaterThan(TValue val);
    /// <summary>
    /// Creates a less than filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values less than the specified value.</returns>
    Filter LessThan(TValue val);
    /// <summary>
    /// Creates an in filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values in the specified list.</returns>
    Filter In(IEnumerable<TValue> val);
    /// <summary>
    /// Create a not in filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values not in the specified list.</returns>
    Filter NotIn(IEnumerable<TValue> val);

    /// <summary>
    /// Creates a before filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values before the specified date.</returns>
    Filter Before(DateTime val);
    /// <summary>
    /// Creates an after filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values after the specified date.</returns>
    Filter After(DateTime val);
    /// <summary>
    /// Creates a between filter.
    /// </summary>
    /// <param name="val1">The val1.</param>
    /// <param name="val2">The val2.</param>
    /// <returns>A filter to allow values between the specified dates</returns>
    Filter Between(DateTime val1, DateTime val2);
  }
}