﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Represents a filter.
  /// </summary>
  public abstract class Filter : IQueryParametersAccessor
  {
    /// <summary>
    /// Represents no filtering.
    /// </summary>
    public static readonly Filter None = new NullFilter();

    /// <summary>
    /// Renders the filter expression.
    /// </summary>
    /// <returns></returns>
    protected internal abstract string Render();

    /// <summary>
    /// Gets the query parameters for the filter.
    /// </summary>
    /// <returns>The filter query parameter</returns>
    public virtual IEnumerable<Tuple<string, string>> GetQueryParameters()
    {
      yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.Filter, this.Render());
    }

    /// <summary>
    /// Creates a where filter for an <see cref="int"/> expression.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <param name="fieldExpression">The field expression.</param>
    /// <returns>A new <see cref="WhereExpression{TEntity,TValue}"/>.</returns>
    public static IWhereExpression<int> Where<TEntity>(Expression<Func<TEntity, int>> fieldExpression)
    {
      return new WhereExpression<TEntity,int>(fieldExpression);
    }
    /// <summary>
    /// Creates a where filter for an <see cref="long"/> expression.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <param name="fieldExpression">The field expression.</param>
    /// <returns>A new <see cref="WhereExpression{TEntity,TValue}"/>.</returns>
    public static IWhereExpression<long> Where<TEntity>(Expression<Func<TEntity, long>> fieldExpression)
    {
      return new WhereExpression<TEntity, long>(fieldExpression);
    }
    /// <summary>
    /// Creates a where filter for an <see cref="float"/> expression.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <param name="fieldExpression">The field expression.</param>
    /// <returns>A new <see cref="WhereExpression{TEntity,TValue}"/>.</returns>
    public static IWhereExpression<float> Where<TEntity>(Expression<Func<TEntity, float>> fieldExpression)
    {
      return new WhereExpression<TEntity, float>(fieldExpression);
    }
    /// <summary>
    /// Creates a where filter for an <see cref="string"/> expression.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <param name="fieldExpression">The field expression.</param>
    /// <returns>A new <see cref="WhereExpression{TEntity,TValue}"/>.</returns>
    public static IWhereExpression<string> Where<TEntity>(Expression<Func<TEntity, string>> fieldExpression)
    {
      return new WhereExpression<TEntity, string>(fieldExpression);
    }
    /// <summary>
    /// Creates a where filter for an <see cref="DateTime"/> expression.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <param name="fieldExpression">The field expression.</param>
    /// <returns>A new <see cref="WhereExpression{TEntity,TValue}"/>.</returns>
    public static IWhereExpression<DateTime> Where<TEntity>(Expression<Func<TEntity, DateTime>> fieldExpression)
    {
      return new WhereExpression<TEntity, DateTime>(fieldExpression);
    }
    /// <summary>
    /// Creates a where filter for an <see cref="bool"/> expression.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <param name="fieldExpression">The field expression.</param>
    /// <returns>A new <see cref="WhereExpression{TEntity,TValue}"/>.</returns>
    public static IWhereExpression<bool> Where<TEntity>(Expression<Func<TEntity, bool>> fieldExpression)
    {
      return new WhereExpression<TEntity, bool>(fieldExpression);
    }
    /// <summary>
    /// Creates a where filter for an <see cref="Enum"/> expression.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <param name="fieldExpression">The field expression.</param>
    /// <returns>A new <see cref="WhereExpression{TEntity,TValue}"/>.</returns>
    public static IWhereExpression<Enum> Where<TEntity>(Expression<Func<TEntity, Enum>> fieldExpression)
    {
      return new WhereExpression<TEntity, Enum>(fieldExpression);
    }
    /// <summary>
    /// Creates a custom filter.
    /// </summary>
    /// <param name="customFilter">The custom filter.</param>
    /// <returns>A new custom filter.</returns>
    public static Filter Where(CustomFilter customFilter)
    {
      return new CustomFilterImpl(customFilter);
    }
  }
}
