﻿using System;

namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Support class for working with operand constants
  /// </summary>
  public static class OperandConstants
  {
    /// <summary>
    /// Constant for the equal to query parameter.
    /// </summary>
    public const string EqualTo = "eq";
    /// <summary>
    /// Constant for the not equal to query parameter.
    /// </summary>
    public const string NotEqualTo = "ne";
    /// <summary>
    /// Constant for the less than query parameter.
    /// </summary>
    public const string LessThan = "lt";
    /// <summary>
    /// Constant for the greater than query parameter.
    /// </summary>
    public const string GreaterThan = "gt";
    /// <summary>
    /// Constant for the in query parameter.
    /// </summary>
    public const string In = "eq";
    /// <summary>
    /// Constant for the not in query parameter.
    /// </summary>
    public const string NotIn = "ne";

    /// <summary>
    /// Renders the specified operand.
    /// </summary>
    /// <param name="operand">The operand.</param>
    /// <returns>The string value for this operand</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">Thrown if an unsupported operand is used.</exception>
    public static string Render(this Operands operand)
    {
      switch (operand)
      {
        case Operands.EqualTo:
          return EqualTo;
        case Operands.NotEqualTo:
          return NotEqualTo;
        case Operands.LessThan:
          return LessThan;
        case Operands.GreaterThan:
          return GreaterThan;
        case Operands.In:
          return In;
        case Operands.NotIn:
          return NotIn;
        default:
          throw new ArgumentOutOfRangeException("operand", operand, null);
      }
    }
  }
}