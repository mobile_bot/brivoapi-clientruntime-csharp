namespace BrivoApi.Core.Filters
{
  /// <summary>
  /// Defines the set of operands available for filtering.
  /// </summary>
  public enum Operands
  {
    /// <summary>
    /// Specifies the equal to operand.
    /// </summary>
    EqualTo,
    /// <summary>
    /// Specifies the not equal to operand.
    /// </summary>
    NotEqualTo,
    /// <summary>
    /// Specifies the less than operand.
    /// </summary>
    LessThan,
    /// <summary>
    /// Specifies the greater than operand.
    /// </summary>
    GreaterThan,
    /// <summary>
    /// Specifies the in operand.
    /// </summary>
    In,
    /// <summary>
    /// The not in operand.
    /// </summary>
    NotIn
  }
}