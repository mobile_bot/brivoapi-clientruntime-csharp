﻿using System;
using System.Diagnostics;
using System.Globalization;

namespace BrivoApi.Core
{
  /// <summary>
  /// Internal support class to check for preconditions or postcondition requirements in a 
  /// centralized and consistent way.
  /// </summary>
  /// <remarks>
  /// This class is not a replacement nor augmentation to code contracts shipped as part of .NET 4.0.
  /// Code contracts are the preferred mechanism for condition checking, this class is intended for simple 
  /// scenarios where code contracts are not (or cannot be) enabled.
  /// </remarks>
  internal static class Requires
  {
    /// <summary>
    /// Requires that an arguments value is not null.
    /// </summary>
    /// <typeparam name="T">The type of the object being tested for null.</typeparam>
    /// <param name="value">The value to test.</param>
    /// <param name="parameterName">The name of the parameter.</param>
    [DebuggerStepThrough]
    public static void ArgumentNotNull<T>(T value, string parameterName)
      where T : class
    {
      if (value == null)
      {
        throw new ArgumentNullException(parameterName);
      }
    }

    /// <summary>
    /// Requires that a string argument is not null or empty.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="parameterName">Name of the parameter.</param>
    [DebuggerStepThrough]
    public static void ArgumentNotNullOrEmpty(string value, string parameterName)
    {
      if (string.IsNullOrEmpty(value))
      {
        throw new ArgumentNullException(string.Format(CultureInfo.CurrentCulture, "{0} cannot be an empty string.", parameterName), parameterName);
      }
    }

    /// <summary>
    /// Requires that a string arguments is not null or whitespace.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <exception cref="ArgumentNullException">Thrown if the value is null or white space.</exception>
    [DebuggerStepThrough]
    public static void ArgumentNotNullOrWhitespace(string value, string parameterName)
    {
      if (string.IsNullOrWhiteSpace(value))
      {
        throw new ArgumentNullException(string.Format(CultureInfo.CurrentCulture, "{0} cannot be an empty string or contain only white space.", parameterName), parameterName);
      }
    }

    /// <summary>
    /// Requires that a string argument be not null and less than the specified maximum size.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <param name="size">The size.</param>
    [DebuggerStepThrough]
    public static void ArgumentNotNullMaximumSize(string value, string parameterName, int size)
    {
      ArgumentNotNull(value, parameterName);
      if (value.Length > size)
      {
        throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "{0} cannot be more than {1} characters.", parameterName, size), parameterName);
      }
    }

    /// <summary>
    /// Requires that a guid is not empty.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="parameterName">Name of the parameter.</param>
    /// <exception cref="System.ArgumentException">Thrown if the value is an empty guid.</exception>
    [DebuggerStepThrough]
    public static void ArgumentNotEmpty(Guid value, string parameterName)
    {
      if (value == Guid.Empty)
      {
        throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "{0} cannot be an empty Guid.", parameterName), parameterName);
      }
    }
  }
}
