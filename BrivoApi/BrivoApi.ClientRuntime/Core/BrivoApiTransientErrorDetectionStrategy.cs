using System;
using System.Net;
using Microsoft.Rest.TransientFaultHandling;

namespace BrivoApi.Core
{
  /// <summary>
  /// Default Http error detection strategy based on Http Status Code. added just to show that we can add Unauthorized
  /// </summary>
  public class BrivoApiTransientErrorDetectionStrategy : ITransientErrorDetectionStrategy
  {
    /// <summary>
    /// Returns true if status code in HttpRequestExceptionWithStatus exception is greater 
    /// than or equal to 500 and not NotImplemented (501) or HttpVersionNotSupported (505).
    /// </summary>
    /// <param name="ex">Exception to check against.</param>
    /// <returns>True if exception is transient otherwise false.</returns>
    public bool IsTransient(Exception ex)
    {
      if (ex != null)
      {
        HttpRequestWithStatusException httpException;
        if ((httpException = ex as HttpRequestWithStatusException) != null)
        {
          if (httpException.StatusCode == HttpStatusCode.RequestTimeout ||
              (httpException.StatusCode >= HttpStatusCode.InternalServerError &&
               httpException.StatusCode != HttpStatusCode.NotImplemented &&
               httpException.StatusCode != HttpStatusCode.HttpVersionNotSupported))
          {
            return true;
          }
          //TODO: it would have been nice to do this but no way of knowing why it was Unauthorized
          //the github code eats the message and only returns the code See: https://github.com/Azure/autorest/blob/a87323539370c54410a43d30ce1a1cacd2252886/src/client/Microsoft.Rest.ClientRuntime/RetryDelegatingHandler.cs
          if (httpException.StatusCode == HttpStatusCode.Unauthorized)
          {
            //return true
            return false;
          }
        }
      }
      return false;
    }
  }
}