using System;
using System.Collections.Generic;
using System.Globalization;

namespace BrivoApi.Core
{
  /// <summary>
  /// Represents information about a page of data.
  /// </summary>
  public class PageInfo : IQueryParametersAccessor
  {
    /// <summary>
    /// The default
    /// </summary>
    public static readonly PageInfo Default = new PageInfo();

    /// <summary>
    /// Initializes a new instance of the <see cref="PageInfo"/> class.
    /// </summary>
    public PageInfo()
    {
      this.Index = 0;
      this.Size = 100;//this is the maximum from onair
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PageInfo"/> class.
    /// </summary>
    /// <param name="index">The index.</param>
    /// <param name="size">The size.</param>
    public PageInfo(int index, int size)
    {
      this.Index = index;
      this.Size = size;
    }

    /// <summary>
    /// Gets the index.
    /// </summary>
    /// <value>
    /// The index.
    /// </value>
    public int Index { get; private set; }
    /// <summary>
    /// Gets the size.
    /// </summary>
    /// <value>
    /// The size.
    /// </value>
    public int Size { get; private set; }

    /// <summary>
    /// Calculates the offset.
    /// </summary>
    /// <returns>The offset</returns>
    public int CalculateOffset()
    {
      return this.Size * this.Index;
    }

    /// <summary>
    /// Gets the query parameters.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<Tuple<string, string>> GetQueryParameters()
    {
      yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.PageOffset, this.CalculateOffset().ToString(CultureInfo.CurrentCulture));
      yield return new Tuple<string, string>(BrivoApiConstants.UriQueries.PageSize, this.Size.ToString(CultureInfo.CurrentCulture));
    }
  }
}