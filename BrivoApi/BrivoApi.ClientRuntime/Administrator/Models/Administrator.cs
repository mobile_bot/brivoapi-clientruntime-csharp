﻿using BrivoApi.Core.Filters;
using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents an Administrator.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class Administrator
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>The identifier.</value>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the username.
    /// </summary>
    /// <value>The username.</value>
    [JsonProperty("username")]
    public string Username { get; set; }

    /// <summary>
    /// Gets or sets the first name.
    /// </summary>
    /// <value>The first name.</value>
    [JsonProperty("firstName")]
    public string FirstName { get; set; }

    /// <summary>
    /// Gets or sets the name of the middle.
    /// </summary>
    /// <value>The name of the middle.</value>
    [JsonProperty("middleName")]
    public string MiddleName { get; set; }

    /// <summary>
    /// Gets or sets the last name.
    /// </summary>
    /// <value>The last name.</value>
    [JsonProperty("lastName")]
    public string LastName { get; set; }

    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    /// <value>The email.</value>
    [JsonProperty("email")]
    public string Email { get; set; }

    /// <summary>
    /// Gets or sets the security question.
    /// </summary>
    /// <value>The security question.</value>
    [JsonProperty("securityQuestion")]
    public string SecurityQuestion { get; set; }

    /// <summary>
    /// Gets or sets the security answer.
    /// </summary>
    /// <value>The security question.</value>
    [JsonProperty("securityAnswer")]
    public string SecurityAnswer { get; set; }

    /// <summary>
    /// Gets or sets the timezone.
    /// </summary>
    /// <value>The timezone.</value>
    [JsonProperty("timezone")]
    public string Timezone { get; set; }

    /// <summary>
    /// Gets or sets a flag indicating if this admin is an account owner.
    /// </summary>
    /// <value>The flag indicating if this admin is an account owner.</value>
    [JsonProperty("owner")]
    public bool IsAccountOwner { get; set; }

    /// <summary>
    /// Gets or sets the account type. not mapped just used for filter
    /// </summary>
    /// <value>
    ///  the account account type
    /// </value>
    [RenderFilter("type")]
    public AdministratorType AdministratorType { get; set; }

  }
}

