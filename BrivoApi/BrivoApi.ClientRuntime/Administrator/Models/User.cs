﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a user
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class User
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the external identifier.
    /// </summary>
    /// <value>
    /// The external identifier.
    /// </value>
    [JsonProperty("externalId")]
    public string ExternalId { get; set; }

    /// <summary>
    /// Gets or sets the first name.
    /// </summary>
    /// <value>
    /// The first name.
    /// </value>
    [JsonProperty("firstName")]
    public string FirstName { get; set; }

    /// <summary>
    /// Gets or sets the name of the middle.
    /// </summary>
    /// <value>
    /// The name of the middle.
    /// </value>
    [JsonProperty("middleName")]
    public string MiddleName { get; set; }

    /// <summary>
    /// Gets or sets the last name.
    /// </summary>
    /// <value>
    /// The last name.
    /// </value>
    [JsonProperty("lastName")]
    public string LastName { get; set; }

    /// <summary>
    /// Gets or sets the pin.
    /// </summary>
    /// <value>
    /// The pin.
    /// </value>
    [JsonProperty("pin")]
    public string Pin { get; set; }

    /// <summary>
    /// Gets or sets the effective from.
    /// </summary>
    /// <value>
    /// The effective from.
    /// </value>
    [JsonProperty("effectiveFrom")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? EffectiveFrom { get; set; }

    /// <summary>
    /// Gets or sets the effective to.
    /// </summary>
    /// <value>
    /// The effective to.
    /// </value>
    [JsonProperty("effectiveTo")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? EffectiveTo { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether this <see cref="User"/> is suspended.
    /// </summary>
    /// <value>
    ///   <c>true</c> if suspended; otherwise, <c>false</c>.
    /// </value>
    [JsonProperty("suspended")]
    public bool Suspended { get; set; }

    /// <summary>
    /// Gets or sets the custom fields.
    /// </summary>
    /// <value>
    /// The custom fields.
    /// </value>
    [JsonProperty("customFields")]
    public List<CustomFieldDefinition> CustomFields { get; set; }
  }
}
