﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BrivoApi.Administrator.Models
{
  [JsonObject(MemberSerialization.OptIn)]
  public class Application
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the account id of the account which created the application.
    /// </summary>
    /// <value>
    ///  the account Id
    /// </value>
    [JsonProperty("accountId")]
    public int AccountId { get; set; }

    /// <summary>
    /// Gets or sets the name of the application.
    /// </summary>
    /// <value>
    ///  the name of the application.
    /// </value>
    [JsonProperty("name")]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the description of the application.
    /// </summary>
    /// <value>
    ///  the description of the application.
    /// </value>
    [JsonProperty("description")]
    public string Description { get; set; }

    /// <summary>
    /// Gets or sets the redirect Uri
    /// </summary>
    /// <value>
    ///  the redirect Uri
    /// </value>
    [JsonProperty("redirectUri")]
    public string RedirectUri { get; set; }

    /// <summary>
    /// Gets or sets the description of the methods used to request authorization
    /// </summary>
    /// <value>
    ///  the methods used to request authorization
    /// </value>
    [JsonProperty("grantTypes")]
    public IEnumerable<string> GrantTypes { get; set; }

    /// <summary>
    /// Gets or sets the client Id.
    /// </summary>
    /// <value>
    /// the client Id.
    /// </value>
    [JsonProperty("clientId")]
    public string ClientId { get; set; }

    /// <summary>
    /// Gets or sets the secret.
    /// </summary>
    /// <value>
    /// the access Token time.
    /// </value>
    [JsonProperty("secret")]
    public string Secret { get; set; }

    /// <summary>
    /// Gets or sets the description of the access Token time.
    /// </summary>
    /// <value>
    /// the access Token time.
    /// </value>
    [JsonProperty("accessTokenTtl")]
    public TimeSpan AccessTokenTtl { get; set; }

    /// <summary>
    /// Gets or sets the description of the refresh Token time.
    /// </summary>
    /// <value>
    /// the refresh Token time.
    /// </value>
    [JsonProperty("refreshTokenTtl")]
    public TimeSpan RefreshTokenTtl { get; set; }

    /// <summary>
    /// Gets or sets created date.
    /// </summary>
    /// <value>
    /// The created date.
    /// </value>
    [JsonProperty("created")]
    [JsonConverter(typeof (IsoDateTimeConverter))]
    public DateTime? Created { get; set; }

    /// <summary>
    /// Gets or sets updated date.
    /// </summary>
    /// <value>
    /// The updated date.
    /// </value>
    [JsonProperty("updated")]
    [JsonConverter(typeof (IsoDateTimeConverter))]
    public string Updated { get; set; }
  }
}