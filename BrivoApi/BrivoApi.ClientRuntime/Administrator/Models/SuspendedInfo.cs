using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents suspended info
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class SuspendedUserInfo
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="SuspendedUserInfo"/> class.
    /// </summary>
    public SuspendedUserInfo()
    {      
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="SuspendedUserInfo" /> class.
    /// </summary>
    /// <param name="suspended">if set to <c>true</c> [suspended].</param>
    public SuspendedUserInfo(bool suspended)
    {
      this.Suspended = suspended;
    }
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
    public int? Id { get; set; }
    /// <summary>
    /// Gets or sets the suspended.
    /// </summary>
    /// <value>
    /// The suspended.
    /// </value>
    [JsonProperty("suspended")]
    public bool Suspended { get; set; }
  }
}