﻿using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a custom field value
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class CustomFieldValue
  {
    /// <summary>
    /// Gets or sets the custom field identifier.
    /// </summary>
    /// <value>
    /// The custom field identifier.
    /// </value>
    [JsonProperty("id")]
    public long? CustomFieldId { get; set; }
    /// <summary>
    /// Gets or sets the name of the custom field.
    /// </summary>
    /// <value>
    /// The name of the custom field.
    /// </value>
    [JsonProperty("fieldName")]
    public string CustomFieldName { get; set; }
    /// <summary>
    /// Gets or sets the type of the custom field.
    /// </summary>
    /// <value>
    /// The type of the custom field.
    /// </value>
    [JsonProperty("fieldType")]
    public string CustomFieldType { get; set; }
    /// <summary>
    /// Gets or sets the value.
    /// </summary>
    /// <value>
    /// The value.
    /// </value>
    [JsonProperty("value")]
    public string Value { get; set; }
  }
}