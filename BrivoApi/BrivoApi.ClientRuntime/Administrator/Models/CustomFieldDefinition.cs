﻿using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a custom field
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class CustomFieldDefinition
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public long Id { get; set; }

    /// <summary>
    /// Gets or sets the name of the field.
    /// </summary>
    /// <value>
    /// The name of the field.
    /// </value>
    [JsonProperty("fieldName")]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the type of the field.
    /// </summary>
    /// <value>
    /// The type of the field.
    /// </value>
    [JsonProperty("fieldType")]
    public string Type { get; set; }

    /// <summary>
    /// Gets or sets the display order of the field.
    /// </summary>
    /// <value>
    /// The display order.
    /// </value>
    [JsonProperty("displayOrder")]
    public long DisplayOrder { get; set; }

    /// <summary>
    /// Gets or sets the validation pattern.
    /// </summary>
    /// <value>
    /// The validation pattern.
    /// </value>
    [JsonProperty("pattern")]
    public string Pattern { get; set; }
  }
}