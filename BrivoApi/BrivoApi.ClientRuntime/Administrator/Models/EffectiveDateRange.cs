﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BrivoApi.Administrator.Models
{
  public class EffectiveDateRange
  {
    /// <summary>
    /// Gets or sets the effective from date.
    /// </summary>
    /// <value>
    /// The the effective from date.
    /// </value>
    [JsonProperty("effectiveFrom")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? EffectiveFrom { get; set; }
    /// <summary>
    /// Gets or sets the effective to.
    /// </summary>
    /// <value>
    /// The the effective to date.
    /// </value>
    [JsonProperty("effectiveTo")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime? EffectiveTo { get; set; }
  }
}
