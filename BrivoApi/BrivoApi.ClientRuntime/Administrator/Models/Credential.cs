using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a credential.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class Credential
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the credential format.
    /// </summary>
    /// <value>
    /// The credential format.
    /// </value>
    [JsonProperty("credentialFormat")]
    public CredentialFormat CredentialFormat { get; set; }
    /// <summary>
    /// Gets or sets the reference identifier.
    /// </summary>
    /// <value>
    /// The reference identifier.
    /// </value>
    [JsonProperty("referenceId")]
    public string ReferenceId { get; set; }
    /// <summary>
    /// Gets or sets the encoded credential.
    /// </summary>
    /// <value>
    /// The encoded credential.
    /// </value>
    [JsonProperty("encodedCredential")]
    public string EncodedCredential { get; set; }
    /// <summary>
    /// Gets or sets the field values.
    /// </summary>
    /// <value>
    /// The field values.
    /// </value>
    [JsonProperty("fieldValues")]
    public CredentialField[] Fields { get; set; }
    /// <summary>
    /// Gets or sets the size.
    /// </summary>
    /// <value>
    /// The size.
    /// </value>
    [JsonProperty("size")]
    public int Size { get; set; }
    /// <summary>
    /// Gets or sets the account identifier.
    /// </summary>
    /// <value>
    /// The account identifier.
    /// </value>
    [JsonProperty("accountId")]
    public int AccountId { get; set; }
  }
}