﻿using BrivoApi.Core.Filters;
using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  ///  Represents an account
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class Account
  {
    /// <summary>
    /// Gets or sets the account Id
    /// </summary>
    /// <value>
    ///  the account Id
    /// </value>
    [JsonProperty("id")]   
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the account name
    /// </summary>
    /// <value>
    ///  the account name
    /// </value>
    [JsonProperty("name")] 
    public string Name { get; set; }
    /// <summary>
    /// Gets or sets the account number
    /// </summary>
    /// <value>
    ///  the account number
    /// </value>
    [JsonProperty("accountNumber")] 
    public string AccountNumber { get; set; }
    /// <summary>
    /// Gets or sets the account contact
    /// </summary>
    /// <value>
    ///  the account contact
    /// </value>
    [JsonProperty("contact")] 
    public Contact Contact { get; set; }
    /// <summary>
    /// Gets or sets the master admin
    /// </summary>
    /// <value>
    ///  the account master admin
    /// </value>
    [JsonProperty("masterAdmin")]
    public Administrator MasterAdmin { get; set; }
    /// <summary>
    /// Gets or sets the account type. not mapped just used for filter
    /// </summary>
    /// <value>
    ///  the account account type
    /// </value>
    [RenderFilter("type")]
    public string AccountType { get; set; }
    /// <summary>
    /// Gets or sets reference number 1 for the account
    /// </summary>
    /// <value>
    ///  the reference number 1 for the account
    /// </value>
    [JsonProperty("referenceNumber1")]
    public string ReferenceNumber1 { get; set; }
    /// <summary>
    /// Gets or sets reference number 2 for the account
    /// </summary>
    /// <value>
    ///  the reference number 2 for the account
    /// </value>
    [JsonProperty("referenceNumber2")]
    public string ReferenceNumber2 { get; set; }
    /// <summary>
    /// Gets or sets reference number 3 for the account
    /// </summary>
    /// <value>
    ///  the reference number 3 for the account
    /// </value>
    [JsonProperty("referenceNumber3")]
    public string ReferenceNumber3 { get; set; }
    /// <summary>
    /// Gets or sets reference number 4 for the account
    /// </summary>
    /// <value>
    ///  the reference number 4 for the account
    /// </value>
    [JsonProperty("referenceNumber4")]
    public string ReferenceNumber4 { get; set; }
    /// <summary>
    /// Gets or sets reference number 5 for the account
    /// </summary>
    /// <value>
    ///  the reference number 5 for the account
    /// </value>
    [JsonProperty("referenceNumber5")]
    public string ReferenceNumber5 { get; set; }
  }
}
