using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a site address
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class Address
  {
    /// <summary>
    /// Gets or sets the street address1.
    /// </summary>
    /// <value>
    /// The street address1.
    /// </value>
    [JsonProperty("streetAddress1")]
    public string StreetAddress1 { get; set; }
    /// <summary>
    /// Gets or sets the street address2.
    /// </summary>
    /// <value>
    /// The street address2.
    /// </value>
    [JsonProperty("streetAddress2")]
    public string StreetAddress2 { get; set; }
    /// <summary>
    /// Gets or sets the city.
    /// </summary>
    /// <value>
    /// The city.
    /// </value>
    [JsonProperty("city")]
    public string City { get; set; }
    /// <summary>
    /// Gets or sets the state.
    /// </summary>
    /// <value>
    /// The state.
    /// </value>
    [JsonProperty("state")]
    public string State { get; set; }
    /// <summary>
    /// Gets or sets the postal code.
    /// </summary>
    /// <value>
    /// The postal code.
    /// </value>
    [JsonProperty("postalCode")]
    public string PostalCode { get; set; }
  }
}