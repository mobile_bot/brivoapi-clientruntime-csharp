using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a credential format.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class CredentialFormat
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
    public string Name { get; set; }
    /// <summary>
    /// Gets or sets the fields.
    /// </summary>
    /// <value>
    /// The fields.
    /// </value>
    [JsonProperty("credentialFields", NullValueHandling = NullValueHandling.Ignore)]
    public CredentialField[] Fields { get; set; }
  }
}