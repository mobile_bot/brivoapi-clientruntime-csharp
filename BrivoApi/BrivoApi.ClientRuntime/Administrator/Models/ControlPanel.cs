﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a control panel.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class ControlPanel
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the name of the panel.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("panelName")]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the activated time.
    /// </summary>
    /// <value>
    /// The site identifier.
    /// </value>
    [JsonProperty("activated")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime Activated { get; set; }
    /// <summary>
    /// Gets or sets the updated time.
    /// </summary>
    /// <value>
    /// The site identifier.
    /// </value>
    [JsonProperty("updated")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime Updated { get; set; }
    /// <summary>
    /// Gets or sets the last contact time.
    /// </summary>
    /// <value>
    /// The site identifier.
    /// </value>
    [JsonProperty("lastContact")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime LastContact { get; set; }
    /// <summary>
    /// Gets or sets the firmware version.
    /// </summary>
    /// <value>
    /// The site identifier.
    /// </value>
    [JsonProperty("firmwareVersion")]
    public string FirmwareVersion { get; set; }
    /// <summary>
    /// Gets or sets the panel type.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("panelType")]
    public string PanelType { get; set; }
  }
}