﻿using System;
using BrivoApi.Core.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents an activity entry.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class Activity
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public long Id { get; set; }
    /// <summary>
    /// Gets or sets the occurred.
    /// </summary>
    /// <value>
    /// The occurred.
    /// </value>
    [JsonProperty("occurred")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTime Occurred { get; set; }
    /// <summary>
    /// Gets or sets the site identifier.
    /// </summary>
    /// <value>
    /// The site identifier.
    /// </value>
    [JsonProperty("siteId")]
    [RenderFilter("site")]
    public long SiteId { get; set; }
    /// <summary>
    /// Gets or sets the name of the site.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("siteName")]
    public string SiteName { get; set; }
    /// <summary>
    /// Gets or sets the user identifier.
    /// </summary>
    /// <value>
    /// The user identifier.
    /// </value>
    [JsonProperty("userId")]
    [RenderFilter("user")]
    public long UserId { get; set; }
    /// <summary>
    /// Gets or sets the object identifier.
    /// </summary>
    /// <value>
    /// The object identifier.
    /// </value>
    [JsonProperty("objectId")]
    [RenderFilter("object")]
    public long ObjectId { get; set; }
    /// <summary>
    /// Gets or sets the type of the object.
    /// </summary>
    /// <value>
    /// The type of the object.
    /// </value>
    [JsonProperty("objectType")]
    public string ObjectType { get; set; }
    /// <summary>
    /// Gets or sets the name of the object.
    /// </summary>
    /// <value>
    /// The name of the object.
    /// </value>
    [JsonProperty("objectName")]
    public string ObjectName { get; set; }
    /// <summary>
    /// Gets or sets the action type identifier.
    /// </summary>
    /// <value>
    /// The action type identifier.
    /// </value>
    [JsonProperty("actionTypeId")]
    public long ActionTypeId { get; set; }
    /// <summary>
    /// Gets or sets the description.
    /// </summary>
    /// <value>
    /// The description.
    /// </value>
    [JsonProperty("description")]
    public string Description { get; set; }
  }
}
