using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a site
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class Site
  {
    /// <summary>
    /// Gets or sets the name of the site.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("siteName")]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the address.
    /// </summary>
    /// <value>
    /// The address.
    /// </value>
    [JsonProperty("address")]
    public Address Address { get; set; }

    /// <summary>
    /// Gets or sets the time zone string.
    /// </summary>
    /// <value>
    /// The time zone string.
    /// </value>
    [JsonProperty("timeZone")]
    public string TimeZoneString { get; set; }
  }
}