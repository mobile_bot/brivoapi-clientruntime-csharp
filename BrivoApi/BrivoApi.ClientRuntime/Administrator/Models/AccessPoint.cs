﻿using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents an access point such as a door.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class AccessPoint
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the name of the site.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("name")]
    public string Name { get; set; }
    /// <summary>
    /// Gets or sets the site identifier.
    /// </summary>
    /// <value>
    /// The site identifier.
    /// </value>
    [JsonProperty("controlPanelId")]
    public int ControlPanelId { get; set; }
    /// <summary>
    /// Gets or sets the site identifier.
    /// </summary>
    /// <value>
    /// The site identifier.
    /// </value>
    [JsonProperty("siteId")]
    public int SiteId { get; set; }
    /// <summary>
    /// Gets or sets the name of the site.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("siteName")]
    public string SiteName { get; set; }
    /// <summary>
    /// Gets or sets a value indicating whether this access point can be remote activation.
    /// </summary>
    /// <value>
    /// <c>true</c> if this instance is remote activation enabled; otherwise, <c>false</c>.
    /// </value>
    [JsonProperty("activationEnabled")]
    public bool IsRemoteActivationEnabled { get; set; }
  }
}