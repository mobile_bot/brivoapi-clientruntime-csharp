﻿using System;
using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  [JsonObject(MemberSerialization.OptIn)]
  public class Proximity
  {
    //
    // Properties
    //
    [JsonProperty ("id")]
    public long Id { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("proximity")]
    public ProximityDefinition ProximityData { get; set; } 	
  }
}