﻿using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a Video Provider
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class VideoProvider
  {
    /// <summary>
    /// Gets or sets the Video Provider Type
    /// </summary>
    [JsonProperty("type")]
    public VideoProviderType Type { get; set; }

    /// <summary>
    /// Gets or sets the Video Provider Type Version
    /// </summary>
    [JsonProperty("version")]
    public string Version { get; set; }
  }
}
