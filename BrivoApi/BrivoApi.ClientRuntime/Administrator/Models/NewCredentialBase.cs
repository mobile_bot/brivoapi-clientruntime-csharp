using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a credential.
  /// </summary>
  /// <remarks>
  /// Use either the <see cref="NewCredentialWithEncodedValue"/> or <see cref="NewCredentialWithFieldValues"/> type
  /// when creating a new credential.
  /// </remarks>
  [JsonObject(MemberSerialization.OptIn)]
  public abstract class NewCredentialBase
  {
    /// <summary>
    /// Gets or sets the credential format.
    /// </summary>
    /// <value>
    /// The credential format.
    /// </value>
    [JsonProperty("credentialFormat")]
    public CredentialFormat CredentialFormat { get; set; }

    /// <summary>
    /// Gets or sets the reference identifier.
    /// </summary>
    /// <value>
    /// The reference identifier.
    /// </value>
    [JsonProperty("referenceId")]
    public string ReferenceId { get; set; }
  }
}