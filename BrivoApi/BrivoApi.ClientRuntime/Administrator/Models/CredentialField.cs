using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a field within a credential.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class CredentialField
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }
    /// <summary>
    /// Gets or sets the value.
    /// </summary>
    /// <value>
    /// The value.
    /// </value>
    [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
    public string Value { get; set; }
    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    [JsonProperty("name", NullValueHandling = NullValueHandling.Ignore)]
    public string Name { get; set; }
  }
}