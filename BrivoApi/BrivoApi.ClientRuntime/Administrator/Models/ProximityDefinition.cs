using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace BrivoApi.Administrator.Models
{
  [JsonObject(MemberSerialization.OptIn)]
  public class ProximityDefinition
  {
    //
    // Properties
    //
    [JsonProperty ("wifi")]
    public IList<WifiDefinition> Wifi { get; set; }
  }
}