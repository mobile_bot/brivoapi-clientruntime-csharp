﻿using BrivoApi.Administrator.Converters;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Defines the type of video provider.
  /// </summary>
  [JsonConverter(typeof(VideoProviderTypeEnumConverter))]
  public enum VideoProviderType
  {
    /// <summary>
    /// Specifies Unknown Camera Type
    /// </summary>
    Unknown,
    /// <summary>
    /// Specifies OnAir Camera Type
    /// </summary>
    [EnumMember(Value = "ONAIR")]
    OnAir,
    /// <summary>
    /// Specifies Eagle Eye Camera Type
    /// </summary>
    [EnumMember(Value = "EAGLE_EYE")]
    EagleEye
  }
}
