﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Defines the status of the Camera.
  /// </summary>
  [JsonConverter(typeof(StringEnumConverter))]
  public enum CameraStatus
  {
    /// <summary>
    /// Specifies Unknown Status.
    /// </summary>
    [EnumMember(Value = "UNKNOWN")]
    Unknown,
    /// <summary>
    /// Specifies Camera is Connected.
    /// </summary>
    [EnumMember(Value = "CONNECTED")]
    Connected,
    /// <summary>
    /// Specifies Camera is Disconnected.
    /// </summary>
    [EnumMember(Value = "DISCONNECTED")]
    Disconnected
  }
}
