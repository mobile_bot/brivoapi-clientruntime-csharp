﻿using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a Camera
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class Camera
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>The identifier.</value>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>The name.</value>
    [JsonProperty("name")]
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the live view URL.
    /// </summary>
    /// <value>The live view URL.</value>
    [JsonProperty("LiveViewUrl")]
    public string LiveViewUrl { get; set; }

    /// <summary>
    /// Gets or sets the video provider.
    /// </summary>
    /// <value>The video provider.</value>
    [JsonProperty("videoProvider")]
    public VideoProvider VideoProvider { get; set; }

    /// <summary>
    /// Gets or sets the status.
    /// </summary>
    /// <value>The status.</value>
    [JsonProperty("status")]
    public CameraStatus Status { get; set; }

    /// <summary>
    /// Gets or sets the site.
    /// </summary>
    /// <value>The site.</value>
    [JsonProperty("site")]
    public Site site { get; set; }

  }

}
  