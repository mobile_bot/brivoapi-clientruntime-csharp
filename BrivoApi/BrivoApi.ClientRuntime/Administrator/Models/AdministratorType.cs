﻿namespace BrivoApi.Administrator.Models
{
  public enum AdministratorType
  {
    Csr,
    Dealer,
    Master,
    Senior,
    Assistant
  }
}