﻿using System;
using System.Linq;
using BrivoApi.Core.Filters;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// A filter for activity event types.
  /// </summary>
  public class ActivityEventTypesFilter : CustomFilter
  {
    /// <summary>
    /// Create an equals filter for the specified event type.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <returns>A new equals filter.</returns>
    public static ActivityEventTypesFilter Equals(ActivityEventTypes value)
    {
      return new ActivityEventTypesFilter(value);
    }

    /// <summary>
    /// Create an in filter for the specified event types.
    /// </summary>
    /// <param name="values">The values.</param>
    /// <returns>A new in filter.</returns>
    public static ActivityEventTypesFilter In(params ActivityEventTypes[] values)
    {
      return new ActivityEventTypesFilter(values);
    }

    private const string EventTypeFilterName = "event_type";
    private const string CameraType = "camera";
    private const string ExceptionType = "exception";
    private const string NonExceptionType = "non_exception";

    private readonly ActivityEventTypes[] values;
    
    private ActivityEventTypesFilter(ActivityEventTypes value)
      : this(new [] {  value})
    {
    }

    private ActivityEventTypesFilter(params ActivityEventTypes[] values)
    {
      this.values = values;
    }

    /// <summary>
    /// Renders this instance.
    /// </summary>
    /// <returns>
    /// The filter expression
    /// </returns>
    protected internal override string Render()    
    {
      if (this.values.Contains(ActivityEventTypes.All))
      {
        // include all events.
        return string.Empty;
      }

      var valueString = string.Join(",", this.values.Select(Render));
      return string.Format("{0}:{1}", EventTypeFilterName, valueString);
    }

    private static string Render(ActivityEventTypes value)
    {
      switch (value)
      {
        case ActivityEventTypes.CameraEvents:
          return CameraType;
        case ActivityEventTypes.ExceptionEvents:
          return ExceptionType;
        case ActivityEventTypes.NonExceptionEvents:
          return NonExceptionType;
        default:
          throw new ArgumentOutOfRangeException("value", value, null);
      }
    }
  }
}