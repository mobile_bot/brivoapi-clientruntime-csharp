using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
    /// <summary>
    /// Represents an account contact
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Contact
    {
        /// <summary>
        /// Gets or sets the street address1.
        /// </summary>
        /// <value>
        /// The street address1.
        /// </value>
        [JsonProperty("streetAddress1")]
        public string StreetAddress1 { get; set; }
        /// <summary>
        /// Gets or sets the street address2.
        /// </summary>
        /// <value>
        /// The street address2.
        /// </value>
        [JsonProperty("streetAddress2")]
        public string StreetAddress2 { get; set; }
        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>
        /// The city.
        /// </value>
        [JsonProperty("city")]
        public string City { get; set; }
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        [JsonProperty("state")]
        public string State { get; set; }
        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>
        /// The postal code.
        /// </value>
        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }
        /// <summary>
        /// Gets or sets the country.
        /// </summary>
        /// <value>
        /// The state.
        /// </value>
        [JsonProperty("country")]
        public string Country { get; set; }
        /// <summary>
        ///  Gets or sets the telephone number.
        /// </summary>
        /// <value>
        /// The telephone Number.
        /// </value>
        [JsonProperty("telephoneNumber")]
        public string TelephoneNumber { get; set; }
        /// <summary>
        ///  Gets or sets the telephone number extension.
        /// </summary>
        /// <value>
        /// The telephone Number.
        /// </value>
        [JsonProperty("telephoneExtension")]
        public string TelephoneNumberExtension { get; set; }
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>
        /// The first name.
        /// </value>
        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>
        /// The last name.
        /// </value>
        [JsonProperty("lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}