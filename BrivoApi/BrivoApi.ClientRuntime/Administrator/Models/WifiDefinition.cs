using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace BrivoApi.Administrator.Models
{
  [JsonObject(MemberSerialization.OptIn)]
  public class WifiDefinition
  {
    //
    // Properties
    //
    [JsonProperty ("ssid")]
    public string Ssid { get; set; }	
  }
}