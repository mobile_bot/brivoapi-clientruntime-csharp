﻿using BrivoApi.Administrator.Converters;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Defines the video format
  /// </summary>
  [JsonConverter(typeof(VideoFormatEnumConverter))]
  public enum VideoFormat
  {
    /// <summary>
    /// Specifies an unknown format.
    /// </summary>
    Unknown,
    /// <summary>
    /// Specifies an MP4 format.
    /// </summary>
    [EnumMember(Value = "mp4")]
    MP4
  }
}
