﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a video clip response.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class VideoClipResponse
  {
    /// <summary>
    /// Gets or sets the start time.
    /// </summary>
    [JsonProperty("startTime")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTimeOffset StartTime { get; set; }

    /// <summary>
    /// Gets or sets the end time.
    /// </summary>
    [JsonProperty("endTime")]
    [JsonConverter(typeof(IsoDateTimeConverter))]
    public DateTimeOffset EndTime { get; set; }

    /// <summary>
    /// Gets or sets the camera.
    /// </summary>
    [JsonProperty("camera")]
    public Camera Camera { get; set; }

    /// <summary>
    /// Gets or sets the list of video clips.
    /// </summary>
    [JsonProperty("videoClips")]
    public IList<VideoClip> VideoClips { get; set; }
  }
}
