﻿namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Defines the types of activity events.
  /// </summary>
  public enum ActivityEventTypes
  {
    /// <summary>
    /// Specifies all events.
    /// </summary>
    All,
    /// <summary>
    /// Specifies camera events.
    /// </summary>
    CameraEvents,
    /// <summary>
    /// Specifies exception events
    /// </summary>
    ExceptionEvents,
    /// <summary>
    /// Specifies non-exception events
    /// </summary>
    NonExceptionEvents
  }
}