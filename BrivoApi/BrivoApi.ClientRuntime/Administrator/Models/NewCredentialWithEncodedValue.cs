using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a new credential with its encoded value provided.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class NewCredentialWithEncodedValue : NewCredentialBase
  {
    /// <summary>
    /// Gets or sets the encoded credential.
    /// </summary>
    /// <value>
    /// The encoded credential.
    /// </value>
    [JsonProperty("encodedCredential")]
    public string EncodedValue { get; set; }
  }
}