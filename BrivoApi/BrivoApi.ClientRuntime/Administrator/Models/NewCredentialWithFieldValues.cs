using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a new credential created with individual field values.
  /// </summary>
  /// <remarks>
  /// The id and value are required for specifying the credential fields.
  /// Each credentialFormat uses a different set of Credential Fields even if they share the same name.
  /// If creating a credential of type "Swipe To Enroll" the encodedCredential must match the exact length 
  /// expected to come from the reader when converted from hexadecimal to binary.
  /// </remarks>
  [JsonObject(MemberSerialization.OptIn)]
  public class NewCredentialWithFieldValues : NewCredentialBase
  {
    /// <summary>
    /// Gets or sets the field values.
    /// </summary>
    /// <value>
    /// The field values.
    /// </value>
    [JsonProperty("fieldValues")]
    public CredentialField[] Fields { get; set; }
  }
}