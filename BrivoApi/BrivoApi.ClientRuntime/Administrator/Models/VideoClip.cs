﻿using Newtonsoft.Json;
using System;

namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Represents a video clip.
  /// </summary>
  [JsonObject(MemberSerialization.OptIn)]
  public class VideoClip
  {
    /// <summary>
    /// Gets or sets the format.
    /// </summary>
    [JsonProperty("format")]
    public VideoFormat Format { get; set; }

    /// <summary>
    /// Gets or sets the start time.
    /// </summary>
    [JsonProperty("startTime")]
    public DateTimeOffset StartTime { get; set; }

    /// <summary>
    /// Gets or sets the end time.
    /// </summary>
    [JsonProperty("endTime")]
    public DateTimeOffset EndTime { get; set; }

    /// <summary>
    /// Gets or sets the download url.
    /// </summary>
    [JsonProperty("downloadUrl")]
    public string DownloadUrl { get; set; }

    /// <summary>
    /// Gets or sets the view url.
    /// </summary>
    [JsonProperty("viewUrl")]
    public string ViewUrl { get; set; }

  }
}
