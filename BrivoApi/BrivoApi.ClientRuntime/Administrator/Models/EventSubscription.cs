﻿using System.Collections.Generic;
using BrivoApi.Core.Filters;
using Newtonsoft.Json;

namespace BrivoApi.Administrator.Models
{
  public class EventSubscription
  {
    /// <summary>
    /// Gets or sets the identifier.
    /// </summary>
    /// <value>
    /// The identifier.
    /// </value>
    [JsonProperty("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets The name of the event subscription.
    /// </summary>
    /// <value>
    /// The name of the site.
    /// </value>
    [JsonProperty("name")]
    public string Name { get; set; }

    /// <summary>
      /// Gets or sets the url event subscription goes to.
      /// </summary>
      /// <value>
      /// The the url event subscription goes to.
      /// </value>
      [JsonProperty("url")]
      public string Url { get; set; }


    /// <summary>
    /// Gets or sets the email address to send a failure to post notification to.
    /// </summary>
    /// <value>
    /// The email address to send a failure to post notification to.
    /// </value>
    [JsonProperty("errorEmail")]
    public string ErrorEmail { get; set; }

    /// <summary>
    /// Gets or sets the event subscription criteria.
    /// </summary>
    /// <value>
    /// The the event subscription criteria.
    /// </value>
    [JsonProperty("criteria")]
    public List<EventSubscriptionCriteria> Criteria { get; set; }

  } 
}

