﻿using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using Microsoft.Rest;
using BrivoApi.Security;
using System;
using System.IO;


namespace BrivoApi.Administrator
{
  /// <summary>
  /// Defines client-side logical representation of the Brivo APIs for account administration.
  /// </summary>
  public interface IAdministratorClient
  {
    /// <summary>
    /// Get the authorization context used for this client.
    /// </summary>
    AuthorizationContext AuthorizationContext { get; }


    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Application>>> ListApplicationsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Application>>> ListAuthorizedApplicationsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Application"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Application"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Application>> GetApplicationByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="Application"/>.
    /// </summary>
    /// <param name="newApplication">The Application.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Application" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Application>> CreateApplicationAsync(
     Application newApplication,CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="Application" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> DeleteApplicationAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="eventSubscription">The event subscription.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="EventSubscription" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<EventSubscription>> CreateEventSubscriptionAsync(EventSubscription eventSubscription,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="eventSubscriptionExpression">The event subscription expression.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="name">the name of the event subscription </param>
    /// <param name="url">the event subscription post url</param>
    /// <param name="errorEmail">the error email for the event subscription </param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="EventSubscription" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<EventSubscription>> CreateEventSubscriptionAsync(string name, string url,
      string errorEmail, IEventSubscriptionExpression eventSubscriptionExpression,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="EventSubscription" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> DeleteEventSubscriptionAsync(long id,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="EventSubscription"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<EventSubscription>> GetEventSubscriptionAsync(long id,
      CancellationToken cancellationToken = new CancellationToken());
    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="EventSubscription" /> and return a HTTP response containing a <see cref="EventSubscription" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="eventSubscription">The new event subscription data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="EventSubscription" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<EventSubscription>> UpdateEventSubscriptionAsync(long id,
      EventSubscription eventSubscription,
      CancellationToken cancellationToken = new CancellationToken());
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="EventSubscription"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<EventSubscription>>> ListEventSubscriptionPagedAsync(
      PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Account"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Account"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Account>>> ListAccountsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Account"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Account"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Account>> GetAccountByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// Lists the sites paged async.
    /// </summary>
    /// <param name="page">Page.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="BrivoApi.Administrator.Models.Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Models.Administrator>>>
      ListAdministratorsPagedAsync(PageInfo page, CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">filter</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>  
    Task<HttpOperationResponse<GenericPagedCollection<Models.Administrator>>> ListAdministratorsPagedAsync(
      PageInfo page, Filter filter, CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="BrivoApi.Administrator.Models.Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Models.Administrator>> GetAdministratorByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    // Initiates an asynchronous operation to return a HTTP response containing a <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="BrivoApi.Administrator.Models.Administrator"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Models.Administrator>> GetCurrentAdministratorAsync(
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="newAdministrator">The Administrator.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Models.Administrator>> CreateAdministratorAsync(
      Models.Administrator newAdministrator,
      CancellationToken cancellationToken = default(CancellationToken));


    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="newAdministrator">The Administrator.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="password">the password for the account</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Models.Administrator>> CreateAdministratorAsync(
      Models.Administrator newAdministrator, string password,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Site"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Site"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Site>>> ListSitesPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Site"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Site"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Site>> GetSiteByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/> for the specified siteId.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="siteId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<HttpOperationResponse<GenericPagedCollection<Camera>>> ListCamerasBySitePagedAsync(
      PageInfo page, int siteId, CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="User"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="User"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<User>>> ListUsersPagedAsync(PageInfo page, Filter filter,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Group" /> for the specified user.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="page"></param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Group"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Group>>> ListUserGroupsAsync(int id, PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Credential" /> for the specified user.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="page">the page</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Credential"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Credential>>> ListUserCredentialsAsync(int id, PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="User"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="User"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<User>> GetUserByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="User"/>.
    /// </summary>
    /// <param name="externalId">The external identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="User"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<User>> GetUserByExternalIdAsync(string externalId,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="User"/>.
    /// </summary>
    /// <param name="user">The user.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<User>> CreateUserAsync(User user,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="User" /> and return a HTTP response containing a <see cref="User" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="user">The new user data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<User>> UpdateUserAsync(int id, User user,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="User" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> DeleteUserAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to determines whether a <see cref="User" /> is suspended or not and return a HTTP response of true if 
    /// the user is suspended, false otherwise.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is true if
    /// the user is suspended, false otherwise.
    /// </returns>
    Task<HttpOperationResponse<SuspendedUserInfo>> IsUserSuspendedAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to aets the user suspended and return a HTTP response.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="suspended">if set to <c>true</c> [suspended].</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is true if
    /// the user is suspended, false otherwise.
    /// </returns>
    Task<HttpOperationResponse<SuspendedUserInfo>> SetUserSuspendedAsync(int id, bool suspended,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to add a user credential to a user and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="credentialId">The credential identifier.</param>
    /// <param name="effectiveDateRange">The credential effective date range.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> AddUserCredentialAsync(int userId, int credentialId,
      EffectiveDateRange effectiveDateRange = null,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to remove a user credential from a user and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="credentialId">The credential identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> RemoveUserCredentialAsync(int userId, int credentialId,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Credential" /> for the specified user.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="page">the page</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="CustomFieldValue" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<CustomFieldValue>>> ListUserCustomFieldsAsync(int id,
      PageInfo page,
      CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="CustomFieldValue" /> and return a HTTP response containing a <see cref="CustomFieldValue" />.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="customFieldValue">The custom field value</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="CustomFieldValue" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<CustomFieldValue>> UpdateUserCustomFieldAsync(int id, CustomFieldValue customFieldValue,
      CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to delete a user custom field.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="customFieldValue">The custom field</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> DeleteUserCustomFieldAsync(int id, CustomFieldValue customFieldValue,
      CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to remove a user photo and return a HTTP response.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> DeleteUserPhotoAsync(int id,
      CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to download an the user photo.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="Stream" /> that represents the asynchronous operation. The response body is photo.
    /// </returns>
    Task<Stream> GetUserPhotoAsync(int id, CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="User" /> Photo with format of png, jpg, jpeg or gif.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="photoBytes">The photo</param>
    /// <param name="photoContentType">ContentType for the photo valid values are image/png, image/jpg, image/jpeg or image/gif</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="object" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<object>> UpdateUserPhotoAsync(int id, byte[] photoBytes,
      string photoContentType = "image/jpeg",
      CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Activity"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Activity"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Activity>>> ListActivitiesPagedAsync(PageInfo page,
      Filter filter, CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Activity"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Activity"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Activity>> GetActivityByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="AccessPoint"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="AccessPoint"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<AccessPoint>>> ListAccessPointsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="AccessPoint"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="AccessPoint"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<AccessPoint>> GetAccessPointByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to activate an access point.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> ActivateAccessPointAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="AccessPoint"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="CustomFieldDefinition"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<CustomFieldDefinition>>> ListCustomFieldsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="ControlPanel"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="ControlPanel"/> that represents the asynchronous operation.
    /// </returns>

    Task<HttpOperationResponse<GenericPagedCollection<ControlPanel>>> ListControlPanelsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="ControlPanel"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="ControlPanel"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<ControlPanel>> GetControlPanelByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="CredentialFormat"/>.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="CredentialFormat"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<CredentialFormat>>> ListCredentialFormatsAsync(
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Credential"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Credential"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Credential>>> ListCredentialsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Credential"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Credential"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Credential>> GetCredentialByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to create a credential and return a HTTP response containing the new <see cref="Credential"/>.
    /// </summary>
    /// <param name="newCredential">The new credential data, use a concrete subclass of <see cref="NewCredentialBase"/> 
    /// such as <see cref="NewCredentialWithEncodedValue"/> or <see cref="NewCredentialWithFieldValues"/>.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Credential"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Credential>> CreateCredentialAsync(NewCredentialBase newCredential,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to delete a credential.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> DeleteCredentialAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to get a user from his credential and return a HTTP response containing the <see cref="User"/>.
    /// </summary>
    /// <param name="credentialId">The credential identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="User"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<User>> GetUserByCredentialIdAsync(int credentialId,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Group" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Group" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Group>>> ListGroupsPagedAsync(PageInfo page, Filter filter,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Group"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Group"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Group>> GetGroupByIdAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<User>>> ListGroupUsersAsync(int id, PageInfo page, Filter filter,
      CancellationToken cancellationToken = new CancellationToken());

    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="Group" />.
    /// </summary>
    /// <param name="newGroup">The new group.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Group" /> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<Group>> CreateGroupAsync(Group newGroup,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="Group" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> DeleteGroupAsync(int id,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to add a user to a group and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="groupId">The group identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> AddUserToGroupAsync(int userId, int groupId,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to remove a user from a group and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="groupId">The group identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    Task<HttpOperationResponse<object>> RemoveUserFromGroupAsync(int userId, int groupId,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/> that represents the asynchronous operation.
    /// </returns>
    Task<HttpOperationResponse<GenericPagedCollection<Camera>>> ListCamerasPagedAsync(PageInfo page,
      Filter filter, CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Camera"/>.
    /// </summary>
    /// <param name="id">The camera id.</param>
    /// <param name="showStatus">Flag to show status.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns></returns>
    Task<HttpOperationResponse<Camera>> GetCameraByIdAsync(int id, bool showStatus,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="AccessPoint"/> for the specified cameraId.
    /// </summary>
    /// <param name="cameraId"></param>
    /// <param name="page"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<HttpOperationResponse<GenericPagedCollection<AccessPoint>>> ListAccessPointsByCameraPagedAsync(
      PageInfo page, int cameraId, CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/> for the specified accessPointId.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="accessPointId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<HttpOperationResponse<GenericPagedCollection<Camera>>> ListCamerasByAccessPointPagedAsync(
      PageInfo page, int accessPointId, CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="VideoClipResponse"/>.
    /// </summary>
    /// <param name="cameraId"></param>
    /// <param name="startTime"></param>
    /// <param name="endTime"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<HttpOperationResponse<VideoClipResponse>> GetVideoClipsAsync(
      int cameraId, DateTimeOffset startTime, DateTimeOffset endTime,
      CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Lists the proximity data paged async.
    /// </summary>
    /// <returns>The proximity data paged async.</returns>
    /// <param name="page">Page.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    Task<HttpOperationResponse<GenericPagedCollection<Proximity>>> ListProximityDataPagedAsync(
      PageInfo page, CancellationToken cancellationToken = default(CancellationToken));

    /// <summary>
    /// Sets the proximity data by site async.
    /// </summary>
    /// <returns>The proximity data by site async.</returns>
    /// <param name="siteId">The site identifier.</param> 
    /// <param name="proximity">Proximity data.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    Task<HttpOperationResponse<Proximity>> SetProximityDataBySiteAsync(
      long siteId, Proximity proximity, CancellationToken cancellationToken = default(CancellationToken));
  }
}