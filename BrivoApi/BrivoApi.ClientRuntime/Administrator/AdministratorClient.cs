﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using BrivoApi.Security;
using Microsoft.Rest;
using System.IO;
using System.Net;
using System.Net.Http.Headers;


namespace BrivoApi.Administrator
{
  /// <summary>
  /// A .NET client for the Brivo Administrator APIs.
  /// </summary>
  public class AdministratorClient : AdministratorClientBase<AdministratorClient>, IAdministratorClient
  {


    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    public AdministratorClient(AuthorizationContext authorizationContext, Uri endpoint)
      : base(authorizationContext, endpoint)
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="handlers">The handlers.</param>
    public AdministratorClient(AuthorizationContext authorizationContext, Uri endpoint,
      params DelegatingHandler[] handlers)
      : base(authorizationContext, endpoint, handlers)
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="rootHandler">The root handler.</param>
    /// <param name="handlers">The handlers.</param>
    public AdministratorClient(AuthorizationContext authorizationContext, Uri endpoint,
      HttpClientHandler rootHandler,
      params DelegatingHandler[] handlers)
      : base(authorizationContext, endpoint, rootHandler, handlers)
    {

    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Application>>> ListApplicationsPagedAsync(PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<Application>>(BrivoApiConstants.UriPaths.GetApplications, page,
            nameof(this.ListEventSubscriptionPagedAsync),
            cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Application"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Application>>> ListAuthorizedApplicationsPagedAsync(PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {
      return
       await
         this.GetAsync<GenericPagedCollection<Application>>(BrivoApiConstants.UriPaths.GetAuthorizedApplications, page,
           nameof(this.ListAuthorizedApplicationsPagedAsync),
           cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Application"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="Application"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Application>> GetApplicationByIdAsync(int id, CancellationToken cancellationToken = new CancellationToken())
    {
      return
         await
           this.GetAsync<Application>(
             string.Format(BrivoApiConstants.UriPaths.GetApplicationFormat, id),
             nameof(this.GetApplicationByIdAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="Application"/>.
    /// </summary>
    /// <param name="newApplication">The Application.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Application" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Application>> CreateApplicationAsync(Application newApplication, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PostAsync<Application, Application>(
           BrivoApiConstants.UriPaths.CreateEventSubscription, newApplication,
            nameof(this.CreateApplicationAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="Application" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> DeleteApplicationAsync(int id, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.DeleteAsync<object>(string.Format(BrivoApiConstants.UriPaths.DeleteApplicationFormat, id),
            nameof(this.DeleteApplicationAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="eventSubscription">The event subscription.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="EventSubscription" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<EventSubscription>> CreateEventSubscriptionAsync(EventSubscription eventSubscription,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PostAsync<EventSubscription, EventSubscription>(
            BrivoApiConstants.UriPaths.CreateEventSubscription, eventSubscription,
            nameof(CreateEventSubscriptionAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="eventSubscriptionExpression">The event subscription expression.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="name">the name of the event subscription </param>
    /// <param name="url">the event subscription post url</param>
    /// <param name="errorEmail">the error email for the event subscription </param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="EventSubscription" /> that represents the asynchronous operation.
    /// </returns>
    public Task<HttpOperationResponse<EventSubscription>> CreateEventSubscriptionAsync(
      string name,
      string url,
      string errorEmail,
      IEventSubscriptionExpression eventSubscriptionExpression,
      CancellationToken cancellationToken = new CancellationToken())
    {
      var es = eventSubscriptionExpression.EventSubscription;
      es.Name = name;
      es.Url = url;
      es.ErrorEmail = errorEmail;
      return this.CreateEventSubscriptionAsync(es, cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="EventSubscription" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> DeleteEventSubscriptionAsync(long id, CancellationToken cancellationToken = new CancellationToken())
    {
      return
       await
         this.DeleteAsync<object>(string.Format(BrivoApiConstants.UriPaths.DeleteEventSubscriptionFormat, id),
           nameof(this.DeleteEventSubscriptionAsync), cancellationToken);

    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="EventSubscription"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<EventSubscription>> GetEventSubscriptionAsync(long id,
     CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<EventSubscription>(
            string.Format(BrivoApiConstants.UriPaths.GetEventSubscriptionFormat, id),
            nameof(this.GetEventSubscriptionAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="EventSubscription" /> and return a HTTP response containing a <see cref="EventSubscription" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="eventSubscription">The new event subscription data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="EventSubscription" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<EventSubscription>> UpdateEventSubscriptionAsync(long id, EventSubscription eventSubscription,
     CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PostAsync<EventSubscription, EventSubscription>(
            string.Format(BrivoApiConstants.UriPaths.UpdateEventSubscriptionFormat, id), eventSubscription,
            nameof(this.UpdateEventSubscriptionAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="EventSubscription"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="EventSubscription"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<EventSubscription>>> ListEventSubscriptionPagedAsync(PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<EventSubscription>>(BrivoApiConstants.UriPaths.GetEventSubscriptions, page,
            nameof(this.ListEventSubscriptionPagedAsync),
            cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Site" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Site" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Site>>> ListSitesPagedAsync(PageInfo page,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<Site>>(BrivoApiConstants.UriPaths.GetSites, page,
            "ListSitesPagedAsync",
            cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Site" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Site" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Site>> GetSiteByIdAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<Site>(string.Format(BrivoApiConstants.UriPaths.GetSiteFormat, id), "GetSiteByIdAsync",
            cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/> for the specified siteId.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="siteId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Camera>>> ListCamerasBySitePagedAsync(
      PageInfo page, int siteId, CancellationToken cancellationToken = default(CancellationToken))
    {
      var queries = new IQueryParametersAccessor[] { page };
      return
        await
          this.GetAsync<GenericPagedCollection<Camera>>(string.Format(BrivoApiConstants.UriPaths.GetSiteCamerasFormat, siteId), queries,
            "ListSiteCamerasPagedAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="User" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<User>>> ListUsersPagedAsync(PageInfo page,
      Filter filter,
      CancellationToken cancellationToken = new CancellationToken())
    {
      var queries = new IQueryParametersAccessor[]
      {
        page,
        filter
      };
      return
        await
          this.GetAsync<GenericPagedCollection<User>>(BrivoApiConstants.UriPaths.GetUsers, queries,
            "ListUsersPagedAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Group" /> for the specified user.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Group" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Group>>> ListUserGroupsAsync(int id, PageInfo page,
      CancellationToken cancellationToken = new CancellationToken())
    {
     var queries = new IQueryParametersAccessor[]
     {
        page        
     };
      return
        await
          this.GetAsync<GenericPagedCollection<Group>>(
            string.Format(BrivoApiConstants.UriPaths.GetUserGroupsFormat, id), queries, "ListUserGroupsAsync",
            cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Credential" /> for the specified user.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Credential" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Credential>>> ListUserCredentialsAsync(int id,
      PageInfo page,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<Credential>>(
            string.Format(BrivoApiConstants.UriPaths.GetUserCredentialsFormat, id), page,
            "ListUserCredentialsAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="User" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<User>> GetUserByIdAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<User>(string.Format(BrivoApiConstants.UriPaths.GetUserFormat, id), "GetUserByIdAsync",
            cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="User" />.
    /// </summary>
    /// <param name="externalId">The external identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<User>> GetUserByExternalIdAsync(string externalId,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<User>(
            string.Format(BrivoApiConstants.UriPaths.GetUserByExternalIdFormat,
              Uri.EscapeDataString(externalId)),
            "GetUserByExternalIdAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="User" />.
    /// </summary>
    /// <param name="user">The user.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<User>> CreateUserAsync(User user,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PostAsync<User, User>(BrivoApiConstants.UriPaths.CreateUser, user, "CreateUserAsync",
            cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="User" /> and return a HTTP response containing a <see cref="User" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="user">The new user data.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<User>> UpdateUserAsync(int id, User user,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PutAsync<User, User>(string.Format(BrivoApiConstants.UriPaths.UpdateUserFormat, id), user,
            "UpdateUserAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="User" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> DeleteUserAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.DeleteAsync<object>(string.Format(BrivoApiConstants.UriPaths.DeleteUserFormat, id),
            "DeleteUserAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to determines whether a <see cref="User" /> is suspended or not and return a HTTP response of true if
    /// the user is suspended, false otherwise.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is true if
    /// the user is suspended, false otherwise.
    /// </returns>
    public async Task<HttpOperationResponse<SuspendedUserInfo>> IsUserSuspendedAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<SuspendedUserInfo>(
            string.Format(BrivoApiConstants.UriPaths.IsUserSuspendedFormat, id), "IsUserSuspendedAsync",
            cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to aets the user suspended and return a HTTP response.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="suspended">if set to <c>true</c> [suspended].</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is true if
    /// the user is suspended, false otherwise.
    /// </returns>
    public async Task<HttpOperationResponse<SuspendedUserInfo>> SetUserSuspendedAsync(int id, bool suspended,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PutAsync<SuspendedUserInfo, SuspendedUserInfo>(
            string.Format(BrivoApiConstants.UriPaths.SetUserSuspendedFormat, id),
            new SuspendedUserInfo(suspended),
            "SetUserSuspendedAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to add a user credential to a user and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="credentialId">The credential identifier.</param>
    /// <param name="effectiveDateRange">The credential effective date range.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> AddUserCredentialAsync(int userId, int credentialId,EffectiveDateRange effectiveDateRange = null,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PutAsync<object, object>(
            string.Format(BrivoApiConstants.UriPaths.AddUserCredentialFormat, userId, credentialId), effectiveDateRange,
            nameof(this.AddUserCredentialAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to remove a user credential from a user and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="credentialId">The credential identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> RemoveUserCredentialAsync(int userId, int credentialId,
      CancellationToken cancellationToken = new CancellationToken())
    { 
      return
        await
          this.DeleteAsync<object>(
            string.Format(BrivoApiConstants.UriPaths.RemoveUserCredentialFormat, userId, credentialId),
            "RemoveUserCredentialAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Credential" /> for the specified user.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="page">the page</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="CustomFieldValue" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<CustomFieldValue>>> ListUserCustomFieldsAsync(int id, PageInfo page,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<CustomFieldValue>>(
            string.Format(BrivoApiConstants.UriPaths.GetUserCustomFieldsFormat, id), page,
            nameof(this.ListUserCustomFieldsAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="CustomFieldValue" /> and return a HTTP response containing a <see cref="CustomFieldValue" />.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="customFieldValue">The custom field value</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="CustomFieldValue" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<CustomFieldValue>> UpdateUserCustomFieldAsync(int id, CustomFieldValue customFieldValue,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.PutAsync<CustomFieldValue, CustomFieldValue>(string.Format(BrivoApiConstants.UriPaths.UpdateUserCustomFieldFormat, id, customFieldValue.CustomFieldId), customFieldValue,
           nameof(this.UpdateUserCustomFieldAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to delete a user custom field.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="customFieldValue">The custom field value</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> DeleteUserCustomFieldAsync(int id, CustomFieldValue customFieldValue,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.DeleteAsync<object>(string.Format(BrivoApiConstants.UriPaths.DeleteUserCustomFieldFormat, id, customFieldValue.CustomFieldId),
            nameof(this.DeleteUserCustomFieldAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to remove a user photo and return a HTTP response.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> DeleteUserPhotoAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.DeleteAsync<object>(
            string.Format(BrivoApiConstants.UriPaths.DeleteUserPhotoFormat, id),
            nameof(this.DeleteUserPhotoAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response from a request to download an the user photo.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="Stream" /> that represents the asynchronous operation. The response body is photo.
    /// </returns>
    public async Task<Stream> GetUserPhotoAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      Action<HttpRequestHeaders> acceptedImageFormats = h =>
      {
        h.Accept.ParseAdd("image/png");
        h.Accept.ParseAdd("image/jpeg");
        h.Accept.ParseAdd("image/gif");
        h.Accept.ParseAdd("application/json");
      };
      var httpContent = await this.GetRawContentAsync(string.Format(BrivoApiConstants.UriPaths.GetUserPhotoFormat, id), null, 
        nameof(this.GetUserPhotoAsync), cancellationToken, acceptedImageFormats);
      return await httpContent.ReadAsStreamAsync();
    }
    /// <summary>
    /// Initiates an asynchronous operation to update a <see cref="User" /> Photo with format of png, jpg, jpeg or gif.
    /// </summary>
    /// <param name="id">The user identifier.</param>
    /// <param name="photoBytes">The photo</param>
    /// <param name="photoContentType">ContentType for the photo valid values are image/png, image/jpg, image/jpeg or image/gif</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="object" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<object>> UpdateUserPhotoAsync(int id, byte[] photoBytes,string photoContentType = "image/jpeg",
      CancellationToken cancellationToken = new CancellationToken())
    {

      Func<MultipartFormDataContent> acceptedImageFormats = ()=>
      {
        var requestContent = new MultipartFormDataContent();
        var imageContent = new ByteArrayContent(photoBytes);
        requestContent.Add(imageContent, "file", "image");
        imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse(photoContentType);
        return requestContent;
      };
     
      return
        await
          this.PostMultipartFormDataContentAsync<object>(string.Format(BrivoApiConstants.UriPaths.UpdateUserPhotoFormat, id), acceptedImageFormats,
            nameof(this.UpdateUserCustomFieldAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Activity" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Activity" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Activity>>> ListActivitiesPagedAsync(
      PageInfo page,
      Filter filter,
      CancellationToken cancellationToken = new CancellationToken())
    {
      var queries = new IQueryParametersAccessor[]
      {
        page,
        filter
      };
      return
        await
          this.GetAsync<GenericPagedCollection<Activity>>(BrivoApiConstants.UriPaths.GetActivities, queries,
            "ListActivitiesPagedAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Activity" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Activity" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Activity>> GetActivityByIdAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {     
      return
        await
          this.GetAsync<Activity>(string.Format(BrivoApiConstants.UriPaths.GetActivityFormat, id),
            "GetActivityByIdAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="AccessPoint" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="AccessPoint" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<AccessPoint>>> ListAccessPointsPagedAsync(
      PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<AccessPoint>>(BrivoApiConstants.UriPaths.GetAccessPoints, page,
            "ListAccessPointsPagedAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="AccessPoint" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="AccessPoint" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<AccessPoint>> GetAccessPointByIdAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {      
      return
        await
          this.GetAsync<AccessPoint>(string.Format(BrivoApiConstants.UriPaths.GetAccessPointFormat, id),
            "GetAccessPointByIdAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to activate an access point.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> ActivateAccessPointAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {      
      return
        await
          this.PostAsync<object, object>(
            string.Format(BrivoApiConstants.UriPaths.ActivateAccessPointFormat, id), null,
            "ActivateAccessPointAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="AccessPoint"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="CustomFieldDefinition"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<CustomFieldDefinition>>> ListCustomFieldsPagedAsync (PageInfo page,
      CancellationToken cancellationToken = default (CancellationToken))
    {
      return
       await
         this.GetAsync<GenericPagedCollection<CustomFieldDefinition>> (BrivoApiConstants.UriPaths.GetCustomFields, page,
            nameof (this.ListControlPanelsPagedAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="ControlPanel"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="ControlPanel"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<ControlPanel>>> ListControlPanelsPagedAsync(PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<ControlPanel>>(BrivoApiConstants.UriPaths.GetControlPanels, page,
             nameof(this.ListControlPanelsPagedAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="ControlPanel"/>.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="ControlPanel"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<ControlPanel>> GetControlPanelByIdAsync(int id, CancellationToken cancellationToken = new CancellationToken())
    {
      return
       await
         this.GetAsync<ControlPanel>(string.Format(BrivoApiConstants.UriPaths.GetControlPanelFormat, id),
           nameof(this.GetControlPanelByIdAsync), cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="CredentialFormat" />.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="CredentialFormat" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<CredentialFormat>>> ListCredentialFormatsAsync(
      CancellationToken cancellationToken = new CancellationToken())
    {
      await this.AuthorizationContext.EnsureValidTokenAsync(cancellationToken);
      return
        await
          this.GetAsync<GenericPagedCollection<CredentialFormat>>(
            BrivoApiConstants.UriPaths.GetCredentialFormats, "ListCredentialFormatsAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Credential" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Credential" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Credential>>> ListCredentialsPagedAsync(
      PageInfo page, CancellationToken cancellationToken = new CancellationToken())
    {     
      return
        await
          this.GetAsync<GenericPagedCollection<Credential>>(BrivoApiConstants.UriPaths.GetCredentials, page,
            "ListCredentialsPagedAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Credential" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Credential" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Credential>> GetCredentialByIdAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {      
      return
        await
          this.GetAsync<Credential>(string.Format(BrivoApiConstants.UriPaths.GetCredentialFormat, id),
            "GetCredentialByIdAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to create a credential and return a HTTP response containing the new <see cref="Credential" />.
    /// </summary>
    /// <param name="newCredential">The new credential data, use a concrete subclass of <see cref="NewCredentialBase" />
    /// such as <see cref="NewCredentialWithEncodedValue" /> or <see cref="NewCredentialWithFieldValues" />.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Credential" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Credential>> CreateCredentialAsync(NewCredentialBase newCredential,
      CancellationToken cancellationToken = new CancellationToken())
    {     
      return
        await
          this.PostAsync<NewCredentialBase, Credential>(BrivoApiConstants.UriPaths.CreateCredential,
            newCredential, "CreateUserAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to delete a credential.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation.  The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> DeleteCredentialAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {      
      return
        await
          this.DeleteAsync<object>(string.Format(BrivoApiConstants.UriPaths.DeleteCredentialFormat, id),
            "DeleteCredentialAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to get a user from his credential and return a HTTP response containing the <see cref="User" />.
    /// </summary>
    /// <param name="credentialId">The credential identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<User>> GetUserByCredentialIdAsync(int credentialId,
      CancellationToken cancellationToken = new CancellationToken())
    {      
      return
        await
          this.GetAsync<User>(
            string.Format(BrivoApiConstants.UriPaths.GetUserByCredentialFormat, credentialId),
            "GetUserByCredentialIdAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Group" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Group" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Group>>> ListGroupsPagedAsync(PageInfo page,
      Filter filter, CancellationToken cancellationToken = new CancellationToken())
    {
      var queries = new IQueryParametersAccessor[]
      {
        page,
        filter
      };
      return
        await
          this.GetAsync<GenericPagedCollection<Group>>(BrivoApiConstants.UriPaths.GetGroups, queries,
            "ListGroupsPagedAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Group" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Group" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Group>> GetGroupByIdAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {     
      return
        await
          this.GetAsync<Group>(string.Format(BrivoApiConstants.UriPaths.GetGroupByIdFormat, id),
            "GetGroupByIdAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<User>>> ListGroupUsersAsync(int id, PageInfo page,
      Filter filter, CancellationToken cancellationToken = new CancellationToken())
    {
      var queries = new IQueryParametersAccessor[]
      {
        page,
        filter
      };

      return
        await
        this.GetAsync<GenericPagedCollection<User>>(string.Format(BrivoApiConstants.UriPaths.GetGroupUsersFormat, id),queries,
          nameof(this.ListGroupUsersAsync), cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="Group" />.
    /// </summary>
    /// <param name="newGroup">The new group.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="User" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Group>> CreateGroupAsync(Group newGroup,
      CancellationToken cancellationToken = new CancellationToken())
    {     
      return
        await
          this.PostAsync<Group, Group>(BrivoApiConstants.UriPaths.CreateGroup, newGroup, "CreateGroupAsync",
            cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to delete a <see cref="Group" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> DeleteGroupAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {     
      return
        await
          this.DeleteAsync<object>(string.Format(BrivoApiConstants.UriPaths.DeleteGroupFormat, id),
            "DeleteGroupAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to add a user to a group and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="groupId">The group identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> AddUserToGroupAsync(int userId, int groupId,
      CancellationToken cancellationToken = new CancellationToken())
    {      
      return
        await
          this.PutAsync<string, object>(
            string.Format(BrivoApiConstants.UriPaths.AddUserToGroupFormat, groupId, userId), null,
            "AddUserToGroupAsync", cancellationToken);
    }
    /// <summary>
    /// Initiates an asynchronous operation to remove a user from a group and return a HTTP response.
    /// </summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="groupId">The group identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> that represents the asynchronous operation. The response body is null.
    /// </returns>
    public async Task<HttpOperationResponse<object>> RemoveUserFromGroupAsync(int userId, int groupId,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.DeleteAsync<object>(
            string.Format(BrivoApiConstants.UriPaths.RemoveUserFromGroupFormat, groupId, userId),
            "RemoveUserFromGroupAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/>.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Camera>>> ListCamerasPagedAsync(PageInfo page,
      Filter filter, CancellationToken cancellationToken = new CancellationToken())
    {
      var queries = new IQueryParametersAccessor[]
      {
        page,
        filter
      };
      return
        await
        this.GetAsync<GenericPagedCollection<Camera>>(BrivoApiConstants.UriPaths.GetCameras, queries,
          "ListCamerasPagedAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Camera"/>.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="showStatus"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<HttpOperationResponse<Camera>> GetCameraByIdAsync(int id, bool showStatus,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<Camera>(string.Format(BrivoApiConstants.UriPaths.GetCameraFormat, id, showStatus),
            "GetCameraByIdAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="AccessPoint"/> for the specified cameraId.
    /// </summary>
    /// <param name="cameraId"></param>
    /// <param name="page"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<AccessPoint>>> ListAccessPointsByCameraPagedAsync(PageInfo page, int cameraId, CancellationToken cancellationToken = default(CancellationToken))
    {
      var queries = new IQueryParametersAccessor[] { page };
      return
        await
          this.GetAsync<GenericPagedCollection<AccessPoint>>(string.Format(BrivoApiConstants.UriPaths.GetAccessPointsByCamerasFormat, cameraId), queries,
            "ListCamerasByAccessPointPagedAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}"/> of <see cref="Camera"/> for the specified accessPointId.
    /// </summary>
    /// <param name="page"></param>
    /// <param name="accessPointId"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Camera>>> ListCamerasByAccessPointPagedAsync(PageInfo page, int accessPointId, CancellationToken cancellationToken = default(CancellationToken))
    {
      var queries = new IQueryParametersAccessor[] { page };
      return
        await
          this.GetAsync<GenericPagedCollection<Camera>>(string.Format(BrivoApiConstants.UriPaths.GetCamerasByAccessPointsFormat, accessPointId), queries,
            "ListAccessPointsByCameraPagedAsync", cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="VideoClipResponse"/>.
    /// </summary>
    /// <param name="cameraId"></param>
    /// <param name="startTime"></param>
    /// <param name="endTime"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public async Task<HttpOperationResponse<VideoClipResponse>> GetVideoClipsAsync(
      int cameraId, DateTimeOffset startTime, DateTimeOffset endTime, CancellationToken cancellationToken = default(CancellationToken))
    {
      var startTimeString = WebUtility.UrlEncode(startTime.ToString(BrivoApiConstants.DateTimeFormat));
      var endTimeString = WebUtility.UrlEncode(endTime.ToString(BrivoApiConstants.DateTimeFormat));
      return
        await
          this.GetAsync<VideoClipResponse>(
            string.Format(BrivoApiConstants.UriPaths.GetVideoClipsFormat, cameraId, startTimeString, endTimeString), 
            "GetVideoClipsAsync", cancellationToken);
    }

    /// <summary>
    /// Lists the proximity data paged async.
    /// </summary>
    /// <returns>The proximity data paged async.</returns>
    /// <param name="page">Page.</param>
    /// <param name="filter">filter</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    public async Task<HttpOperationResponse<GenericPagedCollection<Proximity>>> ListProximityDataPagedAsync(
      PageInfo page, CancellationToken cancellationToken = default(CancellationToken))
    {
      var queries = new IQueryParametersAccessor[] { page };
        return
          await
            this.GetAsync<GenericPagedCollection<Proximity>>(BrivoApiConstants.UriPaths.GetAllProximityData, queries,
              "ListProximityDataPagedAsync", cancellationToken);
    }

    /// <summary>
    /// Sets the proximity data by site async.
    /// </summary>
    /// <returns>The proximity data by site async.</returns>
    /// <param name="siteId">The site identifier.</param> 
    /// <param name="proximity">Proximity data.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    public async Task<HttpOperationResponse<Proximity>> SetProximityDataBySiteAsync(
      long siteId, Proximity proximity, CancellationToken cancellationToken = default(CancellationToken))
    {
      return
        await
    	  this.PutAsync<Proximity, Proximity>(
            string.Format(BrivoApiConstants.UriPaths.SetSiteProximityWifiData, siteId),
              proximity, "SetProximityDataBySiteAsync", cancellationToken);
    }

  }
}