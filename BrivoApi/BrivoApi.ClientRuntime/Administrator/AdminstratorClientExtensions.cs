using System.Threading;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using System;

namespace BrivoApi.Administrator
{
  /// <summary>
  /// Extension methods for working with <see cref="IAdministratorClient"/>.
  /// </summary>
  public static class AdminstratorClientExtensions
  {


    /// <summary>
    /// Lists a page of administrators.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <returns>A page of administrators</returns>
    public static GenericPagedCollection<BrivoApi.Administrator.Models.Administrator> ListAdministratorsPaged(this IAdministratorClient client, PageInfo page)
    {
      return client.ListAdministratorsPagedAsync(page, CancellationToken.None).GetAwaiter().GetResult().Body;
    }


    /// <summary>
    /// Lists a page of sites.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <returns>A page of sites</returns>
    public static GenericPagedCollection<Site> ListSitesPaged(this IAdministratorClient client, PageInfo page)
    {
      return client.ListSitesPagedAsync(page, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the site by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns>The site</returns>
    public static Site GetSiteById(this IAdministratorClient client, int id)
    {
      return client.GetSiteByIdAsync(id, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists a page of cameras for the specified site.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <param name="siteId">The siteId.</param>
    /// <returns>The page of cameras</returns>
    public static GenericPagedCollection<Camera> ListCamerasBySitePaged(this IAdministratorClient client, PageInfo page, int siteId)
    {
      return client.ListCamerasBySitePagedAsync(page, siteId, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists a page of users.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <returns>The page of users.</returns>
    public static GenericPagedCollection<User> ListUsersPaged(this IAdministratorClient client, PageInfo page, Filter filter)
    {
      return client.ListUsersPagedAsync(page, filter, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the user by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns>The user</returns>
    public static User GetUserById(this IAdministratorClient client, int id)
    {
      return client.GetUserByIdAsync(id, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Creates the user.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="user">The user.</param>
    /// <returns></returns>
    public static User CreateUser(this IAdministratorClient client, User user)
    {
      return client.CreateUserAsync(user, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Updates the user.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <param name="user">The user.</param>
    /// <returns></returns>
    public static User UpdateUser(this IAdministratorClient client, int id, User user)
    {
      return client.UpdateUserAsync(id, user, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Deletes the user.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    public static void DeleteUser(this IAdministratorClient client, int id)
    {
      client.DeleteUserAsync(id, CancellationToken.None).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Gets the user by external identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="externalId">The external identifier.</param>
    /// <returns>The user</returns>
    public static User GetUserByExternalId(this IAdministratorClient client, string externalId)
    {
      return client.GetUserByExternalIdAsync(externalId, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists the user groups.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <param name="page">the page</param>
    /// <returns>The collection of user groups</returns>
    public static GenericPagedCollection<Group> ListUserGroups(this IAdministratorClient client, int id, PageInfo page)
    {
      return client.ListUserGroupsAsync(id, page, CancellationToken.None).GetAwaiter().GetResult().Body;      
    }

    /// <summary>
    /// Lists the user credentials.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <param name="page">the page</param>
    /// <returns>The collection of user groups</returns>
    public static GenericPagedCollection<Credential> ListUserCredentials(this IAdministratorClient client, int id, PageInfo page)
    {
      return client.ListUserCredentialsAsync(id, page, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Determines whether the user is suspended.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns>True if the user is suspended, false otherwise.</returns>
    public static bool IsUserSuspended(this IAdministratorClient client, int id)
    {
      return client.IsUserSuspendedAsync(id, CancellationToken.None).GetAwaiter().GetResult().Body.Suspended;
    }

    /// <summary>
    /// Sets the user suspended state.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <param name="suspended">if set to <c>true</c> [suspended].</param>
    /// <returns>True if the user is suspended, false otherwise.</returns>
    public static bool SetUserSuspended(this IAdministratorClient client, int id, bool suspended)
    {
      return client.SetUserSuspendedAsync(id, suspended, CancellationToken.None).GetAwaiter().GetResult().Body.Suspended;
    }

    /// <summary>
    /// Adds the user credential.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="userId">The user identifier.</param>
    /// <param name="effectiveDateRange">The credential effective date range.</param>
    /// <param name="credentialId">The credential identifier.</param>
    public static void AddUserCredential(this IAdministratorClient client, int userId, int credentialId, EffectiveDateRange effectiveDateRange = null)
    {
      client.AddUserCredentialAsync(userId, credentialId, effectiveDateRange, CancellationToken.None).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Removes the user credential.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="userId">The user identifier.</param>
    /// <param name="credentialId">The credential identifier.</param>
    public static void RemoveUserCredential(this IAdministratorClient client, int userId, int credentialId)
    {
      client.RemoveUserCredentialAsync(userId, credentialId, CancellationToken.None).GetAwaiter().GetResult();      
    }

    /// <summary>
    /// Lists a page of activities.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <returns>The page of activities</returns>
    public static GenericPagedCollection<Activity> ListActivitiesPaged(this IAdministratorClient client, PageInfo page, Filter filter)
    {
      return client.ListActivitiesPagedAsync(page, filter, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the activity by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns>The activity</returns>
    public static Activity GetActivityById(this IAdministratorClient client, int id)
    {
      return client.GetActivityByIdAsync(id, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the access point by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns></returns>
    public static AccessPoint GetAccessPointById(this IAdministratorClient client, int id)
    {
      return client.GetAccessPointByIdAsync(id, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists a page of access points.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <returns>A page of access points.</returns>
    public static GenericPagedCollection<AccessPoint> ListAccessPointsPaged(this IAdministratorClient client, PageInfo page)
    {
      return client.ListAccessPointsPagedAsync(page, CancellationToken.None).GetAwaiter().GetResult().Body;
    }
    /// <summary>
    /// Lists a page of custom-field definitions.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <returns>A page of custom-field definitions.</returns>
    public static GenericPagedCollection<CustomFieldDefinition> ListCustomFieldsPaged (this IAdministratorClient client, PageInfo page)
    {
      return client.ListCustomFieldsPagedAsync (page, CancellationToken.None).GetAwaiter ().GetResult ().Body;
    }
    /// <summary>
    /// Gets the control panel by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns>a control panel</returns>
    public static ControlPanel GetControlPanelById(this IAdministratorClient client, int id)
    {
      return client.GetControlPanelByIdAsync(id, CancellationToken.None).GetAwaiter().GetResult().Body;
    }
    /// <summary>
    /// Lists a page of control panels.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <returns>A page of control panels.</returns>
    public static GenericPagedCollection<ControlPanel> ListControlPanelsPaged(this IAdministratorClient client, PageInfo page)
    {
      return client.ListControlPanelsPagedAsync(page, CancellationToken.None).GetAwaiter().GetResult().Body;
    }
    /// <summary>
    /// Activates the specified access point.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    public static void ActivateAccessPoint(this IAdministratorClient client, int id)
    {
      client.ActivateAccessPointAsync(id, CancellationToken.None).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Lists the available credential formats.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <returns>The collection of credential formats available.</returns>
    public static GenericPagedCollection<CredentialFormat> ListCredentialFormats(this IAdministratorClient client)
    {
      return client.ListCredentialFormatsAsync(CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists a page of credentials.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <returns>The page of credentials</returns>
    public static GenericPagedCollection<Credential> ListCredentialsPaged(this IAdministratorClient client, PageInfo page)
    {
      return client.ListCredentialsPagedAsync(page, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the credential by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns>The credential</returns>
    public static Credential GetCredentialById(this IAdministratorClient client, int id)
    {
      return client.GetCredentialByIdAsync(id, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Creates a new credential.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="newCredential">The new credential.</param>
    /// <returns>The credential.</returns>
    public static Credential CreateCredential(this IAdministratorClient client, NewCredentialBase newCredential)
    {
      return client.CreateCredentialAsync(newCredential, CancellationToken.None).GetAwaiter().GetResult().Body;      
    }

    /// <summary>
    /// Deletes a credential.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    public static void DeleteCredential(this IAdministratorClient client, int id)
    {
      client.DeleteCredentialAsync(id, CancellationToken.None).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Gets the user by credential identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="credentialId">The credential identifier.</param>
    /// <returns></returns>
    public static User GetUserByCredentialId(this IAdministratorClient client, int credentialId)
    {
      return client.GetUserByCredentialIdAsync(credentialId, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists a page of groups.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <returns>The page of groups.</returns>
    public static GenericPagedCollection<Group> ListGroupsPaged(this IAdministratorClient client, PageInfo page, Filter filter)
    {
      return client.ListGroupsPagedAsync(page, filter).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the group by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <returns>The group</returns>
    public static Group GetGroupById(this IAdministratorClient client, int id)
    {
      return client.GetGroupByIdAsync(id).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Gets the group users by identifier.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <returns>The group</returns>
    public static GenericPagedCollection<User>  ListGroupUsers(this IAdministratorClient client, int id, PageInfo page, Filter filter)
    {
      return client.ListGroupUsersAsync(id,page,filter, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Creates the group.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="newGroup">The new group.</param>
    /// <returns>The group.</returns>
    public static Group CreateGroup(this IAdministratorClient client, Group newGroup)
    {
      return client.CreateGroupAsync(newGroup, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Deletes the specifies group.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="id">The identifier.</param>
    public static void DeleteGroup(this IAdministratorClient client, int id)
    {
      client.DeleteGroupAsync(id).GetAwaiter().GetResult();
    }


    /// <summary>
    /// Adds a user to a group.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="userId">The user identifier.</param>
    /// <param name="groupId">The group identifier.</param>
    public static void AddUserToGroup(this IAdministratorClient client, int userId, int groupId)
    {
      client.AddUserToGroupAsync(userId, groupId).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Removes a user from a group.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="userId">The user identifier.</param>
    /// <param name="groupId">The group identifier.</param>
    public static void RemoveUserFromGroup(this IAdministratorClient client, int userId, int groupId)
    {
      client.RemoveUserFromGroupAsync(userId, groupId).GetAwaiter().GetResult();
    }

    /// <summary>
    /// Lists a page of cameras.
    /// </summary>
    /// <param name="client">The client.</param>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <returns>The page of cameras</returns>
    public static GenericPagedCollection<Camera> ListCamerasPaged(this IAdministratorClient client, PageInfo page, Filter filter)
    {
      return client.ListCamerasPagedAsync(page, filter, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Get the camera by the specified id.
    /// </summary>
    /// <param name="client">The client</param>
    /// <param name="id">The camera id.</param>
    /// <param name="showStatus">Flag to return camera status.</param>
    /// <returns></returns>
    public static Camera GetCameraById(this IAdministratorClient client, int id, bool showStatus)
    {
      return client.GetCameraByIdAsync(id, showStatus, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists a page of access points tied to the specified camera.
    /// </summary>
    /// <param name="client"></param>
    /// <param name="page"></param>
    /// <param name="cameraId"></param>
    /// <returns></returns>
    public static GenericPagedCollection<AccessPoint> ListAccessPointsByCameraPaged(this IAdministratorClient client, PageInfo page, int cameraId)
    {
      return client.ListAccessPointsByCameraPagedAsync(page, cameraId, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists a page of cameras tied to the specified access point.
    /// </summary>
    /// <param name="client"></param>
    /// <param name="page"></param>
    /// <param name="accessPointId"></param>
    /// <returns></returns>
    public static GenericPagedCollection<Camera> ListCamerasByAccessPointPaged(this IAdministratorClient client, PageInfo page, int accessPointId)
    {
      return client.ListCamerasByAccessPointPagedAsync(page, accessPointId, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Get the Video Clips for the specified camera and date range.
    /// </summary>
    /// <param name="client"></param>
    /// <param name="cameraId"></param>
    /// <param name="startTime"></param>
    /// <param name="endTime"></param>
    /// <returns></returns>
    public static VideoClipResponse GetVideoClips(this IAdministratorClient client, int cameraId, DateTimeOffset startTime, DateTimeOffset endTime)
    {
      return client.GetVideoClipsAsync(cameraId, startTime, endTime, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Lists the proximity data for all sites paged.
    /// </summary>
    /// <param name="client"></param>
    /// <param name="page"></param>
    /// <returns></returns>
    public static GenericPagedCollection<Proximity> ListProximityDataPaged(this IAdministratorClient client, PageInfo page)
    {
      return client.ListProximityDataPagedAsync(page, CancellationToken.None).GetAwaiter().GetResult().Body;
    }

    /// <summary>
    /// Sets the proximity data for a site.
    /// </summary>
    /// <param name="client"></param>
    /// <param name="siteId"></param> 
    /// <param name="proximity"></param>
    /// <returns></returns>
    public static Proximity SetProximityDataBySite(this IAdministratorClient client, long siteId, Proximity proximity)
    {
      return client.SetProximityDataBySiteAsync(siteId, proximity, CancellationToken.None).GetAwaiter().GetResult().Body;
    }
  }
}