﻿using BrivoApi.Administrator.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace BrivoApi.Administrator.Converters
{
  /// <summary>
  /// JSON String to Type Converfter for a VideoProviderType.
  /// </summary>
  internal class VideoProviderTypeEnumConverter : StringEnumConverter
  {
    public override object ReadJson(JsonReader reader, Type objectType, 
      object existingValue, JsonSerializer serializer)
    {
      var value = reader.Value.ToString();
      if (string.IsNullOrEmpty(value))
        return VideoProviderType.Unknown;

      try
      {
        return base.ReadJson(reader, objectType, existingValue, serializer);
      }
      catch
      {
        return VideoProviderType.Unknown;
      }
    }
  }
}
