﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Core.Filters;
using BrivoApi.Security;
using Microsoft.Rest;
using Newtonsoft.Json;

namespace BrivoApi.Administrator
{
  /// <summary>
  /// A .NET client for the Brivo Administrator APIs.
  /// </summary>
  public abstract class AdministratorClientBase<T> : BrivoApiServiceClientBase<T>
    where T : ServiceClient<T>
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    protected AdministratorClientBase(AuthorizationContext authorizationContext, Uri endpoint)
      : base(authorizationContext.GetToken().Credentials, endpoint)
    {
      this.AuthorizationContext = authorizationContext;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="handlers">The handlers.</param>
    protected AdministratorClientBase(AuthorizationContext authorizationContext, Uri endpoint,
      params DelegatingHandler[] handlers)
      : base(authorizationContext.GetToken().Credentials, endpoint, handlers)
    {
      this.AuthorizationContext = authorizationContext;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AdministratorClient"/> class.
    /// </summary>
    /// <param name="authorizationContext">The authorization context.</param>
    /// <param name="endpoint">The endpoint.</param>
    /// <param name="rootHandler">The root handler.</param>
    /// <param name="handlers">The handlers.</param>
    protected AdministratorClientBase(AuthorizationContext authorizationContext, Uri endpoint,
      HttpClientHandler rootHandler,
      params DelegatingHandler[] handlers)
      : base(authorizationContext.GetToken().Credentials, endpoint, rootHandler, handlers)
    {
      this.AuthorizationContext = authorizationContext;
    }

    /// <summary>
    /// Get the authorization context used for this client.
    /// </summary>
    public AuthorizationContext AuthorizationContext { get; }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" />.
    /// </summary>
    /// <param name="accountId"></param>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns> 
    public async Task<HttpOperationResponse<GenericPagedCollection<Administrator.Models.Administrator>>>
      ListAccountAdministratorsPagedAsync(int accountId, PageInfo page,
        CancellationToken cancellationToken = default(CancellationToken))
    {
      return await
        this.GetAsync<GenericPagedCollection<BrivoApi.Administrator.Models.Administrator>>(
          string.Format(BrivoApiConstants.UriPaths.GetAccountAdministrators, accountId), page,
          "ListAccountAdministratorsPagedAsync",
          cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="newAdministrator">The Administrator.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<BrivoApi.Administrator.Models.Administrator>> CreateAdministratorAsync(
      BrivoApi.Administrator.Models.Administrator newAdministrator,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return
        await
          this.PostAsync<BrivoApi.Administrator.Models.Administrator, BrivoApi.Administrator.Models.Administrator>(
            BrivoApiConstants.UriPaths.CreateAdministratorWithPassword, newAdministrator, "CreateAdministratorAsync",
            cancellationToken);
    }


    /// <summary>
    /// Initiates an asynchronous operation to create a user and return a HTTP response containing the new <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="newAdministrator">The Administrator.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <param name="password">the password for the account</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<BrivoApi.Administrator.Models.Administrator>> CreateAdministratorAsync(
      BrivoApi.Administrator.Models.Administrator newAdministrator, string password,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      var encodedPassword = WebUtility.UrlEncode(password);
      return
        await
          this.PostAsync<BrivoApi.Administrator.Models.Administrator, BrivoApi.Administrator.Models.Administrator>(
            string.Format(BrivoApiConstants.UriPaths.CreateAdministratorWithPassword, encodedPassword), newAdministrator,
            "CreateAdministratorAsync",
            cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">filter</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>  
    public async Task<HttpOperationResponse<GenericPagedCollection<Models.Administrator>>>
      ListAdministratorsPagedAsync(PageInfo page, Filter filter,
        CancellationToken cancellationToken = new CancellationToken())
    {
      try
      {
        var queries = new IQueryParametersAccessor[]
      {
        page,
        filter
      };

        return await
          this.GetAsync<GenericPagedCollection<Models.Administrator>>(
            BrivoApiConstants.UriPaths.GetAdministrators, queries, nameof(this.ListAdministratorsPagedAsync),
            cancellationToken);
      }
      catch (HttpOperationException ex)
      {
        if (!ex.Response.IsSuccessStatusCode) throw;
        //This is in place becasue the API team will switch the existing end point from not-paged to paged and we do not want downward calls to brake when that happnes 
        //this logic below will deserialize the response content and returns  HttpOperationResponse<GenericPagedCollection<Models.Administrator>>
        var responseContent = ex.Body.ToString();
        var result = new HttpOperationResponse<GenericPagedCollection<Models.Administrator>>
        {
          Request = ex.Request,
          Response = ex.Response
        };
        var allAdmins = JsonConvert.DeserializeObject<List<Models.Administrator>>(responseContent);
        result.Body = new GenericPagedCollection<Models.Administrator>()
        {
          Page = allAdmins,
          Offset = 0,
          PageSize = allAdmins.Count,
          Total = allAdmins.Count
        };
        return result;
      }
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="BrivoApi.Administrator.Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>  
    public Task<HttpOperationResponse<GenericPagedCollection<Models.Administrator>>>
      ListAdministratorsPagedAsync(PageInfo page,
        CancellationToken cancellationToken = new CancellationToken())
    {
      return this.ListAdministratorsPagedAsync(page, Filter.None, cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Models.Administrator" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Models.Administrator" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Models.Administrator>> GetAdministratorByIdAsync(
      int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<Models.Administrator>(
            string.Format(BrivoApiConstants.UriPaths.GetAdministratorsFormat, id),
            nameof(this.GetAdministratorByIdAsync),
            cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Account" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Account" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<GenericPagedCollection<Account>>> ListAccountsPagedAsync(PageInfo page,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<GenericPagedCollection<Account>>(BrivoApiConstants.UriPaths.GetAccounts, page,
            nameof(this.ListAccountsPagedAsync),
            cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="GenericPagedCollection{TElement}" /> of <see cref="Account" />.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <param name="filter">The filter.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="GenericPagedCollection{T}" /> of <see cref="Account" /> that represents the asynchronous operation.
    /// </returns>
    public Task<HttpOperationResponse<GenericPagedCollection<Account>>> ListAccountsPagedAsync(PageInfo page, Filter filter, CancellationToken cancellationToken = new CancellationToken())
    {
      var queries = new IQueryParametersAccessor[] { page, filter };
      return
        this.GetAsync<GenericPagedCollection<Account>>(
          BrivoApiConstants.UriPaths.GetAccounts, queries, nameof(this.ListAccountsPagedAsync), cancellationToken);
    }

    /// <summary>
    /// Initiates an asynchronous operation to return a HTTP response containing a <see cref="Account" />.
    /// </summary>
    /// <param name="id">The identifier.</param>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}" /> containing a <see cref="Account" /> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Account>> GetAccountByIdAsync(int id,
      CancellationToken cancellationToken = new CancellationToken())
    {
      return
        await
          this.GetAsync<Account>(string.Format(BrivoApiConstants.UriPaths.GetAccountFormat, id),
            nameof(this.GetAccountByIdAsync),
            cancellationToken);
    }

    /// <summary>
    // Initiates an asynchronous operation to return a HTTP response containing a <see cref="BrivoApi.Administrator.Models.Administrator"/>.
    /// </summary>
    /// <param name="cancellationToken">The cancellation token.</param>
    /// <returns>
    /// A task of type <see cref="HttpOperationResponse{T}"/> containing a <see cref="BrivoApi.Administrator.Models.Administrator"/> that represents the asynchronous operation.
    /// </returns>
    public async Task<HttpOperationResponse<Models.Administrator>> GetCurrentAdministratorAsync(
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return
        await
          this.GetAsync<Models.Administrator>(BrivoApiConstants.UriPaths.GetCurrentAdministrator,
            nameof(this.GetCurrentAdministratorAsync),
            cancellationToken);
    }

  }
}