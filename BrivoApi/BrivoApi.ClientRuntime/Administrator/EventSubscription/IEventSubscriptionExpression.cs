﻿using BrivoApi.Administrator.Models;

// ReSharper disable once CheckNamespace
namespace BrivoApi.Administrator.Models
{
  /// <summary>
  /// Defines the interface to an event subscription expression used for search filters.
  /// </summary>
  public interface IEventSubscriptionExpression
  {
    Models.EventSubscription EventSubscription  { get;}
    /// <summary>
    /// Creates an equals subscription filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values equal to the specified value.</returns>
    IEventSubscriptionExpression Equals<TValue>(TValue val);
    /// <summary>
    /// Creates a not equals subscription filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>the event subscription filter.</returns>
    IEventSubscriptionExpression NotEquals<TValue>(TValue val);
    /// <summary>
    /// combines subscription criteria
    /// </summary>
    /// <param name="eventSubscriptionType"></param>
    ///<returns>A combined filter <see cref="IEventSubscriptionExpression"/>.</returns>
    IEventSubscriptionExpression And(EventSubscriptionType eventSubscriptionType);

    /// <summary>
    /// Creates multiple equals subscription filters.
    /// </summary>
    /// <param name="values">The list of value.</param>
    /// <returns>the event subscription filter.</returns>
    IEventSubscriptionExpression In<TValue>(params TValue[] values);

    /// <summary>
    /// Creates multiple not-equals subscription filters.
    /// </summary>
    /// <param name="values">The list of value.</param>
    /// <returns>the event subscription filter.</returns>
    IEventSubscriptionExpression NotIn<TValue>(params TValue[] values);
  }
}