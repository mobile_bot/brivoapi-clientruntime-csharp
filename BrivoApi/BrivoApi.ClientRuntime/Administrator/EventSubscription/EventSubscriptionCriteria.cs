﻿using Newtonsoft.Json;
// ReSharper disable once CheckNamespace
namespace BrivoApi.Administrator.Models
{
  public class EventSubscriptionCriteria
  {
    /// <summary>
    /// Gets or sets the criteria type.
    /// </summary>
    [JsonProperty("criteriaType")]
    public string CriteriaType { get; set; }
    /// <summary>
    /// Gets or sets the criteria operation type.
    /// </summary>
    [JsonProperty("criteriaOperationType")]
    public string CriteriaOperationType { get; set; }
    /// <summary>
    /// Gets or sets the criteria operation value.
    /// </summary>
    [JsonProperty("criteriaValue")]
    public object CriteriaValue { get; set; }
  }
}