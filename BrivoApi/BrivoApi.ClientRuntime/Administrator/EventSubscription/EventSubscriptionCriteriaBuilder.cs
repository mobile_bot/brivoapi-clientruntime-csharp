﻿using System;
using BrivoApi.Administrator.Models;

// ReSharper disable once CheckNamespace
namespace BrivoApi.Administrator.Models
{
  public class EventSubscriptionCriteriaBuilder
  {
    /// <summary>
    /// Creates an <see cref="IEventSubscriptionExpression"/>
    /// </summary>
    /// <param name="eventSubscriptionType">event subscription type. <see cref="EventSubscriptionType"/></param>
    /// <returns>A new <see cref="IEventSubscriptionExpression"/>.</returns>
    public static IEventSubscriptionExpression Where(EventSubscriptionType eventSubscriptionType)
    {
      return new EventSubscriptionExpression(eventSubscriptionType);
    }

    /// <summary>
    /// Creates an <see cref="IEventSubscriptionExpression"/>
    /// </summary>
    /// <param name="eventSubscriptionType">event subscription type. <see cref="EventSubscriptionType"/></param>
    /// <param name="eventSubscriptionExpression"></param>
    /// <returns>A new <see cref="IEventSubscriptionExpression"/>.</returns>
    public static IEventSubscriptionExpression And(EventSubscriptionType eventSubscriptionType,
      IEventSubscriptionExpression eventSubscriptionExpression)
    {
      return eventSubscriptionExpression.And(eventSubscriptionType).Equals(eventSubscriptionExpression);
    }
  }
}