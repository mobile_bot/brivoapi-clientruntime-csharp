﻿// ReSharper disable once CheckNamespace
namespace BrivoApi.Administrator.Models
{
  public enum EventSubscriptionType
  {
    User,
    Site,
    AccessPoint
  }
}