using System.Collections.Generic;
using BrivoApi.Administrator.Models;
using BrivoApi.Core.Filters;

// ReSharper disable once CheckNamespace
namespace BrivoApi.Administrator.Models
{
  internal class EventSubscriptionExpression : IEventSubscriptionExpression
  {
    private EventSubscriptionType subscriptionType;
    public Models.EventSubscription EventSubscription { get; }
    /// <summary>
    /// creates a new EventSubscriptionExpression
    /// </summary>
    /// <param name="type">the EventSubscriptionType</param>
    public EventSubscriptionExpression(EventSubscriptionType type)
    {
      this.subscriptionType = type;
      this.EventSubscription = new Models.EventSubscription
      {Criteria = new List<EventSubscriptionCriteria>()};
    }

    /// <summary>
    /// Creates an equals subscription filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A filter to allow values equal to the specified value.</returns>
    public IEventSubscriptionExpression Equals<TValue>(TValue val)
    {
      this.ExtractCriteria(val, Operands.EqualTo);
      return this;
    }

    /// <summary>
    /// Creates a not equals subscription filter.
    /// </summary>
    /// <param name="val">The value.</param>
    /// <returns>A subscription filter to allow values not equal to the specified value.</returns>
    public IEventSubscriptionExpression NotEquals<TValue>(TValue val)
    {
      this.ExtractCriteria(val, Operands.NotEqualTo);
      return this;
    }
    /// <summary>
    /// combines subscription criteria
    /// </summary>
    /// <param name="eventSubscriptionType"></param>
    ///<returns>A combined <see cref="IEventSubscriptionExpression"/>.</returns>
    public IEventSubscriptionExpression And(EventSubscriptionType eventSubscriptionType)
    {
      this.subscriptionType = eventSubscriptionType;
      return this;
    }

    /// <summary>
    /// Creates multiple equals subscription filters.
    /// </summary>
    /// <param name="values">The list of value.</param>
    /// <returns>the event subscription filter.</returns>
    public IEventSubscriptionExpression In<TValue>(params TValue[] values)
    {
      foreach (var val in values)
      {
        this.ExtractCriteria(val, Operands.EqualTo);
      }
      return this;
    }
    /// <summary>
    /// Creates multiple equals subscription filters.
    /// </summary>
    /// <param name="values">The list of value.</param>
    /// <returns>the event subscription filter.</returns>
    public IEventSubscriptionExpression NotIn<TValue>(params TValue[] values)
    {
      foreach (var val in values)
      {
        this.ExtractCriteria(val, Operands.NotEqualTo);
      }
      return this;
    }
    private void ExtractCriteria<TValue>(TValue val, Operands operand)
    {
      var item = new EventSubscriptionCriteria
      {
        CriteriaOperationType = operand.Render().ToUpper(),
        CriteriaType = this.GetSubscriptionType(),
        CriteriaValue = val
      };
      this.EventSubscription.Criteria.Add(item);
    }
    private string GetSubscriptionType()
    {
      return this.subscriptionType == EventSubscriptionType.AccessPoint ? "ACCESS-POINT" : this.subscriptionType.ToString("G").ToUpper();
    }
  }
}