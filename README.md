Brivo API .NET client library
=============================
A portable .NET client library for working with the Brivo API.

Overview
--------
Provides a simple .NET wrapper around REST calls with synchronous and
asyncronous API bindings.

Getting Started
---------------
The `BrivoAccount` class is the main starting point for all actions and activities.  Use the `BrivoAccount`
class to create a client (facade) for the API actions you want to perform.

The following client facade classes are available:
* `AdministratorClient` - a facade over APIs that OnAir account administrators can use.
* `PassClient` - a facade over APIs that a pass application can use.
* `DealerClient` - a facade over APIs that a dealer login can use.

Each client facade handles automatically refresh of its authorization tokens.  You instantiate the client
once and can use it indefinitely.  For web applications that may only be the duration of a request, but on 
mobile or desktop applications, or in worker services that are long running tokens will be automatically renewed.

To work with raw OAuth tokens, use the `AcquireTokenAsUser` or `AcquireTokenByRefreshToken` methods instead of 
the facade classes.

An authorized **client ID** and **client secret** is necessary to use any Brivo API.  
[See the Brivo API documentation for authentication details.](http://apidocs.brivo.com/)

### Administrator clients
Interaction with an end user account is typically from the perspective of an account administrator 
that wants to perform actions with an API that have traditionally been performed with the OnAir website.

Obtain an `IAdministratorClient` instance using the `BrivoAccount` class.

    var credentials = new AccountCredentials {
	   ClientId = /* client id */,
	   ClientSecret = /* client secret */,
	   UserName = /* the administrator login user name */,
	   Password = /* the administrator login password */
	};

	// blocking
    var client = BrivoAccount.CreateAdministratorClient(credentials);
	// non-blocking
	var client = await BrivoAccount.CreateAdministratorClientAsync(credentials);

Use the `IAdministratorClient` client result to call any APIs available to an administrator.

    var whoami = await client.GetCurrentAdministratorAsync();

### Pass clients
Interaction with a user's pass by creating a pass client.  Since a pass client does not have a user name and password
for OnAir, only the client credentials are required.

    var credentials = new ClientCredentials {
	   ClientId = /* client id */,
	   ClientSecret = /* client secret */
	};

	// blocking
    var client = BrivoAccount.CreatePassClient(credentials);
	// non-blocking
	var client = await BrivoAccount.CreatePassClientAsync(credentials);

Use the `IPassClient` client result to redeem, use and refresh the pass.

    var passToken = await client.RedeemPassTokenAsync(new { email = "john@mail.com", activationCode = "XXX....XXX" });

### Dealer clients
Interaction with a dealer client is used to perform actions with an API that have traditionally been performed with 
the Dealer Extranet website.  Only a dealer account and dealer login and password is allowed.

Obtain an `IDealerClient` instance using the `BrivoAccount` class.

    var credentials = new AccountCredentials {
	   ClientId = /* client id */,
	   ClientSecret = /* client secret */,
	   UserName = /* the dealer login user name */,
	   Password = /* the dealer login password */
	};

	// blocking
    var client = BrivoAccount.CreateDealerClient(credentials);
	// non-blocking
	var client = await BrivoAccount.CreateDealerClientAsync(credentials);

Use the `IDealerClient` client result to call any APIs available to a dealer.

    var whoami = await client.GetCurrentAdministratorAsync();

### OAuth clients
To get and refresh your own OAuth tokens, `BrivoAccount` is again a good starting point.

    var credentials = new AccountCredentials {
	   ClientId = /* client id */,
	   ClientSecret = /* client secret */,
	   UserName = /* the dealer login user name */,
	   Password = /* the dealer login password */
	};
	// blocking
    var oAuthInfo = BrivoAccount.AcquireTokenAsUser(credentials);
	// non-blocking
    var oAuthInfo = await BrivoAccount.AcquireTokenAsUserAsync(credentials);

The token can be saved and refreshed any time thereafter using the ``RefreshToken`` on the response.

	// blocking
    var newOAuthInfo = BrivoAccount.AcquireTokenAsUser(oAuthInfo.RefreshToken);
	// non-blocking
    var newOAuthInfo = await BrivoAccount.AcquireTokenAsUserAsync(oAuthInfo.RefreshToken);

Diagnostics
-----------
Enable tracing in the library using the `ServiceClientTracing` class.  Set the `IsEnabled` flag to 
true and add a tracing interceptor.

    ServiceClientTracing.IsEnabled = true;
    ServiceClientTracing.AddTracingInterceptor(new ConsoleTraceInterceptor());        

Sample OnAir client used with Brivo Mobile Passes
--------
```
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Rest;

// Using BrivoApi.ClientRuntime nuget package 
using BrivoApi;
using BrivoApi.Administrator.Models;
using BrivoApi.Core;
using BrivoApi.Pass;
using BrivoApi.Pass.Models;
using BrivoApi.Security;

namespace Application
{
  public class OnAirClient
  {
    /// <summary>
    /// The client credentials.
    /// </summary>
    readonly ClientCredentials clientCredentials;

    /// <summary>
    /// The OnAir API server URI base.
    /// </summary>
    readonly Uri onAirApiServerUriBase;

    /// <summary>
    /// The OnAir Auth server URI base.
    /// </summary>
    readonly Uri onAirAuthServerUriBase;


    /// <summary>
    /// Initializes a new instance of the <see cref="T:Application.OnAirClientSimple"/> class.
    /// </summary>
    /// <param name="onAirClientId">OnAir client identifier.</param>
    /// <param name="onAirClientSecret">OnAir client secret.</param>
    /// <param name="apikey">Calls to api.brivo.com require a Mashery API key and Authorization header. please get key from <see cref="https://developer.brivo.com/member/register"/> that key</param>
    public OnAirClient(string onAirClientId, string onAirClientSecret, string apikey)
    {
      this.clientCredentials = new ClientCredentials(onAirClientId, onAirClientSecret);
      this.onAirApiServerUriBase = new Uri("https://api.brivo.com/v1/api");
      this.onAirAuthServerUriBase = new Uri("https://auth.brivo.com");
      BrivoAccount.ApiKey = apikey;
    }

    /// <summary>
    /// Creates the pass client.
    /// </summary>
    /// <returns>The pass client.</returns>
    /// <param name="authorizationPayload">Authorization payload - the object returned by the RedeemPass call.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    public async Task<PassClient> CreatePassClient(OAuthResponse authorizationPayload,
      CancellationToken cancellationToken = default(CancellationToken))
    {
      return
        await BrivoAccount.CreatePassClientAsync(this.clientCredentials, authorizationPayload,
          this.onAirAuthServerUriBase, this.onAirApiServerUriBase, cancellationToken);
    }

    /// <summary>
    /// Save this response for later usage when we want to refresh the pass (can serialize/deserialize the string)
    /// </summary>
    /// <returns>The pass token.</returns>
    /// <param name="email">Email.</param>
    /// <param name="accessCode">Access code.</param>
    public async Task<RedeemPassTokenResponse> RedeemPass(string email, string accessCode)
    {
      var passClient = await BrivoAccount.CreatePassClientAsync(this.clientCredentials, this.onAirApiServerUriBase);
      var request = new RedeemPassTokenRequest
      {
        ActivationCode = accessCode,
        Email = email
      };

      return await passClient.RedeemPassTokenAsync(request, this.onAirAuthServerUriBase);
    }

    /// <summary>
    /// gets the pass credential
    /// </summary>
    /// <param name="passClient">Pass client obtained from CreatePassClient call.</param>
    /// <returns>the digital Credential</returns>
    public async Task<DigitalCredential> GetCredential(PassClient passClient)
    {
      var result = await passClient.GetDigitalCredentialAsync();
      return result.Body;
    }

    /// <summary>
    /// gets the pass credential
    /// </summary>
    /// <param name="passClient">Pass client obtained from CreatePassClient call.</param>
    /// <returns>the list of sites</returns>
    public async Task<List<Site>> GetSites(PassClient passClient)
    {
      var result = await passClient.ListSitesPagedAsync(new PageInfo(0, PageInfo.Default.Size));
      return result.Body.Page;
    }

    /// <summary>
    /// gets the pass credential
    /// </summary>
    /// <param name="site">A site object from GetSites call</param>
    /// <param name="passClient">Pass client obtained from CreatePassClient call.</param>
    /// <returns>the list of sites</returns>
    public async Task<List<AccessPoint>> GetAccessPoints(Site site,PassClient passClient)
    {
      var result = await passClient.ListAccessPointsBySiteIdPagedAsync(site.Id,new PageInfo(0, PageInfo.Default.Size));
      return result.Body.Page;
    }

    /// <summary>
    /// Uses the pass to unlock a door
    /// </summary>
    /// <returns>If we failed or succeeded opening the door.</returns>
    /// <param name="passClient">Pass client obtained from CreatePassClient call.</param>
    /// <param name="credential">the credential redeemed from GetCredential call.</param>
    /// <param name="accessPoint">the door we want to unlock from GetAccessPoints call.</param>
    public async Task<bool> UsePass(PassClient passClient, DigitalCredential credential, AccessPoint accessPoint)
    {
      try
      {

        await passClient.UnlockAccessPointAsync(accessPoint.Id, credential.CredentialValue);
        return true;
      }
      catch (HttpOperationException ex)
      {
        switch (ex.Response.StatusCode)
        {
          case HttpStatusCode.BadRequest:
          {
            var msg =
              $"Invalid attempt to use pass '{credential.CredentialValue}' with door '{accessPoint.Id}'.  Either the pass or the door is unusable." +
              "You will need to refresh the pass or request a new invitation from your account administrator.";
            throw new Exception(msg, ex);
          }
          case HttpStatusCode.ServiceUnavailable:
          {
            var msg =$"The OnAir server was unable to communicate with door '{accessPoint.Id}' to use pass '{credential.CredentialValue}'" +
              ", it may have temporarily disconnected from the internet.";
            throw new Exception(msg, ex);
          }
          case HttpStatusCode.Forbidden:
          {
            
            string errorDescription = null;
            var errorResponse = ex.Body as ErrorResponse;
            if (errorResponse != null)
              errorDescription = errorResponse.ErrorDescription;
            else
            {
              var errorMessage = ex.Body as MessageCodeResponse;
              if (errorMessage != null)
                errorDescription = errorMessage.Message;
            }
            // We failed due to revoked permissions or a suspended account
            return false;
          }
          default:
            throw;
        }
      }
    }
  }
}
```